package asd.project.protocols.hyparview.requests;

import babel.requestreply.ProtocolRequest;

import java.util.UUID;

public class GetMembershipRequest extends ProtocolRequest {

    public static final short REQUEST_ID = 101;

    private UUID identifier;

    public GetMembershipRequest() {
        super(GetMembershipRequest.REQUEST_ID);
        this.identifier = UUID.randomUUID();
    }

    public GetMembershipRequest(UUID identifier) {
        super(GetMembershipRequest.REQUEST_ID);
        this.identifier = identifier;
    }

    public UUID getIdentifier() {
        return identifier;
    }
}
