package asd.project.protocols.hyparview;

import asd.project.protocols.hyparview.messages.*;
import asd.project.protocols.hyparview.requests.GetMembershipReply;
import asd.project.protocols.hyparview.requests.GetMembershipRequest;
import asd.project.protocols.hyparview.timers.NeighborNodeUpTimer;
import asd.project.protocols.hyparview.timers.PassiveViewTimer;
import asd.project.utils.Utils;
import babel.exceptions.DestinationProtocolDoesNotExist;
import babel.exceptions.HandlerRegistrationException;
import babel.handlers.ProtocolMessageHandler;
import babel.handlers.ProtocolRequestHandler;
import babel.handlers.ProtocolTimerHandler;
import babel.protocol.GenericProtocol;
import babel.protocol.event.ProtocolMessage;
import babel.requestreply.ProtocolRequest;
import babel.timer.ProtocolTimer;
import network.Host;
import network.INetwork;
import network.INodeListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class HyParView extends GenericProtocol implements INodeListener {

    //Numeric identifier of the protocol
    public final static short PROTOCOL_ID = 100;
    public final static String PROTOCOL_NAME = "HyParView";
    private static final Logger logger = LogManager.getLogger(HyParView.class);
    private Set<Host> activeView;
    private Set<Host> passiveView;
    private Queue<Host> connection_attempts;
    private volatile AtomicInteger nodesDown;
    private int activeViewSize;
    private final ProtocolMessageHandler uponHyParViewNeighborRequest = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            HyParViewNeighborRequest neighborRequest = ((HyParViewNeighborRequest) msg);
            logger.info(String.format("(%d) -> received neighbor request (%d) ", myself.getPort(), neighborRequest.getSender().getPort()));

            if (neighborRequest.getPriority().equals(HyParViewNeighborRequest.PRIORITY.high)
                    || activeView.size() < activeViewSize) {

                addNodeActiveView(neighborRequest.getSender());

                HyParViewNeighborReply reply = new HyParViewNeighborReply(myself, true);

                sendMessage(reply, neighborRequest.getSender());
                logger.debug("[hyparview protocol message] (network) " + myself.getPort());
                logger.info(String.format("(%d) -> (%d) sent neighbor reply accept", myself.getPort(), neighborRequest.getSender().getPort()));
            } else {
                HyParViewNeighborReply reply = new HyParViewNeighborReply(myself, false);

                sendMessage(reply, neighborRequest.getSender());
                logger.debug("[hyparview protocol message] (network) " + myself.getPort());
                logger.info(String.format("(%d) -> (%d) sent neighbor reply deny", myself.getPort(), neighborRequest.getSender().getPort()));
            }
        }
    };
    private final ProtocolMessageHandler uponHyParViewForwardJoinReply = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage protocolMessage) {
            HyParViewForwardJoinReply reply = ((HyParViewForwardJoinReply) protocolMessage);
            addNodeActiveView(reply.getPeer());
        }
    };
    private int passiveViewSize;
    private final ProtocolMessageHandler uponHyParViewDisconnect = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            Host peer = ((HyParViewDisconnect) msg).getPeer();
            logger.info(String.format("(%d) -> received disconnected from (%d)", myself.getPort(), peer.getPort()));
            removeNodeActiveView(peer);
        }
    };
    private final ProtocolMessageHandler uponHyParViewShuffleRequest = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            HyParViewShuffleRequest shuffleRequest = ((HyParViewShuffleRequest) msg);
            logger.info(String.format("(%d) -> received shuffle request (%d) ", myself.getPort(), shuffleRequest.getOriginal().getPort()));

            int ttl = shuffleRequest.getTtl() - 1;
            if (ttl > 0 && activeView.size() > 1) {
                Host n = Utils.getRandomElementFromSetExcept(activeView, shuffleRequest.getSender());
                if (n != null) {
                    sendMessage(new HyParViewShuffleRequest(shuffleRequest.getOriginal(), myself, ttl, shuffleRequest.getView()), n);
                    logger.debug("[hyparview protocol message] (network) " + myself.getPort());
                    logger.info(String.format("(%d) -> (%d) sent shuffleRequest ", myself.getPort(), n.getPort()));
                } else {
                    logger.warn(String.format("(%d) -> cannot send shuffleRequest NULL", myself.getPort()));
                }
            } else {
                List<Host> newNodes = Utils.getRandomSetFromSet(passiveView, shuffleRequest.getView().size());

                sendMessageSideChannel(new HyParViewShuffleReply(new HashSet<>(newNodes)), shuffleRequest.getOriginal());
                logger.debug("[hyparview protocol message] (network) " + myself.getPort());
                addNodesPassiveView(shuffleRequest.getView(), newNodes);
                logger.info(String.format("(%d) -> (%d) sent shuffle reply ", myself.getPort(), shuffleRequest.getOriginal().getPort()));
            }
        }
    };
    private final ProtocolMessageHandler uponHyParViewShuffleReply = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            logger.info(String.format("(%d) -> received shuffleReply (%d)", myself.getPort(), msg.getFrom().getPort()));
            HyParViewShuffleReply shuffleReply = ((HyParViewShuffleReply) msg);
            addNodesPassiveView(shuffleReply.getView(), new LinkedList<>());
        }
    };
    private Map<Host, UUID> neighborTimers;
    private int ARWL;
    private final ProtocolMessageHandler uponHyParViewJoin = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            Host newNode = ((HyParViewJoin) msg).getPeer();
            logger.info(String.format("(%d) -> received join from (%d)", myself.getPort(), newNode.getPort()));
            addNodeActiveView(newNode);
            for (Host n : activeView) {
                if (!n.equals(newNode)) {
                    sendMessage(new HyParViewForwardJoin(newNode, ARWL, myself), n);
                    logger.debug("[hyparview protocol message] (network) " + myself.getPort());
                    logger.info(String.format("(%d) -> (%d) sent forward join", myself.getPort(), newNode.getPort()));
                }
            }
        }
    };
    private int PRWL;
    private final ProtocolMessageHandler uponHyParViewForwardJoin = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            HyParViewForwardJoin fjMsg = ((HyParViewForwardJoin) msg);
            int ttl = fjMsg.getTtl();
            Host newNode = fjMsg.getNewNode();
            Host peer = fjMsg.getPeer();
            logger.info(String.format("(%d) -> received forward join from (%d) ", myself.getPort(), peer.getPort()));

            if (ttl == 0 || activeView.size() == 1) {
                addNodeActiveView(newNode);
                HyParViewForwardJoinReply reply = new HyParViewForwardJoinReply(myself);
                sendMessage(reply, newNode);
                logger.debug("[hyparview protocol message] (network) " + myself.getPort());
            } else {
                if (ttl == PRWL) {
                    addNodePassiveView(newNode);
                }
                Host n = Utils.getRandomElementFromSetExcept(activeView, peer);
                if (n != null) {
                    sendMessage(new HyParViewForwardJoin(newNode, ttl - 1, myself), n);
                    logger.debug("[hyparview protocol message] (network) " + myself.getPort());
                    logger.info(String.format("(%d) -> (%d) sent forward join", myself.getPort(), peer.getPort()));
                }
            }
        }
    };
    private int Ka;
    private int Kp;
    private int neighborWaitTime;
    private final ProtocolMessageHandler uponHyParViewNeighborReply = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            HyParViewNeighborReply neighborReply = ((HyParViewNeighborReply) msg);
            logger.info(String.format("(%d) -> received neighbor reply (%d) ", myself.getPort(), neighborReply.getSender().getPort()));

            if (neighborReply.isAccept() /*&& nodesDown.get() > 0*/ && !activeView.contains(neighborReply.getSender())) {
                addNodeActiveView(neighborReply.getSender());
                nodesDown.decrementAndGet();
            } else /*if(nodesDown.get() > 0)*/ {
                getTestPeer();
            }
        }
    };
    private ProtocolRequestHandler uponGetMembershipRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest request) {
            GetMembershipRequest req = (GetMembershipRequest) request;
            logger.info(String.format("(%d) -> got membership request from (%s)", myself.getPort(), req.getIdentifier()));

            //Send reply
            GetMembershipReply reply = new GetMembershipReply(req.getIdentifier(), activeView);
            reply.invertDestination(req);
            try {
                sendReply(reply);
            } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                destinationProtocolDoesNotExist.printStackTrace();
                System.exit(1);
            }
        }
    };
    private ProtocolTimerHandler uponPassiveViewTimerHandler = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            if (!activeView.isEmpty()) {
                Host destination = Utils.getRandomElementFromSet(activeView);
                logger.info(String.format("(%d) -> proc timer", myself.getPort()));
                List<Host> randomNodesActive = Utils.getRandomSetFromSet(activeView, Ka);
                randomNodesActive.addAll(Utils.getRandomSetFromSet(passiveView, Kp));
                sendMessage(new HyParViewShuffleRequest(myself, myself, PRWL, new HashSet<>(randomNodesActive)), destination);
                logger.debug("[hyparview protocol message] (network) " + myself.getPort());
                logger.info(String.format("(%d) -> (%d) sent shuffle request", myself.getPort(), Objects.requireNonNull(destination).getPort()));
                //logger.info(myself.getPort() + " activeView: " + activeView);
                //logger.info(myself.getPort() + " passiveView: " + passiveView);
            } else {
                logger.warn("--------------------> Empty Active View of " + myself.getPort());
            }
        }
    };
    private ProtocolTimerHandler uponNeighborNodeUpTimerHandler = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            NeighborNodeUpTimer neighborTimer = (NeighborNodeUpTimer) timer;
            removeNetworkPeer(neighborTimer.getNeighbor());
            neighborTimers.remove(neighborTimer.getNeighbor());

            getTestPeer();
        }
    };

    public HyParView(INetwork net) throws HandlerRegistrationException {
        super(PROTOCOL_NAME, HyParView.PROTOCOL_ID, net);

        //Declare Messages sent/received of the protocol
        registerMessageHandler(HyParViewDisconnect.MSG_CODE, uponHyParViewDisconnect, HyParViewDisconnect.serializer);
        registerMessageHandler(HyParViewJoin.MSG_CODE, uponHyParViewJoin, HyParViewJoin.serializer);
        registerMessageHandler(HyParViewForwardJoin.MSG_CODE, uponHyParViewForwardJoin, HyParViewForwardJoin.serializer);
        registerMessageHandler(HyParViewForwardJoinReply.MSG_CODE, uponHyParViewForwardJoinReply, HyParViewForwardJoinReply.serializer);
        registerMessageHandler(HyParViewShuffleRequest.MSG_CODE, uponHyParViewShuffleRequest, HyParViewShuffleRequest.serializer);
        registerMessageHandler(HyParViewShuffleReply.MSG_CODE, uponHyParViewShuffleReply, HyParViewShuffleReply.serializer);
        registerMessageHandler(HyParViewNeighborRequest.MSG_CODE, uponHyParViewNeighborRequest, HyParViewNeighborRequest.serializer);
        registerMessageHandler(HyParViewNeighborReply.MSG_CODE, uponHyParViewNeighborReply, HyParViewNeighborReply.serializer);

        //Declare Timers of the Protocol
        registerTimerHandler(PassiveViewTimer.TimerCode, uponPassiveViewTimerHandler);
        registerTimerHandler(NeighborNodeUpTimer.TimerCode, uponNeighborNodeUpTimerHandler);

        //Declare Notifications issued by the protocol

        //declare Notifications consumed by the protocol

        //declare requests exposed by the protocol
        registerRequestHandler(GetMembershipRequest.REQUEST_ID, uponGetMembershipRequest);

        //declare replies consumed by the protocol
    }

    private void addNodeActiveView(Host node) {
        if (!node.equals(myself) && !activeView.contains(node)) {
            passiveView.remove(node);
            if (activeView.size() == this.activeViewSize) {
                dropRandomElementFromActiveView();
            }
            activeView.add(node);
            addNetworkPeer(node);
        }
        logger.info(String.format("(%d) -> added node (%d) to active view", myself.getPort(), node.getPort()));
    }

    private boolean removeNodeActiveView(Host host) {
        if (activeView.remove(host)) {
            removeNetworkPeer(host);
            addNodePassiveView(host);
            logger.info(String.format("(%d) -> removed node (%d) from active view", myself.getPort(), host.getPort()));
            return true;
        }
        return false;
    }

    private void addNodePassiveView(Host node) {
        logger.info(String.format("(%d) -> adding node (%d) to passive view", myself.getPort(), node.getPort()));

        if (!node.equals(myself) && !activeView.contains(node) && !passiveView.contains(node)) {
            if (passiveView.size() == passiveViewSize) {
                Host r = Utils.getRandomElementFromSet(passiveView);
                passiveView.remove(r);
            }
            passiveView.add(node);
        }
    }

    private void addNodesPassiveView(Set<Host> received, List<Host> sent) {
        for (Host h : received) {
            if (!h.equals(myself) && !activeView.contains(h) && !passiveView.contains(h)) {
                if (passiveView.size() == passiveViewSize) {
                    Host chosen = !sent.isEmpty() ? sent.remove(0) : null;
                    if (chosen != null)
                        passiveView.remove(chosen);
                    else {
                        Host randomNode = Utils.getRandomElementFromSet(passiveView);
                        passiveView.remove(randomNode);
                    }
                }
                passiveView.add(h);
            }
        }
    }

    private void dropRandomElementFromActiveView() {
        Host n = Utils.getRandomElementFromSet(this.activeView);

        if (n != null) {
            removeNodeActiveView(n);
            sendMessageSideChannel(new HyParViewDisconnect(myself), n);
            logger.debug("[hyparview protocol message] (network) " + myself.getPort());
        }
    }

    @Override
    public void init(Properties props) {
        //Setup configuration of the protocol
        registerNodeListener(this);

        this.activeViewSize = Integer.parseInt(props.getProperty("ActiveViewSize", "5"));
        this.passiveViewSize = Integer.parseInt(props.getProperty("PassiveViewSize", "30"));
        this.ARWL = Integer.parseInt(props.getProperty("ARWL", "6"));
        this.PRWL = Integer.parseInt(props.getProperty("PRWL", "3"));
        this.Ka = Integer.parseInt(props.getProperty("Ka", "3"));
        this.Kp = Integer.parseInt(props.getProperty("Kp", "4"));
        this.neighborWaitTime = Integer.parseInt((props.getProperty("NeighborWaitTime", "5000")));

        this.activeView = new HashSet<>();
        this.passiveView = new HashSet<>();
        this.connection_attempts = new ConcurrentLinkedQueue<>();
        this.nodesDown = new AtomicInteger(0);

        this.neighborTimers = new HashMap<>(activeViewSize);

        if (props.containsKey("Contact")) {
            try {
                String[] hostElems = props.getProperty("Contact").split(":");
                Host contact = new Host(InetAddress.getByName(hostElems[0]), Short.parseShort(hostElems[1]));
                addNodeActiveView(contact);

                sendMessage(new HyParViewJoin(myself), contact);
                logger.debug("[hyparview protocol message] (network) " + myself.getPort());
            } catch (Exception e) {
                logger.warn("Invalid contact on configuration: '" + props.getProperty("Contact"));
            }
        }

        //Setup timers
        setupPeriodicTimer(
                new PassiveViewTimer(),
                Long.parseLong(props.getProperty("TimerFirst")),
                Long.parseLong(props.getProperty("TimerPeriod"))
        );
    }

    //Implementation of the INodeListener interface
    @Override
    public void nodeDown(Host peer) {
        logger.info(String.format("(%d) -> node down (%d)", myself.getPort(), peer.getPort()));

        if (removeNodeActiveView(peer)) {
            passiveView.remove(peer);
            getTestPeer();
        }
    }

    private void getTestPeer() {

        if (connection_attempts.size() == passiveView.size())
            connection_attempts.clear();
        Host test_peer = Utils.getRandomElementFromSetExceptQueue(passiveView, connection_attempts);
        logger.info(String.format("(%s) -> TEST PEER (%s)", myself.getPort(), test_peer == null ? "NULL" : test_peer.getPort()));

        if (test_peer == null)
            return;

        connection_attempts.add(test_peer);
        addNetworkPeer(test_peer);

        UUID timerId = setupTimer(new NeighborNodeUpTimer(test_peer), neighborWaitTime);
        neighborTimers.put(test_peer, timerId);
    }

    @Override
    public void nodeUp(Host peer) {
        logger.info(String.format("(%d) -> node up (%d)", myself.getPort(), peer.getPort()));

        doNeighborRequestIfNeeded(peer);
    }

    @Override
    public void nodeConnectionReestablished(Host peer) {
        logger.info(String.format("(%d) -> connection reestablished (%d)", myself.getPort(), peer.getPort()));

        doNeighborRequestIfNeeded(peer);
    }

    private void doNeighborRequestIfNeeded(Host peer) {
        UUID timerId = neighborTimers.get(peer);
        if (timerId != null) {
            cancelTimer(timerId);
            neighborTimers.remove(peer);
            HyParViewNeighborRequest request = new HyParViewNeighborRequest(myself,
                    this.activeView.isEmpty() ? HyParViewNeighborRequest.PRIORITY.high : HyParViewNeighborRequest.PRIORITY.low);
            this.sendMessage(request, peer);
            logger.debug("[hyparview protocol message] (network) " + myself.getPort());
        }
    }
}
