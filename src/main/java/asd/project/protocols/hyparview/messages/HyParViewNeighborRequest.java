package asd.project.protocols.hyparview.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;

public class HyParViewNeighborRequest extends ProtocolMessage {

    public final static short MSG_CODE = 106;
    public static final ISerializer<HyParViewNeighborRequest> serializer = new ISerializer<>() {
        @Override
        public void serialize(HyParViewNeighborRequest req, ByteBuf out) {
            byte[] prio_bytes = req.priority.toString().getBytes();

            req.sender.serialize(out);
            out.writeInt(prio_bytes.length);
            out.writeBytes(prio_bytes);
        }

        @Override
        public HyParViewNeighborRequest deserialize(ByteBuf in) throws UnknownHostException {
            Host sender = Host.deserialize(in);

            int bytes_size = in.readInt();
            byte[] prio_bytes = new byte[bytes_size];
            in.readBytes(prio_bytes);
            String prio_string = new String(prio_bytes);
            PRIORITY priority = PRIORITY.valueOf(prio_string);

            return new HyParViewNeighborRequest(sender, priority);
        }

        @Override
        public int serializedSize(HyParViewNeighborRequest req) {
            if (req.size == -1) {
                req.size = 0;
                req.size += req.sender.serializedSize();
                req.size += 4;
                req.size += req.priority.toString().getBytes().length;
            }
            return req.size;
        }
    };
    private final Host sender; // request sender
    private final PRIORITY priority;

    private int size;

    public HyParViewNeighborRequest(Host sender, PRIORITY priority) {
        super(MSG_CODE);
        this.sender = sender;
        this.priority = priority;

        this.size = -1;
    }

    public Host getSender() {
        return sender;
    }

    public PRIORITY getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return "HyParViewNeighborRequest{" +
                "payload=" + sender + " " + priority +
                '}';
    }

    public enum PRIORITY {
        low, high
    }
}
