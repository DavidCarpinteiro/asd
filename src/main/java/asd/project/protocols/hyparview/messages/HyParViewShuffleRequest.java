package asd.project.protocols.hyparview.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;

public class HyParViewShuffleRequest extends ProtocolMessage {

    public final static short MSG_CODE = 104;
    public static final ISerializer<HyParViewShuffleRequest> serializer = new ISerializer<>() {
        @Override
        public void serialize(HyParViewShuffleRequest req, ByteBuf out) {
            int viewSize = req.view.size();
            out.writeInt(viewSize);

            for (Host h : req.view)
                h.serialize(out);

            out.writeInt(req.ttl);

            req.sender.serialize(out);

            req.original.serialize(out);
        }

        @Override
        public HyParViewShuffleRequest deserialize(ByteBuf in) throws UnknownHostException {
            int viewSize = in.readInt();

            Set<Host> view = new HashSet<>();
            for (int i = 0; i < viewSize; i++)
                view.add(Host.deserialize(in));

            int ttl = in.readInt();

            Host sender = Host.deserialize(in);

            Host original = Host.deserialize(in);

            return new HyParViewShuffleRequest(original, sender, ttl, view);
        }

        @Override
        public int serializedSize(HyParViewShuffleRequest req) {
            if (req.size == -1) {
                req.size = 0;
                req.size += 4;
                for (Host h : req.view) {
                    req.size += h.serializedSize();
                }
                req.size += 4;
                req.size += req.sender.serializedSize();
                req.size += req.original.serializedSize();
            }
            return req.size;
        }
    };
    private final Host sender; // request sender
    private final Host original;
    private final int ttl;
    private final Set<Host> view;
    private int size;

    public HyParViewShuffleRequest(Host original, Host sender, int ttl, Set<Host> view) {
        super(MSG_CODE);
        this.sender = sender;
        this.original = original;
        this.ttl = ttl;
        this.view = new HashSet<>(view);

        this.size = -1;
    }

    public Host getSender() {
        return sender;
    }

    public Host getOriginal() {
        return original;
    }

    public int getTtl() {
        return ttl;
    }

    public Set<Host> getView() {
        return view;
    }

    @Override
    public String toString() {
        return "HyParViewShuffleRequest{" +
                "payload=" + view + " " + ttl + " " + sender + " " + original +
                '}';
    }
}
