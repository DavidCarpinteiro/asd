package asd.project.protocols.hyparview.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;

public class HyParViewShuffleReply extends ProtocolMessage {

    public final static short MSG_CODE = 105;
    public static final ISerializer<HyParViewShuffleReply> serializer = new ISerializer<>() {
        @Override
        public void serialize(HyParViewShuffleReply req, ByteBuf out) {
            int viewSize = req.view.size();
            out.writeInt(viewSize);

            for (Host h : req.view)
                h.serialize(out);
        }

        @Override
        public HyParViewShuffleReply deserialize(ByteBuf in) throws UnknownHostException {
            int viewSize = in.readInt();

            Set<Host> view = new HashSet<>();
            for (int i = 0; i < viewSize; i++)
                view.add(Host.deserialize(in));

            return new HyParViewShuffleReply(view);
        }

        @Override
        public int serializedSize(HyParViewShuffleReply req) {
            if (req.size == -1) {
                req.size = 0;
                req.size += 4;
                for (Host h : req.view) {
                    req.size += h.serializedSize();
                }
            }
            return req.size;
        }
    };
    private final Set<Host> view;
    private int size;

    public HyParViewShuffleReply(Set<Host> view) {
        super(MSG_CODE);
        this.view = new HashSet<>(view);

        this.size = -1;
    }

    public Set<Host> getView() {
        return view;
    }

    @Override
    public String toString() {
        return "HyParViewShuffleReply{" +
                "payload=" + view +
                '}';
    }
}
