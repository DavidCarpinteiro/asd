package asd.project.protocols.hyparview.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;

public class HyParViewDisconnect extends ProtocolMessage {

    public final static short MSG_CODE = 101;
    public static final ISerializer<HyParViewDisconnect> serializer = new ISerializer<>() {
        @Override
        public void serialize(HyParViewDisconnect disconnect, ByteBuf out) {
            disconnect.peer.serialize(out);
        }

        @Override
        public HyParViewDisconnect deserialize(ByteBuf in) throws UnknownHostException {
            return new HyParViewDisconnect(Host.deserialize(in));
        }

        @Override
        public int serializedSize(HyParViewDisconnect disconnect) {
            return disconnect.peer.serializedSize();
        }
    };
    private final Host peer;

    public HyParViewDisconnect(Host peer) {
        super(MSG_CODE);
        this.peer = peer;
    }

    public Host getPeer() {
        return peer;
    }

    @Override
    public String toString() {
        return "HyParViewDisconnect{" +
                "payload=" + peer +
                '}';
    }
}
