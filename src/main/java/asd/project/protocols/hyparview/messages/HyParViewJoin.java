package asd.project.protocols.hyparview.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;

public class HyParViewJoin extends ProtocolMessage {

    public final static short MSG_CODE = 102;
    public static final ISerializer<HyParViewJoin> serializer = new ISerializer<>() {
        @Override
        public void serialize(HyParViewJoin join, ByteBuf out) {
            join.peer.serialize(out);
        }

        @Override
        public HyParViewJoin deserialize(ByteBuf in) throws UnknownHostException {
            return new HyParViewJoin(Host.deserialize(in));
        }

        @Override
        public int serializedSize(HyParViewJoin join) {
            return join.peer.serializedSize();
        }
    };
    private final Host peer;

    public HyParViewJoin(Host peer) {
        super(MSG_CODE);
        this.peer = peer;
    }

    public Host getPeer() {
        return peer;
    }

    @Override
    public String toString() {
        return "HyParViewJoin{" +
                "payload=" + peer +
                '}';
    }
}
