package asd.project.protocols.hyparview.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;

public class HyParViewNeighborReply extends ProtocolMessage {

    public final static short MSG_CODE = 107;
    public static final ISerializer<HyParViewNeighborReply> serializer = new ISerializer<>() {
        @Override
        public void serialize(HyParViewNeighborReply req, ByteBuf out) {
            out.writeInt(req.reply);
            req.sender.serialize(out);
        }

        @Override
        public HyParViewNeighborReply deserialize(ByteBuf in) throws UnknownHostException {
            int reply = in.readInt();

            Host sender = Host.deserialize(in);

            return new HyParViewNeighborReply(sender, reply == 1);
        }

        @Override
        public int serializedSize(HyParViewNeighborReply req) {
            if (req.size == -1) {
                req.size = 0;
                req.size += req.sender.serializedSize();
                req.size += 4;
            }
            return req.size;
        }
    };
    private final Host sender; // request sender
    private final int reply;
    private int size;

    public HyParViewNeighborReply(Host sender, boolean reply) {
        super(MSG_CODE);
        this.sender = sender;
        this.reply = reply ? 1 : 0;

        this.size = -1;
    }

    public Host getSender() {
        return sender;
    }

    public boolean isAccept() {
        return reply == 1;
    }

    @Override
    public String toString() {
        return "HyParViewNeighborReply{" +
                "payload=" + sender + " " + reply +
                '}';
    }
}
