package asd.project.protocols.hyparview.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;

public class HyParViewForwardJoinReply extends ProtocolMessage {

    public final static short MSG_CODE = 108;
    public static final ISerializer<HyParViewForwardJoinReply> serializer = new ISerializer<>() {
        @Override
        public void serialize(HyParViewForwardJoinReply reply, ByteBuf out) {
            reply.peer.serialize(out);
        }

        @Override
        public HyParViewForwardJoinReply deserialize(ByteBuf in) throws UnknownHostException {
            return new HyParViewForwardJoinReply(Host.deserialize(in));
        }

        @Override
        public int serializedSize(HyParViewForwardJoinReply reply) {
            return reply.peer.serializedSize();
        }
    };
    private final Host peer;

    public HyParViewForwardJoinReply(Host peer) {
        super(MSG_CODE);
        this.peer = peer;
    }

    public Host getPeer() {
        return peer;
    }

    @Override
    public String toString() {
        return String.format("HyParViewForwardJoinReply{peer=%s}", peer);
    }
}
