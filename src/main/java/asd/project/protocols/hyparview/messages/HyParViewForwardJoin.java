package asd.project.protocols.hyparview.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;

public class HyParViewForwardJoin extends ProtocolMessage {

    public final static short MSG_CODE = 103;
    public static final ISerializer<HyParViewForwardJoin> serializer = new ISerializer<>() {
        @Override
        public void serialize(HyParViewForwardJoin join, ByteBuf out) {
            join.newNode.serialize(out);
            out.writeInt(join.ttl);
            join.peer.serialize(out);
        }

        @Override
        public HyParViewForwardJoin deserialize(ByteBuf in) throws UnknownHostException {
            return new HyParViewForwardJoin(Host.deserialize(in), in.readInt(), Host.deserialize(in));
        }

        @Override
        public int serializedSize(HyParViewForwardJoin join) {
            return join.newNode.serializedSize() + 4 + join.peer.serializedSize();
        }
    };
    private final Host peer;
    private final int ttl;
    private final Host newNode;

    public HyParViewForwardJoin(Host newNode, int ttl, Host peer) {
        super(MSG_CODE);
        this.peer = peer;
        this.ttl = ttl;
        this.newNode = newNode;
    }

    public Host getPeer() {
        return peer;
    }

    public int getTtl() {
        return ttl;
    }

    public Host getNewNode() {
        return newNode;
    }

    @Override
    public String toString() {
        return "HyParViewForwardJoin{" +
                "payload=" + newNode + " " + ttl + " " + peer +
                '}';
    }
}
