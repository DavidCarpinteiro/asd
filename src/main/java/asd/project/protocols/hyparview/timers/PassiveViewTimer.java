package asd.project.protocols.hyparview.timers;

import babel.timer.ProtocolTimer;

public class PassiveViewTimer extends ProtocolTimer {

    public static final short TimerCode = 102;

    public PassiveViewTimer() {
        super(PassiveViewTimer.TimerCode);
    }

    @Override
    public Object clone() {
        return this;
    }
}