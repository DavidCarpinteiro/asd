package asd.project.protocols.hyparview.timers;

import babel.timer.ProtocolTimer;
import network.Host;

public class NeighborNodeUpTimer extends ProtocolTimer {

    public static final short TimerCode = 103;

    private Host neighbor;

    public NeighborNodeUpTimer(Host neighbor) {
        super(NeighborNodeUpTimer.TimerCode);
        this.neighbor = neighbor;
    }

    public Host getNeighbor() {
        return this.neighbor;
    }

    @Override
    public Object clone() {
        return this;
    }
}