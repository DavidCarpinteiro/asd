package asd.project.protocols.dht.messages;

import asd.project.utils.Message;
import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.math.BigInteger;
import java.net.UnknownHostException;

public class RouteMessageDelivery extends ProtocolMessage {

    public final static short MSG_CODE = 43;
    public static final ISerializer<RouteMessageDelivery> serializer = new ISerializer<>() {
        @Override
        public void serialize(RouteMessageDelivery req, ByteBuf out) {
            byte[] idBytes = req.ID.toByteArray();
            out.writeInt(idBytes.length);
            out.writeBytes(idBytes);

            req.getMessage().serialize(out);

            req.sender.serialize(out);
        }

        @Override
        public RouteMessageDelivery deserialize(ByteBuf in) throws UnknownHostException {
            int idBytesLen = in.readInt();
            byte[] idBytes = new byte[idBytesLen];
            in.readBytes(idBytes);

            Message message = Message.deserialize(in);

            Host h = Host.deserialize(in);

            return new RouteMessageDelivery(new BigInteger(idBytes), message, h);
        }

        @Override
        public int serializedSize(RouteMessageDelivery req) {
            return Integer.BYTES + req.ID.toByteArray().length + req.message.serializedSize() + req.sender.serializedSize();
        }
    };
    private final BigInteger ID;
    private final Message message;
    private final Host sender;

    public RouteMessageDelivery(BigInteger ID, Message message, Host sender) {
        super(MSG_CODE);
        this.ID = ID;
        this.message = message;
        this.sender = sender;
    }

    public BigInteger getID() {
        return ID;
    }

    public Message getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return String.format("ChordRouteMessageDelivery{ID=%s, message=%s}", ID, message);
    }

    public Host getSender() {
        return sender;
    }
}
