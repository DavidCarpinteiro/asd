package asd.project.protocols.dht.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.util.UUID;

public class SuccessorPredecessorRequest extends ProtocolMessage {

    public final static short MSG_CODE = 404;
    public static final ISerializer<SuccessorPredecessorRequest> serializer = new ISerializer<>() {
        @Override
        public void serialize(SuccessorPredecessorRequest req, ByteBuf out) {

            out.writeLong(req.uuid.getMostSignificantBits());
            out.writeLong(req.uuid.getLeastSignificantBits());
        }

        @Override
        public SuccessorPredecessorRequest deserialize(ByteBuf in) {

            UUID uuid = new UUID(in.readLong(), in.readLong());

            return new SuccessorPredecessorRequest(uuid);
        }

        @Override
        public int serializedSize(SuccessorPredecessorRequest req) {
            return Long.BYTES * 2;
        }
    };
    private final UUID uuid;

    public SuccessorPredecessorRequest(UUID uuid) {
        super(MSG_CODE);
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return "ChordSuccessorPredecessorRequest{" +
                "payload=" + uuid + " " +
                '}';
    }
}
