package asd.project.protocols.dht.messages;

import asd.project.protocols.dht.Finger;
import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;
import java.util.UUID;

public class SuccessorPredecessorReply extends ProtocolMessage {

    public final static short MSG_CODE = 405;
    public static final ISerializer<SuccessorPredecessorReply> serializer = new ISerializer<>() {
        @Override
        public void serialize(SuccessorPredecessorReply req, ByteBuf out) {
            req.predId.serialize(out);

            out.writeLong(req.uuid.getMostSignificantBits());
            out.writeLong(req.uuid.getLeastSignificantBits());
        }

        @Override
        public SuccessorPredecessorReply deserialize(ByteBuf in) throws UnknownHostException {
            Finger predId = Finger.deserialize(in);

            UUID uuid = new UUID(in.readLong(), in.readLong());

            return new SuccessorPredecessorReply(predId, uuid);
        }

        @Override
        public int serializedSize(SuccessorPredecessorReply req) {
            return req.predId.serializedSize() + Long.BYTES * 2;
        }
    };
    private final Finger predId;
    private final UUID uuid;

    public SuccessorPredecessorReply(Finger predId, UUID uuid) {
        super(MSG_CODE);
        this.predId = predId;
        this.uuid = uuid;
    }

    public Finger getPredecessorId() {
        return this.predId;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return "ChordSuccessorPredecessorReply{" +
                "payload=" + this.uuid + " " + this.predId + " " +
                '}';
    }
}
