package asd.project.protocols.dht.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.math.BigInteger;
import java.net.UnknownHostException;
import java.util.UUID;

public class PredecessorRequest extends ProtocolMessage {

    public final static short MSG_CODE = 401;
    public static final ISerializer<PredecessorRequest> serializer = new ISerializer<>() {
        @Override
        public void serialize(PredecessorRequest req, ByteBuf out) {
            req.originalSender.serialize(out);

            byte[] idBytes = req.id.toByteArray();
            out.writeInt(idBytes.length);
            out.writeBytes(idBytes);

            out.writeLong(req.uuid.getMostSignificantBits());
            out.writeLong(req.uuid.getLeastSignificantBits());

        }

        @Override
        public PredecessorRequest deserialize(ByteBuf in) throws UnknownHostException {
            Host originalSender = Host.deserialize(in);

            int idBytesLen = in.readInt();
            byte[] idBytes = new byte[idBytesLen];
            in.readBytes(idBytes);

            UUID uuid = new UUID(in.readLong(), in.readLong());

            return new PredecessorRequest(new BigInteger(idBytes), originalSender, uuid);
        }

        @Override
        public int serializedSize(PredecessorRequest req) {
            return req.originalSender.serializedSize() + req.id.toByteArray().length + Long.BYTES * 2 + Integer.BYTES;
        }
    };
    private final Host originalSender;
    private final BigInteger id;
    private final UUID uuid;

    public PredecessorRequest(BigInteger id, Host originalSender, UUID uuid) {
        super(MSG_CODE);
        this.originalSender = originalSender;
        this.id = id;
        this.uuid = uuid;
    }

    public Host getOriginalSender() {
        return originalSender;
    }

    public BigInteger getRequestId() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return "ChordPredecessorRequest{" +
                "payload=" + originalSender + " " + uuid + " " + id + " " +
                '}';
    }
}
