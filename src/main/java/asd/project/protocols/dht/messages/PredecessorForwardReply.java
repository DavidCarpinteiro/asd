package asd.project.protocols.dht.messages;

import asd.project.utils.Message;
import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.math.BigInteger;
import java.net.UnknownHostException;

public class PredecessorForwardReply extends ProtocolMessage {

    public final static short MSG_CODE = 407;
    public static final ISerializer<PredecessorForwardReply> serializer = new ISerializer<>() {
        @Override
        public void serialize(PredecessorForwardReply req, ByteBuf out) {

            byte[] idBytes = req.msgID.toByteArray();
            out.writeInt(idBytes.length);
            out.writeBytes(idBytes);

            req.msg.serialize(out);
        }

        @Override
        public PredecessorForwardReply deserialize(ByteBuf in) throws UnknownHostException {

            int idBytesLen = in.readInt();
            byte[] idBytes = new byte[idBytesLen];
            in.readBytes(idBytes);

            Message msg = Message.deserialize(in);

            return new PredecessorForwardReply(new BigInteger(idBytes), msg);
        }

        @Override
        public int serializedSize(PredecessorForwardReply req) {
            return req.msgID.toByteArray().length + req.msg.serializedSize();
        }
    };
    private final BigInteger msgID;
    private final Message msg;

    public PredecessorForwardReply(BigInteger msgID, Message msg) {
        super(MSG_CODE);
        this.msgID = msgID;
        this.msg = msg;
    }

    public BigInteger getRequestId() {
        return msgID;
    }

    public Message getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "ChordPredecessorForwardReply{" +
                "payload=" + " " + msgID + " " + msg +
                '}';
    }
}
