package asd.project.protocols.dht.messages;

import asd.project.protocols.dht.Finger;
import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;
import java.util.UUID;

public class PredecessorReply extends ProtocolMessage {

    public final static short MSG_CODE = 402;
    public static final ISerializer<PredecessorReply> serializer = new ISerializer<>() {
        @Override
        public void serialize(PredecessorReply req, ByteBuf out) {

            req.predId.serialize(out);
            req.sucId.serialize(out);

            out.writeLong(req.uuid.getMostSignificantBits());
            out.writeLong(req.uuid.getLeastSignificantBits());
        }

        @Override
        public PredecessorReply deserialize(ByteBuf in) throws UnknownHostException {

            Finger predId = Finger.deserialize(in);
            Finger sucId = Finger.deserialize(in);

            UUID uuid = new UUID(in.readLong(), in.readLong());

            return new PredecessorReply(predId, sucId, uuid);
        }

        @Override
        public int serializedSize(PredecessorReply req) {
            return req.predId.serializedSize() + req.sucId.serializedSize() + Long.BYTES * 2;
        }
    };
    private final Finger predId;
    private final Finger sucId;
    private final UUID uuid;

    public PredecessorReply(Finger predId, Finger sucId, UUID uuid) {
        super(MSG_CODE);
        this.predId = predId;
        this.sucId = sucId;
        this.uuid = uuid;
    }

    public Finger getPredecessorId() {
        return this.predId;
    }

    public Finger getSuccessorId() {
        return this.sucId;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return "ChordPredecessorReply{" +
                "payload=" + this.uuid + " " + this.predId + " " + this.sucId + " " +
                '}';
    }
}
