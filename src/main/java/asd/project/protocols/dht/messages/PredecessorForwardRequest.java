package asd.project.protocols.dht.messages;

import asd.project.utils.Message;
import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.math.BigInteger;
import java.net.UnknownHostException;

public class PredecessorForwardRequest extends ProtocolMessage {

    public final static short MSG_CODE = 406;
    public static final ISerializer<PredecessorForwardRequest> serializer = new ISerializer<>() {
        @Override
        public void serialize(PredecessorForwardRequest req, ByteBuf out) {

            byte[] idBytes = req.msgID.toByteArray();
            out.writeInt(idBytes.length);
            out.writeBytes(idBytes);

            req.msg.serialize(out);

            req.sender.serialize(out);
        }

        @Override
        public PredecessorForwardRequest deserialize(ByteBuf in) throws UnknownHostException {

            int idBytesLen = in.readInt();
            byte[] idBytes = new byte[idBytesLen];
            in.readBytes(idBytes);

            Message msg = Message.deserialize(in);

            Host host = Host.deserialize(in);

            return new PredecessorForwardRequest(new BigInteger(idBytes), msg, host);
        }

        @Override
        public int serializedSize(PredecessorForwardRequest req) {
            return req.msgID.toByteArray().length + req.msg.serializedSize() + req.sender.serializedSize() + Integer.BYTES;
        }
    };
    private final BigInteger msgID;
    private final Message msg;
    private Host sender;

    public PredecessorForwardRequest(BigInteger msgID, Message msg, Host sender) {
        super(MSG_CODE);
        this.msgID = msgID;
        this.msg = msg;
        this.sender = sender;
    }

    public Host getSender() {
        return sender;
    }

    public BigInteger getRequestId() {
        return msgID;
    }

    public Message getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "ChordPredecessorForwardRequest{" +
                "payload=" + " " + msgID + " " + msg + " " +
                '}';
    }
}
