package asd.project.protocols.dht.messages;

import asd.project.protocols.dht.Finger;
import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;
import java.util.UUID;

public class NotifyRequest extends ProtocolMessage {
    public final static short MSG_CODE = 408;
    public static final ISerializer<NotifyRequest> serializer = new ISerializer<>() {
        @Override
        public void serialize(NotifyRequest req, ByteBuf out) {
            req.id.serialize(out);

            out.writeLong(req.uuid.getMostSignificantBits());
            out.writeLong(req.uuid.getLeastSignificantBits());
        }

        @Override
        public NotifyRequest deserialize(ByteBuf in) throws UnknownHostException {
            Finger node = Finger.deserialize(in);
            UUID uuid = new UUID(in.readLong(), in.readLong());

            return new NotifyRequest(node, uuid);
        }

        @Override
        public int serializedSize(NotifyRequest req) {
            return req.id.serializedSize() + Long.BYTES * 2;
        }
    };
    private final Finger id;
    private final UUID uuid;

    public NotifyRequest(Finger id, UUID uuid) {
        super(MSG_CODE);
        this.id = id;
        this.uuid = uuid;
    }

    public Finger getID() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return "ChordNotifyRequest{" +
                "payload=" + uuid + " " + this.id +
                '}';
    }
}
