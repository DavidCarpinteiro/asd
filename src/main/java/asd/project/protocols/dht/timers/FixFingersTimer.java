package asd.project.protocols.dht.timers;

import babel.timer.ProtocolTimer;

public class FixFingersTimer extends ProtocolTimer {

    public static final short TimerCode = 401;

    public FixFingersTimer() {
        super(FixFingersTimer.TimerCode);
    }

    @Override
    public Object clone() {
        return this;
    }
}