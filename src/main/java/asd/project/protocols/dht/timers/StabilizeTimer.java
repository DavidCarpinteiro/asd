package asd.project.protocols.dht.timers;

import babel.timer.ProtocolTimer;

public class StabilizeTimer extends ProtocolTimer {

    public static final short TimerCode = 402;


    public StabilizeTimer() {
        super(StabilizeTimer.TimerCode);
    }


    @Override
    public Object clone() {
        return this;
    }
}