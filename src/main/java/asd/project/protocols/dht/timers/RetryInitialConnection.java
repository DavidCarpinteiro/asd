package asd.project.protocols.dht.timers;

import babel.timer.ProtocolTimer;

public class RetryInitialConnection extends ProtocolTimer {

    public static final short TimerCode = 404;

    public RetryInitialConnection() {
        super(RetryInitialConnection.TimerCode);
    }

    @Override
    public Object clone() {
        return this;
    }
}