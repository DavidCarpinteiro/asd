package asd.project.protocols.dht.helper;

import asd.project.protocols.disseminate.Disseminate;
import asd.project.utils.Message;

import java.math.BigInteger;

public class Context {

    private ExecutionStage label;
    private int index;
    private BigInteger ID;
    private Message message;
    private short senderId;

    public Context(ExecutionStage label) {
        this(label, -1, null, null, Disseminate.PROTOCOL_ID);
    }

    public Context(ExecutionStage label, int index, BigInteger ID, Message message, short senderId) {
        this.label = label;
        this.index = index;
        this.ID = ID;
        this.message = message;
        this.senderId = senderId;
    }

    public ExecutionStage getLabel() {
        return label;
    }

    public void setLabel(ExecutionStage label) {
        this.label = label;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public BigInteger getID() {
        return ID;
    }

    public void setID(BigInteger ID) {
        this.ID = ID;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public short getSenderId() {
        return senderId;
    }

    public void setSenderId(short senderId) {
        this.senderId = senderId;
    }
}
