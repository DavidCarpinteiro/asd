package asd.project.protocols.dht.helper;

public enum ExecutionStage {
    CONCURRENT_JOIN,
    FIX_FINGERS,
    ROUTE
}
