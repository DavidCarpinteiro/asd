package asd.project.protocols.dht;

import network.Host;
import org.apache.commons.codec.digest.DigestUtils;

import java.math.BigInteger;

public class FingerTable {
    public static final int INVALID_INDEX = 0;
    private final int sha1Size;
    // Starts at one
    private Finger[] fingers;
    private int current_idx;

    public FingerTable() {
        this.sha1Size = DigestUtils.sha1("").length * 8;
        this.fingers = new Finger[this.sha1Size + 1];
        this.current_idx = 2;
    }

    public static BigInteger generate(String h) {
        return new BigInteger(1, DigestUtils.sha1(h));
    }

    public static BigInteger generate(Host h) {
        return generate(h.toString());
    }

    private BigInteger generateStart(BigInteger n, int k) {
        return n.add(BigInteger.TWO.pow(k + 1)).mod(BigInteger.TWO.pow(this.sha1Size));
    }

    public Finger getSuccessor() {
        return this.fingers[1];
    }

    public Finger get(int k) {
        if (k == 0) throw new RuntimeException("no no no : v > 0");
        return this.fingers[k];
    }

    public void set(int k, Finger newFinger) {
        if (k == 0) throw new RuntimeException("no no no : v > 0");
        this.fingers[k] = newFinger;
    }

    public void init(Host h) {
        BigInteger n = generate(h);

        // only used for implementation convenience not for algorithmic purposes
        fingers[0] = new Finger(n, n, h);

        for (int i = 1; i <= this.sha1Size; i++) {
            BigInteger start = generateStart(n, i);
            this.fingers[i] = new Finger(n, start, h);
        }
    }

    public int getNextIndex() {
        // return new Random().nextInt(getSha1Size() - 1) + 2;
        current_idx++;

        if (current_idx >= sha1Size) {
            current_idx = 2;
        }

        return current_idx;
    }

    public int getSha1Size() {
        return this.sha1Size;
    }
}
