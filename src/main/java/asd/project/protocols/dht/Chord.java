package asd.project.protocols.dht;

import asd.project.protocols.dht.helper.Context;
import asd.project.protocols.dht.helper.ExecutionStage;
import asd.project.protocols.dht.messages.*;
import asd.project.protocols.dht.requests.*;
import asd.project.protocols.dht.timers.FixFingersTimer;
import asd.project.protocols.dht.timers.RetryInitialConnection;
import asd.project.protocols.dht.timers.StabilizeTimer;
import asd.project.protocols.disseminate.Disseminate;
import asd.project.utils.Message;
import asd.project.utils.Utils;
import babel.exceptions.DestinationProtocolDoesNotExist;
import babel.exceptions.HandlerRegistrationException;
import babel.handlers.ProtocolMessageHandler;
import babel.handlers.ProtocolRequestHandler;
import babel.handlers.ProtocolTimerHandler;
import babel.protocol.GenericProtocol;
import babel.protocol.event.ProtocolMessage;
import babel.requestreply.ProtocolRequest;
import babel.timer.ProtocolTimer;
import network.Host;
import network.INetwork;
import network.INodeListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

public class Chord extends GenericProtocol implements INodeListener {

    //Numeric identifier of the protocol
    public final static short PROTOCOL_ID = 400;
    public final static String PROTOCOL_NAME = "Chord";
    private static final Logger logger = LogManager.getLogger(Chord.class);
    private final ProtocolMessageHandler uponRouteMessageDelivery = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            RouteMessageDelivery request = (RouteMessageDelivery) msg;

            Message message = request.getMessage();

            logger.info(String.format("(%d) -> got route message (%s) from (%d) ",
                    myself.getPort(), message, request.getFrom().getPort()));

            RouteDelivery notification = new RouteDelivery(request.getID(), message, request.getSender());
            logger.debug("CHORD TRIGGERING ROUTE DELIVERY TYPE " + notification.getMessage().getType());
            triggerNotification(notification);
        }
    };
    private final ProtocolMessageHandler uponPredecessorForwardRequest = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            PredecessorForwardRequest req = (PredecessorForwardRequest) msg;

            logger.info(String.format("(%d) -> got predecessor forward request from (%d) with id %s",
                    myself.getPort(), req.getFrom().getPort(), req.getRequestId().toString()));

            ForwardDelivery delivery = new ForwardDelivery(req.getSender(), req.getRequestId(), req.getMsg());
            triggerNotification(delivery);
        }
    };
    private Map<UUID, Context> contextMap;
    private Finger predecessor;
    private final ProtocolMessageHandler uponSuccessorPredecessorRequest = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            // Return my predecessor to origin,  Update my predecessor with one received in request
            SuccessorPredecessorRequest request = (SuccessorPredecessorRequest) msg;

            logger.debug(String.format("(%d) -> got successor predecessor request from (%d)",
                    myself.getPort(), request.getFrom().getPort()));

            SuccessorPredecessorReply req;
            if (predecessor != null)
                req = new SuccessorPredecessorReply(predecessor, UUID.randomUUID());
            else
                req = new SuccessorPredecessorReply(Finger.nullFinger(myself), UUID.randomUUID());

            sendMessageCorrectChannel(req, request.getFrom());

        }
    };
    private Finger myFinger;
    private final ProtocolMessageHandler uponNotifyRequest = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            NotifyRequest request = (NotifyRequest) msg;

            logger.info(String.format("(%d) -> got notify request from (%d) with id %s",
                    myself.getPort(), request.getFrom().getPort(), request.getID().getNode().toString()));

            if (predecessor == null || Utils.betweenOpen(request.getID().getNode(), predecessor.getNode(), myFinger.getNode())) {
                if (predecessor != null) {
                    removeNetworkPeer(predecessor.getHost());
                }
                predecessor = request.getID();
                addNetworkPeer(predecessor.getHost());
            }
        }
    };
    private FingerTable fingers;
    private final ProtocolMessageHandler uponPredecessorRequest = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            PredecessorRequest request = (PredecessorRequest) msg;

            logger.info(String.format("(%d) -> got predecessor request from (%d) with id %s",
                    myself.getPort(), request.getFrom().getPort(), request.getRequestId().toString()));

            sendRouteMessage(request, request.getRequestId());
        }
    };
    private final ProtocolMessageHandler uponSuccessorPredecessorReply = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            SuccessorPredecessorReply request = (SuccessorPredecessorReply) msg;

            logger.debug(String.format("(%d) -> got successor predecessor reply from (%d) with: id %s; context %s;",
                    myself.getPort(), request.getFrom().getPort(),
                    request.getPredecessorId() == null ? "null" : request.getPredecessorId().getNode().toString(),
                    contextMap.get(request.getUuid()) == null ? "null" : "not null"));

            Finger x = request.getPredecessorId();
            if (x != null && x.getNode().compareTo(new BigInteger(String.valueOf(-1))) != 0) {
                if (Utils.betweenOpen(x.getNode(), myFinger.getNode(), fingers.getSuccessor().getNode())) {
                    removeNetworkPeer(fingers.getSuccessor().getHost());
                    fingers.set(1, x);
                    addNetworkPeer(x.getHost());
                }
            }

            NotifyRequest notify = new NotifyRequest(myFinger, UUID.randomUUID());
            sendMessage(notify, fingers.getSuccessor().getHost());
            logger.debug("[chord protocol message] (network) " + myself.getPort());
        }
    };
    private Host CONTACT;
    private UUID CONTACT_UUID;
    private UUID CONTACT_TIMER;
    private final ProtocolMessageHandler uponPredecessorReply = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            PredecessorReply reply = (PredecessorReply) msg;

            Context context = contextMap.remove(reply.getUuid());

            logger.info(String.format("(%d) -> got predecessor reply from (%d) with: predecessor %s; successor %s; label %s; index %s;",
                    myself.getPort(), reply.getFrom().getPort(), reply.getPredecessorId().getNode().toString(), reply.getSuccessorId().getNode().toString(),
                    context.getLabel(), context.getIndex()));

            switch (context.getLabel()) {
                case CONCURRENT_JOIN:
                    clearTimer();
                    removeNetworkPeer(fingers.getSuccessor().getHost());
                    fingers.set(1, reply.getSuccessorId());
                    addNetworkPeer(reply.getSuccessorId().getHost());
                    break;
                case FIX_FINGERS:
                    removeNetworkPeer(fingers.get(context.getIndex()).getHost());
                    fingers.set(context.getIndex(), reply.getSuccessorId());
                    addNetworkPeer(reply.getSuccessorId().getHost());
                    break;

                case ROUTE:
                    RouteMessageDelivery del = new RouteMessageDelivery(context.getID(), context.getMessage(), myself);
                    logger.debug("CHORD SENDING ROUTE MESSAGE DELIVERY TO " + reply.getSuccessorId().getHost());
                    sendMessageCorrectChannel(del, reply.getSuccessorId().getHost());
                    break;
                default:
                    throw new RuntimeException("Unknown Execution Stage");
            }
        }
    };
    private ProtocolRequestHandler uponRouteRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest request) {
            RouteRequest req = (RouteRequest) request;

            UUID uuid = UUID.randomUUID();
            Context context = new Context(ExecutionStage.ROUTE);
            context.setID(req.getID());
            context.setMessage(req.getMessage());
            context.setSenderId(req.getSenderID());
            contextMap.put(uuid, context);
            PredecessorRequest pred = new PredecessorRequest(req.getID(), myself, uuid);
            sendRouteMessage(pred, pred.getRequestId());
        }
    };
    private ProtocolRequestHandler uponForwardRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest request) {
            ForwardRequest req = (ForwardRequest) request;

            Finger destination = null;

            if (Utils.betweenLeftClosed(req.getID(), predecessor.getNode(), myFinger.getNode())) {
                logger.warn("FORWARD REQUEST TO MYSELF");
                RouteDelivery delivery = new RouteDelivery(req.getID(), req.getMessage(), myself);
                triggerNotification(delivery);
                return;
            }

            // if reqID not in ]myself, successor]
            if (!Utils.betweenRightClosed(req.getID(), myFinger.getNode(), fingers.getSuccessor().getNode())) {
                Finger nLine = closestPrecedingFinger(req.getID());

                if (nLine.getNode().compareTo(myFinger.getNode()) == 0) {
                    return;
                }

                destination = nLine;
                PredecessorForwardRequest predRequest = new PredecessorForwardRequest(req.getID(), req.getMessage(), myself);
                sendMessageCorrectChannel(predRequest, destination.getHost());

            } else {
                destination = fingers.getSuccessor();
                RouteMessageDelivery routeMessageDelivery = new RouteMessageDelivery(req.getID(), req.getMessage(), myself);
                sendMessage(routeMessageDelivery, destination.getHost());
                logger.debug("[chord protocol message] (network) " + myself.getPort());
            }

            ForwardInfo info = new ForwardInfo(destination.getHost(), req.getID(), req.getMessage());
            info.setDestination(Disseminate.PROTOCOL_ID);
            try {
                sendReply(info);
            } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                destinationProtocolDoesNotExist.printStackTrace();
                System.exit(1);
            }
        }
    };
    private ProtocolTimerHandler uponRetryInitialConnection = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {

            if (CONTACT != null && CONTACT_UUID != null) {
                PredecessorRequest request = new PredecessorRequest(myFinger.getNode(), myself, CONTACT_UUID);
                sendMessageSideChannel(request, CONTACT);
                logger.warn("RETRY RETRY:::::: " + CONTACT.toString());
                logger.debug("[chord protocol message] (network) " + myself.getPort());
            }

        }
    };
    private ProtocolTimerHandler uponStabilizeTimer = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            StabilizeTimer stabilizeTimer = (StabilizeTimer) timer;

            logger.debug(String.format("(%d) -> got stabilize timer", myself.getPort()));

            SuccessorPredecessorRequest req = new SuccessorPredecessorRequest(UUID.randomUUID());
            addNetworkPeer(fingers.getSuccessor().getHost());
            sendMessage(req, fingers.getSuccessor().getHost());
            logger.debug("[chord protocol message] (network) " + myself.getPort());

            printState();
        }
    };
    private ProtocolTimerHandler uponFixFingersTimer = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            FixFingersTimer fixFingersTimer = (FixFingersTimer) timer;

            int i = fingers.getNextIndex();

            if (i == FingerTable.INVALID_INDEX) {
                logger.warn(String.format("(%d) -> empty finger table", myself.getPort()));
                return;
            }
            logger.debug(String.format("(%d) -> got fix fingers timer with random i %d", myself.getPort(), i));

            UUID uuid = UUID.randomUUID();
            Context context = new Context(ExecutionStage.FIX_FINGERS);
            context.setIndex(i);
            contextMap.put(uuid, context);
            PredecessorRequest req = new PredecessorRequest(fingers.get(i).getStart(), myself, uuid);
            sendRouteMessage(req, req.getRequestId());
        }
    };

    public Chord(INetwork net) throws HandlerRegistrationException {
        super(PROTOCOL_NAME, PROTOCOL_ID, net);

        //Declare Messages sent/received of the protocol
        registerMessageHandler(PredecessorRequest.MSG_CODE, uponPredecessorRequest, PredecessorRequest.serializer);
        registerMessageHandler(PredecessorReply.MSG_CODE, uponPredecessorReply, PredecessorReply.serializer);

        registerMessageHandler(SuccessorPredecessorRequest.MSG_CODE, uponSuccessorPredecessorRequest, SuccessorPredecessorRequest.serializer);
        registerMessageHandler(SuccessorPredecessorReply.MSG_CODE, uponSuccessorPredecessorReply, SuccessorPredecessorReply.serializer);

        registerMessageHandler(NotifyRequest.MSG_CODE, uponNotifyRequest, NotifyRequest.serializer);
        registerMessageHandler(RouteMessageDelivery.MSG_CODE, uponRouteMessageDelivery, RouteMessageDelivery.serializer);

        registerMessageHandler(PredecessorForwardRequest.MSG_CODE, uponPredecessorForwardRequest, PredecessorForwardRequest.serializer);

        // Declare Requests
        registerRequestHandler(RouteRequest.REQUEST_ID, uponRouteRequest);
        registerRequestHandler(ForwardRequest.REQUEST_ID, uponForwardRequest);

        registerNotification(ForwardDelivery.NOTIFICATION_ID, ForwardDelivery.NOTIFICATION_NAME);
        registerNotification(RouteDelivery.NOTIFICATION_ID, RouteDelivery.NOTIFICATION_NAME);

        registerTimerHandler(StabilizeTimer.TimerCode, uponStabilizeTimer);
        registerTimerHandler(FixFingersTimer.TimerCode, uponFixFingersTimer);
        registerTimerHandler(RetryInitialConnection.TimerCode, uponRetryInitialConnection);
    }

    @Override
    public void nodeDown(Host host) {
        logger.warn("NODE DOWN :::: " + host);

        removeNetworkPeer(host);
        if (host.compareTo(predecessor.getHost()) == 0)
            predecessor = null;

        if (host.compareTo(fingers.getSuccessor().getHost()) == 0) {
            fingers.set(1, myFinger);
            UUID uuid = UUID.randomUUID();
            Context context = new Context(ExecutionStage.FIX_FINGERS);
            context.setIndex(1);
            contextMap.put(uuid, context);
            PredecessorRequest req = new PredecessorRequest(fingers.get(1).getStart(), myself, uuid);
            sendRouteMessage(req, req.getRequestId());
        }
    }

    @Override
    public void nodeUp(Host host) {
        logger.info("NODE UP ::: " + host);
    }

    @Override
    public void nodeConnectionReestablished(Host host) {
    }

    private Finger closestPrecedingFinger(BigInteger id) {
        for (int i = this.fingers.getSha1Size(); i >= 1; i--) {
            BigInteger node = this.fingers.get(i).getNode();

            if (Utils.betweenOpen(node, this.myFinger.getNode(), id)) {
                return this.fingers.get(i);
            }
        }
        return myFinger;
    }

    private void clearTimer() {
        if (CONTACT != null || CONTACT_UUID != null) {
            CONTACT = null;
            CONTACT_UUID = null;
            cancelTimer(CONTACT_TIMER);
            CONTACT_TIMER = null;
        }

    }

    private void sendRouteMessage(PredecessorRequest message, BigInteger reqID) {
        // if reqID not in ]myself, successor]
        if (!Utils.betweenRightClosed(reqID, myFinger.getNode(), fingers.getSuccessor().getNode())) {
            Finger nLine = closestPrecedingFinger(reqID);

            if (nLine.getNode().compareTo(myFinger.getNode()) != 0) {
                sendMessage(message, nLine.getHost());
                logger.debug("[chord protocol message] (network) " + myself.getPort());
                return;
            }
        }

        PredecessorReply reply = new PredecessorReply(myFinger, fingers.getSuccessor(), message.getUuid());
        sendMessageCorrectChannel(reply, message.getOriginalSender());
    }

    @Override
    public void init(Properties props) {
        registerNodeListener(this);

        this.contextMap = new HashMap<>();
        this.fingers = new FingerTable();
        this.fingers.init(myself);
        this.predecessor = null;
        BigInteger id = FingerTable.generate(myself);
        this.myFinger = new Finger(id, id, myself);

        if (props.containsKey("Contact")) {
            try {
                String[] hostElems = props.getProperty("Contact").split(":");

                this.CONTACT = new Host(InetAddress.getByName(hostElems[0]), Short.parseShort(hostElems[1]));
                this.CONTACT_UUID = UUID.randomUUID();
                logger.debug("CONTACT::::: " + CONTACT.toString());

                contextMap.put(CONTACT_UUID, new Context(ExecutionStage.CONCURRENT_JOIN));
                PredecessorRequest request = new PredecessorRequest(myFinger.getNode(), myself, CONTACT_UUID);
                sendMessageSideChannel(request, CONTACT);
                logger.debug("[chord protocol message] (network) " + myself.getPort());

            } catch (Exception e) {
                e.printStackTrace();
                logger.warn("Invalid contact on configuration: " + props.getProperty("Contact"));
            }
        }

        setupPeriodicTimer(
                new StabilizeTimer(),
                Long.parseLong(props.getProperty("StabilizedTimerPeriod")),
                Long.parseLong(props.getProperty("StabilizedTimerPeriod"))
        );
        setupPeriodicTimer(
                new FixFingersTimer(),
                Long.parseLong(props.getProperty("FixFingerTimerPeriod")),
                Long.parseLong(props.getProperty("FixFingerTimerPeriod"))
        );

        CONTACT_TIMER = setupPeriodicTimer(
                new RetryInitialConnection(),
                5000, 5000
        );

    }

    private void sendMessageCorrectChannel(ProtocolMessage msg, Host to) {
        if (network.isConnectionActive(to)) {
            sendMessage(msg, to);
        } else {
            sendMessageSideChannel(msg, to);
        }
        logger.debug("[chord protocol message] (network) " + myself.getPort());
    }

    private void printState() {
        if (predecessor == null || predecessor == myFinger || fingers.getSuccessor() == null || fingers.getSuccessor() == myFinger) {
            System.err.println("\n/////////////////////////////////////////////////////");
            if (predecessor != null) {
                System.err.println("PRED " + predecessor.getNode() + " " + predecessor.getHost());
            }
            if (myFinger != null) {
                System.err.println("MY " + myFinger.getNode() + " " + myFinger.getHost());
            }
            if (fingers.getSuccessor() != null) {
                System.err.println("SUC " + fingers.getSuccessor().getNode() + " " + fingers.getSuccessor().getHost());
            }
            System.err.println("/////////////////////////////////////////////////////\n");
        }
    }
}
