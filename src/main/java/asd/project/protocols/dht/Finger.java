package asd.project.protocols.dht;

import io.netty.buffer.ByteBuf;
import network.Host;

import java.io.Serializable;
import java.math.BigInteger;
import java.net.UnknownHostException;

public class Finger implements Serializable {

    private BigInteger start;
    private BigInteger node;
    private Host host;

    public Finger(BigInteger node, BigInteger start, Host host) {
        this.node = node;
        this.start = start;
        this.host = host;
    }

    public static Finger deserialize(ByteBuf in) throws UnknownHostException {
        Host host = Host.deserialize(in);

        int startBytesLen = in.readInt();
        byte[] startBytes = new byte[startBytesLen];
        in.readBytes(startBytes);

        int nodeBytesLen = in.readInt();
        byte[] nodeBytes = new byte[nodeBytesLen];
        in.readBytes(nodeBytes);

        return new Finger(new BigInteger(nodeBytes), new BigInteger(startBytes), host);
    }

    public static Finger nullFinger(Host h) {
        return new Finger(new BigInteger(String.valueOf(-1)), new BigInteger(String.valueOf(-1)), h);
    }

    public BigInteger getNode() {
        return this.node;
    }

    public void setNode(BigInteger node) {
        this.node = node;
    }

    public BigInteger getStart() {
        return this.start;
    }

    public void setStart(BigInteger start) {
        this.start = start;
    }

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    public void serialize(ByteBuf out) {
        this.host.serialize(out);

        byte[] startBytes = start.toByteArray();
        out.writeInt(startBytes.length);
        out.writeBytes(startBytes);

        byte[] nodeBytes = node.toByteArray();
        out.writeInt(nodeBytes.length);
        out.writeBytes(nodeBytes);
    }

    public int serializedSize() {
        return this.host.serializedSize() + this.start.toByteArray().length + this.node.toByteArray().length + Integer.BYTES * 2;
    }
}
