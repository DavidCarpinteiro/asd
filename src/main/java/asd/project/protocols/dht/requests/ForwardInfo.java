package asd.project.protocols.dht.requests;

import asd.project.utils.Message;
import babel.requestreply.ProtocolReply;
import network.Host;

import java.math.BigInteger;

public class ForwardInfo extends ProtocolReply {

    public static final short REPLY_ID = 412;

    private Host nextHost;
    private BigInteger msgID;
    private Message message;

    public ForwardInfo(Host nextHost, BigInteger msgID, Message message) {
        super(ForwardInfo.REPLY_ID);
        this.nextHost = nextHost;
        this.msgID = msgID;
        this.message = message;
    }

    public Host getNextHost() {
        return nextHost;
    }

    public BigInteger getMsgID() {
        return msgID;
    }

    public Message getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return String.format("ChordForwardInfo{NextID=%s, MsgID=%s, message=%s}", nextHost, msgID, message);
    }
}
