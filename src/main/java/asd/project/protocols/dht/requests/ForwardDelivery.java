package asd.project.protocols.dht.requests;

import asd.project.utils.Message;
import babel.notification.ProtocolNotification;
import network.Host;

import java.math.BigInteger;

public class ForwardDelivery extends ProtocolNotification {
    public static final short NOTIFICATION_ID = 401;
    public static final String NOTIFICATION_NAME = "ForwardDelivery";

    private Host sender;
    private BigInteger msgID;
    private Message message;

    public ForwardDelivery(Host sender, BigInteger msgID, Message message) {
        super(ForwardDelivery.NOTIFICATION_ID, ForwardDelivery.NOTIFICATION_NAME);
        this.sender = sender;
        this.msgID = msgID;
        this.message = message;
    }

    public Host getHostSender() {
        return sender;
    }

    public BigInteger getMsgID() {
        return msgID;
    }

    public Message getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return String.format("ChordForwardDelivery{NextID=%s, MsgID=%s, message=%s}", sender, msgID, message);
    }
}
