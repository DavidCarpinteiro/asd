package asd.project.protocols.dht.requests;

import asd.project.utils.Message;
import babel.notification.ProtocolNotification;
import network.Host;

import java.math.BigInteger;

public class RouteDelivery extends ProtocolNotification {
    public static final short NOTIFICATION_ID = 402;
    public static final String NOTIFICATION_NAME = "RouteDelivery";

    private BigInteger ID;
    private Message message;
    private Host sender;

    public RouteDelivery(BigInteger ID, Message message, Host sender) {
        super(RouteDelivery.NOTIFICATION_ID, RouteDelivery.NOTIFICATION_NAME);
        this.ID = ID;
        this.message = message;
        this.sender = sender;
    }

    public Host getHostSender() {
        return sender;
    }

    public BigInteger getID() {
        return ID;
    }

    public Message getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return String.format("RouteDelivery{ID=%s, message=%s, sender=%s}", ID, message, sender);
    }
}
