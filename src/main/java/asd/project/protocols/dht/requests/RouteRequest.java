package asd.project.protocols.dht.requests;

import asd.project.utils.Message;
import babel.requestreply.ProtocolRequest;

import java.math.BigInteger;

public class RouteRequest extends ProtocolRequest {

    public final static short REQUEST_ID = 410;
    private BigInteger ID;
    private Message message;

    public RouteRequest(BigInteger ID, Message message) {
        super(REQUEST_ID);
        this.ID = ID;
        this.message = message;
    }

    public BigInteger getID() {
        return ID;
    }

    public Message getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return String.format("ChordRouteRequest{ID=%s, message=%s}", ID, message);
    }
}