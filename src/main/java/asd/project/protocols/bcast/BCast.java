package asd.project.protocols.bcast;

import asd.project.protocols.bcast.messages.BCastMessage;
import asd.project.protocols.bcast.messages.BCastOneHopMessage;
import asd.project.protocols.bcast.messages.BCastRecoveryMessage;
import asd.project.protocols.bcast.notifications.BCastDeliver;
import asd.project.protocols.bcast.notifications.BCastOneHopDeliver;
import asd.project.protocols.bcast.requests.BCastOneHopRequest;
import asd.project.protocols.bcast.requests.BCastRequest;
import asd.project.protocols.bcast.timers.RecoveryTimer;
import asd.project.protocols.hyparview.HyParView;
import asd.project.protocols.hyparview.requests.GetMembershipReply;
import asd.project.protocols.hyparview.requests.GetMembershipRequest;
import asd.project.utils.Pair;
import asd.project.utils.Utils;
import babel.exceptions.DestinationProtocolDoesNotExist;
import babel.exceptions.HandlerRegistrationException;
import babel.handlers.ProtocolMessageHandler;
import babel.handlers.ProtocolReplyHandler;
import babel.handlers.ProtocolRequestHandler;
import babel.handlers.ProtocolTimerHandler;
import babel.protocol.GenericProtocol;
import babel.protocol.event.ProtocolMessage;
import babel.requestreply.ProtocolReply;
import babel.requestreply.ProtocolRequest;
import babel.timer.ProtocolTimer;
import network.Host;
import network.INetwork;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class BCast extends GenericProtocol {
    public final static short PROTOCOL_ID = 200;
    public final static String PROTOCOL_NAME = "BCast";
    private static final Logger logger = LogManager.getLogger(BCast.class);
    //Protocol State
    private Map<UUID, BCastMessage> receivedMessages;
    private Map<UUID, BCastMessage> ghostMessages;
    private Set<BCastMessage> pendingMessages;
    private Set<BCastRecoveryMessage> pendingRecoveries;
    private Set<BCastOneHopMessage> pendingOneHop;

    private int bCastMessageTTL;
    private int ghostMessageTTL;
    private ProtocolRequestHandler uponBCastOneHopRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest r) {
            BCastOneHopRequest request = (BCastOneHopRequest) r;

            logger.info("({}): received {} from ({})", myself, request, request.getSender());

            Map<String, Pair<Boolean, Integer>> infoMap = request.getInfoMap();
            BCastOneHopMessage bCastMessage = new BCastOneHopMessage(infoMap);

            pendingOneHop.add(bCastMessage);

            GetMembershipRequest membershipRequest = new GetMembershipRequest();
            membershipRequest.setDestination(HyParView.PROTOCOL_ID);

            try {
                logger.info("({}): sending membership request", myself);
                sendRequest(membershipRequest);
            } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                destinationProtocolDoesNotExist.printStackTrace();
                System.exit(1);
            }
        }
    };
    private ProtocolReplyHandler uponGetMembershipReplyHandler = new ProtocolReplyHandler() {
        @Override
        public void uponReply(ProtocolReply r) {
            GetMembershipReply reply = (GetMembershipReply) r;

            logger.info("({}): received {} from ({})", myself, reply, reply.getSender());

            pendingMessages.forEach(message -> {
                Host sender = message.getSender();
                message.setSender(myself);
                reply.getActiveView().stream()
                        .filter(host -> !host.equals(sender))
                        .forEach(host -> {
                            sendMessage(message, host);
                            logger.debug("[bcast  protocol message] (network) " + myself.getPort());
                        });
            });
            pendingMessages.clear();

            pendingRecoveries.forEach(recoveryMessage -> {
                Host node = Utils.getRandomElementFromSet(reply.getActiveView());
                if (node != null) {
                    logger.info("({}): sending {} to ({})", myself, recoveryMessage, node);
                    sendMessage(recoveryMessage, node);
                    logger.debug("[bcast  protocol message] (network) " + myself.getPort());
                }
            });
            pendingRecoveries.clear();

            pendingOneHop.forEach(message -> {
                reply.getActiveView()
                        .forEach(host -> {
                            sendMessage(message, host);
                            logger.debug("[bcast  protocol message] (network) " + myself.getPort());
                        });
            });
            pendingOneHop.clear();
        }
    };

    private ProtocolRequestHandler uponBCastRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest r) {
            BCastRequest request = (BCastRequest) r;

            logger.info("({}): received {} from ({})", myself, request, request.getSender());

            long seqNum = request.getSeqNum();
            String topic = request.getTopic();
            String message = request.getMessage();

            BCastMessage bCastMessage = new BCastMessage(seqNum, topic, message, myself);

            GetMembershipRequest membershipRequest = new GetMembershipRequest();
            membershipRequest.setDestination(HyParView.PROTOCOL_ID);

            receivedMessages.put(bCastMessage.getMessageId(), bCastMessage);

            BCastDeliver deliver = new BCastDeliver(seqNum, topic, message);
            logger.info("({}): broadcast deliver {}", myself, deliver);
            triggerNotification(deliver);

            pendingMessages.add(bCastMessage);

            try {
                logger.info("({}): sending membership request", myself);
                sendRequest(membershipRequest);
            } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                destinationProtocolDoesNotExist.printStackTrace();
                System.exit(1);
            }
        }
    };
    private ProtocolMessageHandler uponBCastOneHopMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage m) {
            BCastOneHopMessage msg = (BCastOneHopMessage) m;
            triggerNotification(new BCastOneHopDeliver(msg.getInfoMap()));
        }
    };
    private ProtocolMessageHandler uponBCastMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage m) {
            BCastMessage msg = (BCastMessage) m;
            if (!ghostMessages.containsKey(msg.getMessageId())) {
                if (receivedMessages.putIfAbsent(msg.getMessageId(), msg) == null) {
                    logger.info("({}): received {} from {}", myself, msg, msg.getFrom());
                    triggerNotification(new BCastDeliver(msg.getSeqNum(), msg.getTopic(), msg.getMessage()));

                    pendingMessages.add(msg);
                    GetMembershipRequest request = new GetMembershipRequest();
                    request.setDestination(HyParView.PROTOCOL_ID);

                    try {
                        logger.info("({}): sending membership request", myself);
                        sendRequest(request);
                    } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                        destinationProtocolDoesNotExist.printStackTrace();
                        System.exit(1);
                    }
                }
            }
        }
    };
    private ProtocolTimerHandler uponRecoveryTimerHandler = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            purgeExpired();

            BCastRecoveryMessage message = new BCastRecoveryMessage(myself, receivedMessages.keySet());
            pendingRecoveries.add(message);

            GetMembershipRequest membershipRequest = new GetMembershipRequest();
            membershipRequest.setDestination(HyParView.PROTOCOL_ID);

            try {
                logger.info("({}): sending membership request for recovery", myself);
                sendRequest(membershipRequest);
            } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                destinationProtocolDoesNotExist.printStackTrace();
                System.exit(1);
            }
        }
    };
    private ProtocolMessageHandler uponBCastRecoveryMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage pm) {
            BCastRecoveryMessage message = (BCastRecoveryMessage) pm;
            logger.info("({}): received {}", myself, message);

            purgeExpired();

            Host sender = message.getSender();
            Set<UUID> knownMessages = message.getMessageIDs();

            receivedMessages.forEach((mid, m) -> {
                if (!knownMessages.contains(mid)) {
                    logger.info("({}) -> {} sending recovery reply message", myself, sender);
                    m.setSender(myself);
                    sendMessage(m, sender);
                    logger.debug("[bcast  protocol message] (network) " + myself.getPort());
                }
            });
        }
    };

    public BCast(INetwork net) throws HandlerRegistrationException {
        super(PROTOCOL_NAME, PROTOCOL_ID, net);

        //Register Events

        //Requests
        registerRequestHandler(BCastRequest.REQUEST_ID, uponBCastRequest);
        registerRequestHandler(BCastOneHopRequest.REQUEST_ID, uponBCastOneHopRequest);

        //Replies
        registerReplyHandler(GetMembershipReply.REPLY_ID, uponGetMembershipReplyHandler);

        //Notifications Produced
        registerNotification(BCastDeliver.NOTIFICATION_ID, BCastDeliver.NOTIFICATION_NAME);
        registerNotification(BCastOneHopDeliver.NOTIFICATION_ID, BCastOneHopDeliver.NOTIFICATION_NAME);

        //Messages
        registerMessageHandler(BCastMessage.MSG_CODE, uponBCastMessage, BCastMessage.serializer);
        registerMessageHandler(BCastRecoveryMessage.MSG_CODE, uponBCastRecoveryMessage, BCastRecoveryMessage.serializer);
        registerMessageHandler(BCastOneHopMessage.MSG_CODE, uponBCastOneHopMessage, BCastOneHopMessage.serializer);

        //Timers
        registerTimerHandler(RecoveryTimer.TimerCode, uponRecoveryTimerHandler);
    }

    @Override
    public void init(Properties props) {
        //Initialize State
        this.receivedMessages = new HashMap<>(100);
        this.ghostMessages = new HashMap<>(100);
        this.pendingMessages = new HashSet<>(100);
        this.pendingRecoveries = new HashSet<>(100);
        this.pendingOneHop = new HashSet<>(100);

        //setup timers
        UUID recoveryTimerUUID = setupPeriodicTimer(
                new RecoveryTimer(),
                Long.parseLong(props.getProperty("RecoveryTimerFirst")),
                Long.parseLong(props.getProperty("RecoveryTimerPeriod"))
        );

        this.bCastMessageTTL = Integer.parseInt(props.getProperty("BCastMessageTTL"));
        this.ghostMessageTTL = Integer.parseInt(props.getProperty("GhostMessageTTL"));
    }

    private void purgeExpired() {
        long firstMoment = System.currentTimeMillis();
        ghostMessages.entrySet().removeIf((entry) ->
                firstMoment - entry.getValue().getTimestamp() >= ghostMessageTTL
        );

        long secondMoment = System.currentTimeMillis();

        Iterator<UUID> it = receivedMessages.keySet().iterator();
        while (it.hasNext()) {
            UUID id = it.next();
            BCastMessage msg = receivedMessages.get(id);
            if (secondMoment - msg.getTimestamp() >= bCastMessageTTL) {
                it.remove();
                ghostMessages.put(id, msg);
            }
        }
    }
}
