package asd.project.protocols.bcast.requests;

import asd.project.utils.Pair;
import babel.requestreply.ProtocolRequest;

import java.util.Map;

public class BCastOneHopRequest extends ProtocolRequest {

    public static final short REQUEST_ID = 202;

    private Map<String, Pair<Boolean, Integer>> infoMap;

    public BCastOneHopRequest(Map<String, Pair<Boolean, Integer>> infoMap) {
        super(BCastOneHopRequest.REQUEST_ID);
        this.infoMap = infoMap;
    }

    public Map<String, Pair<Boolean, Integer>> getInfoMap() {
        return infoMap;
    }

    @Override
    public String toString() {
        return String.format("BCastOneHopRequest{map=%s}", infoMap);
    }
}
