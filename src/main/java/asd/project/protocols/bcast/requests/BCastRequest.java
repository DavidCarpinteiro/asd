package asd.project.protocols.bcast.requests;

import babel.requestreply.ProtocolRequest;

public class BCastRequest extends ProtocolRequest {

    public static final short REQUEST_ID = 201;

    private long seqNum;
    private String topic;
    private String message;

    public BCastRequest(long seqNum, String topic, String message) {
        super(BCastRequest.REQUEST_ID);
        this.seqNum = seqNum;
        this.topic = topic;
        this.message = message;

    }

    public long getSeqNum() {
        return seqNum;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return String.format("BCastRequest{seqNum=%d, topic=%s, msg=%s}", seqNum, topic, message);
    }
}
