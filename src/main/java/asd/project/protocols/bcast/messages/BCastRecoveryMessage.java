package asd.project.protocols.bcast.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class BCastRecoveryMessage extends ProtocolMessage {
    public final static short MSG_CODE = 202;
    public static final ISerializer<BCastRecoveryMessage> serializer = new ISerializer<>() {
        @Override
        public void serialize(BCastRecoveryMessage req, ByteBuf out) {
            req.sender.serialize(out);

            int viewSize = req.messageIDs.size();
            out.writeInt(viewSize);

            for (UUID id : req.messageIDs) {
                out.writeLong(id.getMostSignificantBits());
                out.writeLong(id.getLeastSignificantBits());
            }
        }

        @Override
        public BCastRecoveryMessage deserialize(ByteBuf in) throws UnknownHostException {
            Host peer = Host.deserialize(in);

            int midsSize = in.readInt();
            Set<UUID> mids = new HashSet<>();
            for (int i = 0; i < midsSize; i++)
                mids.add(new UUID(in.readLong(), in.readLong()));

            return new BCastRecoveryMessage(peer, mids);
        }

        @Override
        public int serializedSize(BCastRecoveryMessage req) {
            if (req.size == -1) {
                req.size = 0;
                req.size += req.sender.serializedSize();
                req.size += 4;
                req.size += 2 * 8 * req.messageIDs.size();
            }
            return req.size;
        }
    };
    private final Host sender;
    private final Set<UUID> messageIDs;
    private int size;

    public BCastRecoveryMessage(Host sender, Set<UUID> messageIDs) {
        super(MSG_CODE);
        this.sender = sender;
        this.messageIDs = new HashSet<>(messageIDs);
        this.size = -1;
    }

    public Host getSender() {
        return sender;
    }

    public Set<UUID> getMessageIDs() {
        return messageIDs;
    }

    @Override
    public String toString() {
        return String.format("BCastRecoveryMessage{sender=%s, messageIDs=%s}", sender, messageIDs);
    }
}
