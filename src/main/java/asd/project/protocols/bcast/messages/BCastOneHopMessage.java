package asd.project.protocols.bcast.messages;

import asd.project.utils.Pair;
import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BCastOneHopMessage extends ProtocolMessage {

    public final static short MSG_CODE = 203;
    public static final ISerializer<BCastOneHopMessage> serializer = new ISerializer<>() {
        @Override
        public void serialize(BCastOneHopMessage m, ByteBuf out) {
            out.writeLong(m.mid.getMostSignificantBits());
            out.writeLong(m.mid.getLeastSignificantBits());

            out.writeInt(m.infoMap.size());

            for (Map.Entry<String, Pair<Boolean, Integer>> entry : m.infoMap.entrySet()) {
                String topic = entry.getKey();
                out.writeInt(topic.length());
                out.writeBytes(topic.getBytes());

                boolean popular = entry.getValue().first;
                out.writeByte(popular ? 1 : 0);

                int seqNum = entry.getValue().second;
                out.writeInt(seqNum);
            }
        }

        @Override
        public BCastOneHopMessage deserialize(ByteBuf in) {
            UUID mid = new UUID(in.readLong(), in.readLong());

            int numTopics = in.readInt();
            Map<String, Pair<Boolean, Integer>> infoMap = new HashMap<>(numTopics);

            for (int i = 0; i < numTopics; i++) {
                int topicSize = in.readInt();
                byte[] topicBytes = new byte[topicSize];
                in.readBytes(topicBytes);
                String topic = new String(topicBytes);

                boolean popular = in.readByte() == 1;
                int seqNum = in.readInt();
                infoMap.put(topic, new Pair<>(popular, seqNum));
            }
            return new BCastOneHopMessage(mid, infoMap);
        }

        @Override
        public int serializedSize(BCastOneHopMessage m) {
            if (m.size == -1) {
                m.size = (2 * Long.BYTES) + // Message ID
                        (Integer.BYTES);   // Number Topics

                for (Map.Entry<String, Pair<Boolean, Integer>> entry : m.infoMap.entrySet()) {
                    m.size += Integer.BYTES;
                    m.size += entry.getKey().getBytes().length;

                    m.size += Byte.BYTES;

                    m.size += Integer.BYTES;
                }
            }
            return m.size;
        }
    };
    private final UUID mid;
    private Map<String, Pair<Boolean, Integer>> infoMap;
    private int size;

    public BCastOneHopMessage(Map<String, Pair<Boolean, Integer>> infoMap) {
        this(UUID.randomUUID(), infoMap);
    }

    private BCastOneHopMessage(UUID mid, Map<String, Pair<Boolean, Integer>> infoMap) {
        super(BCastOneHopMessage.MSG_CODE);
        this.mid = mid;
        this.infoMap = infoMap;
        this.size = -1;
    }

    public UUID getMid() {
        return mid;
    }

    public Map<String, Pair<Boolean, Integer>> getInfoMap() {
        return infoMap;
    }

    @Override
    public String toString() {
        return String.format("BCastOneHopMessage{id=%s, map=%s}", mid, infoMap);
    }
}