package asd.project.protocols.bcast.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;
import java.util.UUID;

public class BCastMessage extends ProtocolMessage {

    public final static short MSG_CODE = 201;
    public static final ISerializer<BCastMessage> serializer = new ISerializer<>() {
        @Override
        public void serialize(BCastMessage m, ByteBuf out) {
            out.writeLong(m.mid.getMostSignificantBits());
            out.writeLong(m.mid.getLeastSignificantBits());

            out.writeLong(m.seqNum);

            byte[] topicBytes = m.topic.getBytes();
            byte[] messageBytes = m.message.getBytes();

            int topicLength = topicBytes.length;
            int messageLength = messageBytes.length;

            out.writeInt(topicLength);
            out.writeBytes(topicBytes);

            out.writeInt(messageLength);
            out.writeBytes(messageBytes);

            m.sender.serialize(out);
        }

        @Override
        public BCastMessage deserialize(ByteBuf in) throws UnknownHostException {
            UUID mid = new UUID(in.readLong(), in.readLong());

            long seqNum = in.readLong();

            int topicSize = in.readInt();
            byte[] topicBytes = new byte[topicSize];
            in.readBytes(topicBytes);
            String topic = new String(topicBytes);

            int messageSize = in.readInt();
            byte[] messageBytes = new byte[messageSize];
            in.readBytes(messageBytes);
            String message = new String(messageBytes);

            Host sender = Host.deserialize(in);

            return new BCastMessage(mid, seqNum, topic, message, sender);
        }

        @Override
        public int serializedSize(BCastMessage m) {
            return (3 * Long.BYTES) + // Message ID and Message Sequence number
                    (2 * Integer.BYTES) + // Topic and Message length
                    m.topic.getBytes().length +
                    m.message.getBytes().length +
                    m.sender.serializedSize();
        }
    };
    private final UUID mid;
    private final long timestamp;
    private long seqNum;
    private String topic;
    private String message;
    private Host sender;

    public BCastMessage(long seqNum, String topic, String message, Host sender) {
        this(UUID.randomUUID(), seqNum, topic, message, sender);
    }

    private BCastMessage(UUID mid, long seqNum, String topic, String message, Host sender) {
        super(BCastMessage.MSG_CODE);
        this.mid = mid;
        this.seqNum = seqNum;
        this.topic = topic;
        this.message = message;
        this.sender = sender;
        this.timestamp = System.currentTimeMillis();
    }

    public long getSeqNum() {
        return seqNum;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UUID getMessageId() {
        return mid;
    }

    public Host getSender() {
        return sender;
    }

    public void setSender(Host sender) {
        this.sender = sender;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return String.format("BCastMessage{id=%s, topic=%s, msg=%s}", mid, topic, message);
    }
}