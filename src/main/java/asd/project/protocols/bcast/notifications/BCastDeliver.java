package asd.project.protocols.bcast.notifications;

import babel.notification.ProtocolNotification;

public class BCastDeliver extends ProtocolNotification {

    public static final short NOTIFICATION_ID = 201;
    public static final String NOTIFICATION_NAME = "BCastDeliver";

    private long seqNum;
    private String topic;
    private String message;

    public BCastDeliver(long seqNum, String topic, String message) {
        super(BCastDeliver.NOTIFICATION_ID, BCastDeliver.NOTIFICATION_NAME);
        this.seqNum = seqNum;
        this.topic = topic;
        this.message = message;

    }

    public long getSeqNum() {
        return seqNum;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return String.format("BCastDeliver{seqNum=%d, topic=%s, msg=%s}", seqNum, topic, message);
    }
}
