package asd.project.protocols.bcast.notifications;

import asd.project.utils.Pair;
import babel.notification.ProtocolNotification;

import java.util.Map;

public class BCastOneHopDeliver extends ProtocolNotification {

    public static final short NOTIFICATION_ID = 202;
    public static final String NOTIFICATION_NAME = "BCastOneHopDeliver";

    private Map<String, Pair<Boolean, Integer>> infoMap;

    public BCastOneHopDeliver(Map<String, Pair<Boolean, Integer>> infoMap) {
        super(BCastOneHopDeliver.NOTIFICATION_ID, BCastOneHopDeliver.NOTIFICATION_NAME);

        this.infoMap = infoMap;

    }

    public Map<String, Pair<Boolean, Integer>> getInfoMap() {
        return infoMap;
    }

    @Override
    public String toString() {
        return String.format("BCastOneHopDeliver{map=%s}", infoMap);
    }
}
