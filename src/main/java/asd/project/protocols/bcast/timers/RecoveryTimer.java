package asd.project.protocols.bcast.timers;

import babel.timer.ProtocolTimer;

public class RecoveryTimer extends ProtocolTimer {

    public static final short TimerCode = 201;

    public RecoveryTimer() {
        super(RecoveryTimer.TimerCode);
    }

    @Override
    public Object clone() {
        return this;
    }
}