package asd.project.protocols.pubsub;

import asd.project.protocols.bcast.BCast;
import asd.project.protocols.bcast.notifications.BCastDeliver;
import asd.project.protocols.bcast.notifications.BCastOneHopDeliver;
import asd.project.protocols.bcast.requests.BCastOneHopRequest;
import asd.project.protocols.bcast.requests.BCastRequest;
import asd.project.protocols.dht.Chord;
import asd.project.protocols.dht.FingerTable;
import asd.project.protocols.dht.requests.RouteDelivery;
import asd.project.protocols.dht.requests.RouteRequest;
import asd.project.protocols.disseminate.Disseminate;
import asd.project.protocols.disseminate.requests.DisseminateRequest;
import asd.project.protocols.disseminate.requests.DisseminateSubscribeRequest;
import asd.project.protocols.disseminate.requests.DisseminateUnsubscribeRequest;
import asd.project.protocols.disseminate.requests.MessageDelivery;
import asd.project.protocols.paxos.Paxos;
import asd.project.protocols.paxos.notifications.DecideNotification;
import asd.project.protocols.paxos.notifications.PaxosLeaderChange;
import asd.project.protocols.pubsub.messages.PaxosMembershipReply;
import asd.project.protocols.pubsub.messages.PaxosMessageRangeReply;
import asd.project.protocols.pubsub.messages.PaxosPublish;
import asd.project.protocols.pubsub.notifications.PubSubDeliver;
import asd.project.protocols.pubsub.notifications.RemovePopularity;
import asd.project.protocols.pubsub.notifications.UpdatePopularity;
import asd.project.protocols.pubsub.requests.PublishRequest;
import asd.project.protocols.pubsub.requests.SubscribeRequest;
import asd.project.protocols.pubsub.requests.UnsubscribeRequest;
import asd.project.protocols.pubsub.timers.PopularityTimer;
import asd.project.utils.Message;
import asd.project.utils.Pair;
import asd.project.utils.Proposal;
import asd.project.utils.Storage;
import babel.exceptions.DestinationProtocolDoesNotExist;
import babel.exceptions.HandlerRegistrationException;
import babel.exceptions.NotificationDoesNotExistException;
import babel.exceptions.ProtocolDoesNotExist;
import babel.handlers.*;
import babel.notification.ProtocolNotification;
import babel.protocol.event.ProtocolMessage;
import babel.requestreply.ProtocolReply;
import babel.requestreply.ProtocolRequest;
import babel.timer.ProtocolTimer;
import network.Host;
import network.INetwork;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.util.*;

public class PubSub extends AbstractPubSub {
    // Numeric identifier and name of the protocol.

    private static final Logger logger = LogManager.getLogger(PubSub.class);
    // Set of topics this client is subscribed to.
    private Set<String> topics;
    private int POPULAR_THRESHOLD;
    private int UNPOPULAR_THRESHOLD;
    // Topic popularity, an integer with the number of subscribers to that topic. Only contains the popularity of
    // the topics that this node is responsible for.
    private Map<String, Integer> topicPopularity;
    private Map<String, Pair<Boolean, Integer>> myPopularTopics;
    private Map<String, Pair<Boolean, Integer>> otherPopularTopics;
    private Map<String, SortedSet<Proposal>> otherPendingProposals;
    private Map<String, Long> otherTopicSeqNums;
    private final ProtocolMessageHandler uponPaxosMessageRangeReply = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            PaxosMessageRangeReply reply = (PaxosMessageRangeReply) msg;
            List<Proposal> added;

            if (myPopularTopics.containsKey(reply.getMessages().get(0).getTopic())) {
                added = rangeReplyWriteProcess(pendingTopicsBuffer, myTopicSeqNums, msg, true);
                for (Proposal prop : added) {
                    choosePublishMethod(prop);
                }
            } else {
                added = rangeReplyWriteProcess(otherPendingProposals, otherTopicSeqNums, msg, false);
                for (Proposal prop : added) {
                    PubSubDeliver deliver = new PubSubDeliver(prop.getTopic(), prop.getMessage(), prop.getSeqNum());
                    triggerNotification(deliver);
                }
            }
        }
    };
    private Map<BigInteger, List<PaxosPublish>> pendingPaxosPublishes;
    private Map<BigInteger, String> topicIDtoTopicString;
    private final ProtocolMessageHandler uponPaxosMembershipReply = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            logger.info("({}) -> received PaxosMembershipReply from ({})", myself, msg.getFrom());

            logger.debug("GOT MEMBERSHIP REPLY FROM " + msg.getFrom().getPort());

            PaxosMembershipReply reply = (PaxosMembershipReply) msg;

            Set<Host> membership = reply.getMembership();

            List<PaxosPublish> paxosPublishes = pendingPaxosPublishes.getOrDefault(reply.getID(), new LinkedList<>());

            if (!paxosPublishes.isEmpty()) {
                Host target = (Host) membership.toArray()[new Random().nextInt(membership.size())];

                logger.debug("CHOSEN " + target.getPort() + " OUT OF " + membership.size());
                logger.debug(pendingPaxosPublishes.get(reply.getID()).size() + " IN PENDING PUBLISHES");

                for (PaxosPublish p : paxosPublishes) {
                    logger.debug("SENDING PAXOS PUBLISH TO " + target);
                    logger.debug("CONTENT: TOPIC-" + p.getTopic() + " ; MESSAGE-" + p.getMessage() + " ; SIZE=" + (p.getTopic().length() + p.getMessage().length()));
                    logger.debug("[pubsub protocol message] (network) " + myself.getPort());
                    sendMessageSideChannel(p, target);
                }

                paxosPublishes.clear();
            }

            String topic = topicIDtoTopicString.get(reply.getID());

            if (topic == null) {
                logger.debug("TOPIC IS NULL");
                return;
            }

            SortedSet<Proposal> paxosProposals = otherPendingProposals.getOrDefault(topic, new TreeSet<>());

            if (!paxosProposals.isEmpty()) {
                long myTopicSeqNum = otherTopicSeqNums.getOrDefault(topic, 0L);
                checkForLateTopics(myTopicSeqNum, paxosProposals, membership);
                topicIDtoTopicString.remove(reply.getID());
            }
        }
    };
    private ProtocolNotificationHandler uponRemovePopularity = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification n) {
            RemovePopularity notification = (RemovePopularity) n;
            topicPopularity.remove(notification.getTopic());

            Pair<Boolean, Integer> popularityPair = myPopularTopics.getOrDefault(notification.getTopic(), new Pair<>(false, 0));
            popularityPair = new Pair<>(false, popularityPair.second + 1);
            myPopularTopics.put(notification.getTopic(), popularityPair);
            myPopularTopics.keySet().forEach(logger::warn);
        }
    };
    private ProtocolNotificationHandler uponUpdatePopularity = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification n) {
            UpdatePopularity notification = (UpdatePopularity) n;
            final int futurePopularity = notification.getAmount();
            final String topic = notification.getTopic();

            int currentPopularity = topicPopularity.getOrDefault(topic, 0);

            if (futurePopularity < 0) {
                throw new RuntimeException(
                        "Future popularity of topic " + topic + " is negative (" + currentPopularity + ") -> (" + futurePopularity + ")"
                );
            }
            logger.info("({}): updating popularity of ({}) from ({}) to ({})", myself, topic, currentPopularity, futurePopularity);

            topicPopularity.put(topic, futurePopularity);

            Pair<Boolean, Integer> popularityPair = myPopularTopics.getOrDefault(notification.getTopic(), new Pair<>(false, 0));
            myPopularTopics.putIfAbsent(notification.getTopic(), popularityPair);

            if (currentPopularity < POPULAR_THRESHOLD && futurePopularity >= POPULAR_THRESHOLD) { // Changing from Unpopular to Popular
                popularityPair = new Pair<>(true, popularityPair.second + 1);
                myPopularTopics.put(notification.getTopic(), popularityPair);
                logger.info("TOPIC " + topic + " IS NOW POPULAR");
            } else if (currentPopularity > UNPOPULAR_THRESHOLD && futurePopularity <= UNPOPULAR_THRESHOLD) { // Changing from popular to unpopular
                popularityPair = new Pair<>(false, popularityPair.second + 1);
                myPopularTopics.put(notification.getTopic(), popularityPair);
                logger.info("TOPIC " + topic + " IS NOW UNPOPULAR");
            }
        }
    };
    private ProtocolReplyHandler uponMessageDelivery = new ProtocolReplyHandler() {
        @Override
        public void uponReply(ProtocolReply protocolReply) {
            MessageDelivery delivery = (MessageDelivery) protocolReply;
            String topic = delivery.getTopic();
            String message = delivery.getMessage();
            long seqNum = delivery.getSeqNum();

            checkResponsibleTopicAndRequestMissing(topic, message, seqNum);
        }
    };
    private ProtocolNotificationHandler uponBCastDeliverNotification = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification n) {
            BCastDeliver notification = (BCastDeliver) n;
            String topic = notification.getTopic();
            String message = notification.getMessage();
            long seqNum = notification.getSeqNum();

            logger.info("({}): received {} from ({})", myself, notification, notification.getEmitter());

            checkResponsibleTopicAndRequestMissing(topic, message, seqNum);
        }
    };

    private ProtocolRequestHandler uponSubscribeRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest r) {
            SubscribeRequest request = (SubscribeRequest) r;
            logger.info("({}): received {} from ({})", myself, request, request.getSender());

            String subscribeTopic = request.getTopic();
            topics.add(subscribeTopic);

            DisseminateSubscribeRequest subscribe = new DisseminateSubscribeRequest(subscribeTopic);
            subscribe.setDestination(Disseminate.PROTOCOL_ID);
            try {
                logger.info("({}): sending disseminate subscribe request", myself);
                sendRequest(subscribe);
            } catch (DestinationProtocolDoesNotExist e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    };

    private ProtocolRequestHandler uponUnsubscribeRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest r) {
            UnsubscribeRequest request = (UnsubscribeRequest) r;
            logger.info("({}): received {} from ({})", myself, request, request.getSender());

            String unsubscribeTopic = request.getTopic();
            topics.remove(unsubscribeTopic);

            DisseminateUnsubscribeRequest unsubscribe = new DisseminateUnsubscribeRequest(unsubscribeTopic);
            unsubscribe.setDestination(Disseminate.PROTOCOL_ID);
            try {
                logger.info("({}): sending route request", myself);
                sendRequest(unsubscribe);
            } catch (DestinationProtocolDoesNotExist e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
    };
    private ProtocolRequestHandler uponPublishRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest r) {
            PublishRequest request = (PublishRequest) r;
            logger.info("({}): received {} from ({})", myself, request, request.getSender());

            String topic = request.getTopic();
            String message = request.getMessage();

            PaxosPublish toPublish = new PaxosPublish(topic, message);
            BigInteger id = FingerTable.generate(topic);
            List<PaxosPublish> topicPublishes = pendingPaxosPublishes.getOrDefault(id, new LinkedList<>());
            topicPublishes.add(toPublish);
            pendingPaxosPublishes.putIfAbsent(id, topicPublishes);

            logger.debug("SENDING MEMBERSHIP ROUTE REQUEST");
            RouteRequest membershipRequest = new RouteRequest(id, new Message(Message.MessageType.GET_MEMBERSHIP, myself));
            membershipRequest.setDestination(Chord.PROTOCOL_ID);
            try {
                sendRequest(membershipRequest);
            } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                destinationProtocolDoesNotExist.printStackTrace();
                System.exit(-1);
            }
        }
    };
    private ProtocolNotificationHandler uponDecideNotification = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification n) {
            DecideNotification notification = (DecideNotification) n;
            Proposal proposal = notification.getProposal();

            logger.info("({}): received {} from ({})", myself, notification, notification.getEmitter());

            switch (proposal.getType()) {
                case PUBLISH:
                    Long myTopicSeqNum = myTopicSeqNums.getOrDefault(proposal.getTopic(), 0L);
                    if (myTopicSeqNum < (proposal.getSeqNum() - 1)) {
                        // Am late; missing messages; must actively request them
                        logger.warn("BEHIND: HAVE " + myTopicSeqNum + " ; SHOULD HAVE " + (proposal.getSeqNum() - 1));
                        SortedSet<Proposal> proposals = pendingTopicsBuffer.getOrDefault(proposal.getTopic(), new TreeSet<>());
                        proposals.add(proposal);
                        pendingTopicsBuffer.putIfAbsent(proposal.getTopic(), proposals);

                        checkForLateTopics(myTopicSeqNum, proposals, membership);
                    } else {
                        myTopicSeqNums.put(proposal.getTopic(), proposal.getSeqNum());
                        // Writing to durable storage
                        String filename = myself.getPort() + "_" + proposal.getTopic();
                        Storage.writePublish(filename, proposal);
                        choosePublishMethod(proposal);
                    }
                    break;
                case ADD_REPLICA:
                    membership.add(proposal.getReplica());
                    logger.debug("PUB/SUB ADDING REPLICA; NEW MEMBERSHIP SIZE = " + membership.size());
                    addReplicaBehaviour(proposal.getReplica(), notification.getInstanceNumber());
                    break;
                case REMOVE_REPLICA:
                    membership.remove(proposal.getReplica());
                    logger.debug("PUB/SUB REMOVING REPLICA; NEW MEMBERSHIP SIZE = " + membership.size());
                    break;
                default:
                    throw new RuntimeException("OOPS; WRONG TYPE = " + proposal.getType());
            }
        }
    };
    private ProtocolNotificationHandler uponRouteDelivery = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification notification) {
            RouteDelivery delivery = (RouteDelivery) notification;
            Message message = delivery.getMessage();

            logger.debug("GOT ROUTE DELIVERY TYPE " + message.getType());

            if (message.getType() == Message.MessageType.GET_MEMBERSHIP) {
                logger.debug("GOT MEMBERSHIP REQUEST FROM " + message.getFrom());
                PaxosMembershipReply reply = new PaxosMembershipReply(delivery.getID(), membership);
                logger.debug("[pubsub protocol message] (network) " + myself.getPort());
                sendMessageSideChannel(reply, message.getFrom());
            }
        }
    };
    private ProtocolNotificationHandler uponBCastOneHopDeliverNotification = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification n) {
            BCastOneHopDeliver notification = (BCastOneHopDeliver) n;

            for (Map.Entry<String, Pair<Boolean, Integer>> entry : notification.getInfoMap().entrySet()) {
                String topic = entry.getKey();
                if (!myPopularTopics.containsKey(topic)) {
                    Pair<Boolean, Integer> newPair = entry.getValue();

                    Pair<Boolean, Integer> existingTopicPair = otherPopularTopics.get(topic);
                    if (existingTopicPair == null || existingTopicPair.second < newPair.second) {
                        otherPopularTopics.put(topic, newPair);
                    }
                }
            }

            logger.info("({}): received {} from ({})", myself, notification, notification.getEmitter());
        }
    };
    private ProtocolNotificationHandler uponPaxosMembershipChange = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification n) {
            PaxosLeaderChange change = (PaxosLeaderChange) n;
            paxosLeader = change.getNewLeader();
            logger.debug("PUBSUB WAS NOTIFIED OF CHANGES TO PAXOS LEADERSHIP; NEW LEADER = " + paxosLeader);
        }
    };
    private ProtocolTimerHandler uponPopularityTimerHandler = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            Map<String, Pair<Boolean, Integer>> combinedMap = new HashMap<>(myPopularTopics.size() + otherPopularTopics.size());
            combinedMap.putAll(myPopularTopics);
            combinedMap.putAll(otherPopularTopics);

            BCastOneHopRequest req = new BCastOneHopRequest(combinedMap);
            req.setDestination(BCast.PROTOCOL_ID);
            try {
                sendRequest(req);
            } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                destinationProtocolDoesNotExist.printStackTrace();
                System.exit(1);
            }

            int numPopTopics = 0;
            int numUnPopTopics = 0;
            for (Pair<Boolean, Integer> pair : combinedMap.values()) {
                if (pair.first) {
                    numPopTopics++;
                } else {
                    numUnPopTopics++;
                }
            }
        }
    };

    public PubSub(INetwork net) throws HandlerRegistrationException {
        super(net);

        // Requests
        registerRequestHandler(SubscribeRequest.REQUEST_ID, uponSubscribeRequest);
        registerRequestHandler(UnsubscribeRequest.REQUEST_ID, uponUnsubscribeRequest);
        registerRequestHandler(PublishRequest.REQUEST_ID, uponPublishRequest);

        // Declare Messages sent/received of the protocol.
        registerMessageHandler(PaxosMembershipReply.MSG_CODE, uponPaxosMembershipReply, PaxosMembershipReply.serializer);
        registerMessageHandler(PaxosMessageRangeReply.MSG_CODE, uponPaxosMessageRangeReply, PaxosMessageRangeReply.serializer);

        // Declare Timers of the Protocol.

        // Declare Notifications issued by the protocol.

        // Declare replies consumed by the protocol
        registerReplyHandler(MessageDelivery.REPLY_ID, uponMessageDelivery);

        // Timers
        registerTimerHandler(PopularityTimer.TimerCode, uponPopularityTimerHandler);
    }

    private void checkResponsibleTopicAndRequestMissing(String topic, String msg, long seqNum) {
        if (topics.contains(topic)) {
            PubSubDeliver deliver = new PubSubDeliver(topic, msg, seqNum);
            boolean canDeliver = true;

            if (!myPopularTopics.containsKey(topic)) {
                // This topic is not one I'm responsible for;

                long myTopicSeqNum = otherTopicSeqNums.getOrDefault(topic, 0L);

                if(myTopicSeqNum >= seqNum) {
                    logger.debug("GOT OUTDATED MESSAGE - IGNORING");
                    return;
                } else if (myTopicSeqNum < (seqNum - 1)) {
                    logger.info("I AM BEHIND");
                    Proposal proposal = new Proposal(Proposal.ProposalType.PUBLISH, topic, msg, seqNum);
                    BigInteger topicID = FingerTable.generate(topic);

                    SortedSet<Proposal> proposals = otherPendingProposals.getOrDefault(topic, new TreeSet<>());
                    proposals.add(proposal);
                    otherPendingProposals.putIfAbsent(topic, proposals);
                    topicIDtoTopicString.put(topicID, topic);
                    requestMembership(topic);
                    canDeliver = false;
                } else {
                    otherTopicSeqNums.put(topic, seqNum);
                }
            }

            if (canDeliver) {
                logger.info("({}): sending {}", myself, deliver);
                triggerNotification(deliver);
            }
        }
    }

    private void requestMembership(String topic) {
        logger.debug("SENDING MEMBERSHIP ROUTE REQUEST");
        BigInteger id = FingerTable.generate(topic);
        RouteRequest membershipRequest = new RouteRequest(id, new Message(Message.MessageType.GET_MEMBERSHIP, myself));
        membershipRequest.setDestination(Chord.PROTOCOL_ID);
        try {
            sendRequest(membershipRequest);
        } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
            destinationProtocolDoesNotExist.printStackTrace();
            System.exit(-1);
        }
    }

    private void choosePublishMethod(Proposal proposal) {
        ProtocolRequest req = null;
        if (isPopular(proposal.getTopic())) {
            req = new BCastRequest(proposal.getSeqNum(), proposal.getTopic(), proposal.getMessage());
            req.setDestination(BCast.PROTOCOL_ID);
        } else {
            req = new DisseminateRequest(proposal.getTopic(), proposal.getMessage(), proposal.getSeqNum());
            req.setDestination(Disseminate.PROTOCOL_ID);
        }

        try {
            sendRequest(req);
        } catch (DestinationProtocolDoesNotExist e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private boolean isPopular(String topic) {
        Pair<Boolean, Integer> toCheck = myPopularTopics.get(topic);
        if (toCheck != null && toCheck.first)
            return true;

        toCheck = otherPopularTopics.get(topic);
        if (toCheck != null && toCheck.first)
            return true;

        return false;
    }

    @Override
    public void init(Properties props) {
        this.topicPopularity = new HashMap<>(100);
        topics = new HashSet<>(1000);
        myPopularTopics = new HashMap<>(100);
        otherPopularTopics = new HashMap<>(100);

        POPULAR_THRESHOLD = Integer.parseInt(props.getProperty("PopularityThreshold"));
        UNPOPULAR_THRESHOLD = Integer.parseInt(props.getProperty("UnpopularityThreshold"));

        pendingPaxosPublishes = new HashMap<>();

        otherTopicSeqNums = new HashMap<>(100);
        otherPendingProposals = new HashMap<>(100);
        topicIDtoTopicString = new HashMap<>(100);

        pendingTopicsBuffer = new HashMap<>(50);

        Storage.clearStorage(String.valueOf(myself.getPort()));

        try {
            registerNotificationHandler(BCast.PROTOCOL_ID, BCastDeliver.NOTIFICATION_ID, uponBCastDeliverNotification);
            registerNotificationHandler(BCast.PROTOCOL_ID, BCastOneHopDeliver.NOTIFICATION_ID, uponBCastOneHopDeliverNotification);
            registerNotificationHandler(Disseminate.PROTOCOL_ID, RemovePopularity.NOTIFICATION_ID, uponRemovePopularity);
            registerNotificationHandler(Disseminate.PROTOCOL_ID, UpdatePopularity.NOTIFICATION_ID, uponUpdatePopularity);
            registerNotificationHandler(Chord.PROTOCOL_ID, RouteDelivery.NOTIFICATION_ID, uponRouteDelivery);

            registerNotificationHandler(Paxos.PROTOCOL_ID, DecideNotification.NOTIFICATION_ID, uponDecideNotification);
            registerNotificationHandler(Paxos.PROTOCOL_ID, PaxosLeaderChange.NOTIFICATION_ID, uponPaxosMembershipChange);

        } catch (HandlerRegistrationException | NotificationDoesNotExistException | ProtocolDoesNotExist e) {
            e.printStackTrace();
            System.exit(1);
        }

        //setup timers
        setupPeriodicTimer(
                new PopularityTimer(),
                Long.parseLong(props.getProperty("PopularityTimerPeriod")),
                Long.parseLong(props.getProperty("PopularityTimerPeriod"))
        );

        startPaxos(props);
    }
}
