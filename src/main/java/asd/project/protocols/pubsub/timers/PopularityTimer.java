package asd.project.protocols.pubsub.timers;

import babel.timer.ProtocolTimer;

public class PopularityTimer extends ProtocolTimer {
    public static final short TimerCode = 301;

    public PopularityTimer() {
        super(PopularityTimer.TimerCode);
    }

    @Override
    public Object clone() {
        return this;
    }
}