package asd.project.protocols.pubsub.notifications;

import babel.notification.ProtocolNotification;

public class RemovePopularity extends ProtocolNotification {

    public static final short NOTIFICATION_ID = 303;
    public static final String NOTIFICATION_NAME = "RemovePopularity";

    private final String topic;

    public RemovePopularity(String topic) {
        super(RemovePopularity.NOTIFICATION_ID, RemovePopularity.NOTIFICATION_NAME);
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    @Override
    public String toString() {
        return String.format("PubSubDeliver{topic='%s'}", topic);
    }
}
