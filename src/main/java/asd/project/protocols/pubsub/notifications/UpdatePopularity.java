package asd.project.protocols.pubsub.notifications;

import babel.notification.ProtocolNotification;

public class UpdatePopularity extends ProtocolNotification {

    public static final short NOTIFICATION_ID = 302;
    public static final String NOTIFICATION_NAME = "UpdatePopularity";

    private final String topic;
    private final int amount;

    public UpdatePopularity(String topic, int amount) {
        super(UpdatePopularity.NOTIFICATION_ID, UpdatePopularity.NOTIFICATION_NAME);
        this.topic = topic;
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public String getTopic() {
        return topic;
    }

    @Override
    public String toString() {
        return String.format("UpdatePopularity{topic='%s', amount='%s'}", topic, amount);
    }
}
