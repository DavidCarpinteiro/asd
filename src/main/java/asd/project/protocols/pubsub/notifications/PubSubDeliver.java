package asd.project.protocols.pubsub.notifications;

import babel.notification.ProtocolNotification;

public class PubSubDeliver extends ProtocolNotification {

    public static final short NOTIFICATION_ID = 301;
    public static final String NOTIFICATION_NAME = "PubSubDeliver";

    private final String topic;
    private final String message;
    private final long seqNum;

    public PubSubDeliver(String topic, String message, long seqNum) {
        super(PubSubDeliver.NOTIFICATION_ID, PubSubDeliver.NOTIFICATION_NAME);
        this.topic = topic;
        this.message = message;
        this.seqNum = seqNum;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    public long getSeqNum() {
        return seqNum;
    }

    @Override
    public String toString() {
        return String.format("PubSubDeliver{topic='%s', message='%s', seqNum='%s'}", topic, message, seqNum);
    }
}
