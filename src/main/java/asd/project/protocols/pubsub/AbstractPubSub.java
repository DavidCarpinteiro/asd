package asd.project.protocols.pubsub;

import asd.project.protocols.paxos.Paxos;
import asd.project.protocols.paxos.requests.ProposeRequest;
import asd.project.protocols.paxos.requests.StartRequest;
import asd.project.protocols.pubsub.messages.*;
import asd.project.protocols.pubsub.notifications.PubSubDeliver;
import asd.project.utils.Proposal;
import asd.project.utils.Storage;
import babel.exceptions.DestinationProtocolDoesNotExist;
import babel.exceptions.HandlerRegistrationException;
import babel.handlers.ProtocolMessageHandler;
import babel.protocol.GenericProtocol;
import babel.protocol.event.ProtocolMessage;
import network.Host;
import network.INetwork;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public abstract class AbstractPubSub extends GenericProtocol {
    // Numeric identifier and name of the protocol.

    public static final short PROTOCOL_ID = 300;
    public static final String PROTOCOL_NAME = "Publish/Subscribe";

    private static final String STORAGE = "storage";

    private static final Logger logger = LogManager.getLogger(AbstractPubSub.class);
    protected Set<Host> membership;
    protected Host paxosLeader;
    private final ProtocolMessageHandler uponAddReplicaRequest = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            AddReplicaRequest req = (AddReplicaRequest) msg;
            logger.info("({}) -> received AddReplicaRequest from ({})", myself, req.getReplica());

            if (paxosLeader.compareTo(myself) == 0) {
                logger.debug("GOT ADD REPLICA FROM " + msg.getFrom() + " ; AM LEADER");
                proposePaxos(new Proposal(Proposal.ProposalType.ADD_REPLICA, req.getReplica()));
            } else {
                logger.debug("GOT ADD REPLICA FROM " + msg.getFrom() + " ; AM NOT LEADER");
                logger.debug("[pubsub protocol message] (network) " + myself.getPort());
                sendMessage(msg, paxosLeader);
            }
        }
    };
    protected Map<String, Long> myTopicSeqNums;
    private final ProtocolMessageHandler uponPaxosPublish = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            logger.info("({}) -> received PaxosPublish from ({})", myself, msg.getFrom());

            logger.debug("GOT PAXOS PUBLISH FROM " + msg.getFrom() + " ; LEADER IS " + paxosLeader);

            PaxosPublish publish = (PaxosPublish) msg;
            String topic = publish.getTopic();
            String message = publish.getMessage();

            if (paxosLeader.compareTo(myself) == 0) {
                logger.debug("PUBLISHING");

                long seqNum = 1 + myTopicSeqNums.getOrDefault(topic, 0L);
                Proposal proposal = new Proposal(Proposal.ProposalType.PUBLISH, topic, message, seqNum);
                proposePaxos(proposal);

                logger.debug("DONE");
            } else {
                logger.debug("[pubsub protocol message] (network) " + myself.getPort());
                sendMessage(publish, paxosLeader);
            }
        }
    };
    private final ProtocolMessageHandler uponPaxosMessageRangeRequest = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            logger.info("({}) -> received PaxosMessageRangeRequest from ({})", myself, msg.getFrom());
            PaxosMessageRangeRequest req = (PaxosMessageRangeRequest) msg;
            logger.debug("GOT PAXOS RANGE REQUEST FROM " + msg.getFrom() + " RANGE IS [" + req.getStartSeqNum() + "-" + req.getEndSeqNum() + "]");

            if (myTopicSeqNums.get(req.getTopic()) >= req.getEndSeqNum()) {
                // Have all publishes in the topic
                String filename = myself.getPort() + "_" + req.getTopic();
                PaxosMessageRangeReply reply = new PaxosMessageRangeReply(Storage.readRange(filename, req.getTopic(), req.getStartSeqNum(), req.getEndSeqNum()));
                if (membership.contains(msg.getFrom())) {
                    logger.debug(String.format("SENDING RANGE REPLY %s-[%s %s] TO %s", req.getTopic(), req.getStartSeqNum(), req.getEndSeqNum(), msg.getFrom()));
                    logger.debug("[pubsub protocol message] (network) " + myself.getPort());
                    sendMessage(reply, msg.getFrom());
                } else {
                    logger.debug(String.format("SENDING RANGE REPLY %s-[%s %s] TO %s", req.getTopic(), req.getStartSeqNum(), req.getEndSeqNum(), msg.getFrom()));
                    logger.debug("[pubsub protocol message] (network) " + myself.getPort());
                    sendMessageSideChannel(reply, msg.getFrom());
                }
            }
        }
    };
    protected Map<String, SortedSet<Proposal>> pendingTopicsBuffer;
    private Set<String> inTransferTopics;
    private final ProtocolMessageHandler uponPaxosInfoReply = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            logger.info("({}) -> received PaxosInfoReply from ({})", myself, msg.getFrom());

            PaxosInfoReply reply = (PaxosInfoReply) msg;

            membership = reply.getMembership();
            membership.add(myself);
            paxosLeader = reply.getLeader();

            inTransferTopics = new HashSet<>(reply.getEntries().size());
            for (Entry<String, Long> entry : reply.getEntries()) {
                myTopicSeqNums.put(entry.getKey(), entry.getValue());
                inTransferTopics.add(entry.getKey());
            }

            if (inTransferTopics.isEmpty()) {
                StartRequest startRequest = new StartRequest(membership, paxosLeader, reply.getInstanceNumber());
                startRequest.setDestination(Paxos.PROTOCOL_ID);

                try {
                    sendRequest(startRequest);
                } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                    destinationProtocolDoesNotExist.printStackTrace();
                    System.exit(-1);
                }
            }

        }
    };
    private final ProtocolMessageHandler uponPaxosInfoFollowupReply = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            logger.info("({}) -> received PaxosInfoFollowupReply from ({})", myself, msg.getFrom());

            PaxosInfoFollowupReply reply = (PaxosInfoFollowupReply) msg;

            String topic = reply.getTopic();

            String filename = myself.getPort() + "_" + topic;
            Storage.writeFullFile(filename, reply.getContent());

            inTransferTopics.remove(topic);

            if (inTransferTopics.isEmpty()) {
                StartRequest startRequest = new StartRequest(membership, paxosLeader, reply.getInstanceNumber());
                startRequest.setDestination(Paxos.PROTOCOL_ID);

                try {
                    sendRequest(startRequest);
                } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                    destinationProtocolDoesNotExist.printStackTrace();
                    System.exit(-1);
                }
            }
        }
    };

    public AbstractPubSub(INetwork net) throws HandlerRegistrationException {
        super(PROTOCOL_NAME, PROTOCOL_ID, net);

        // Declare Messages sent/received of the protocol.
        registerMessageHandler(AddReplicaRequest.MSG_CODE, uponAddReplicaRequest, AddReplicaRequest.serializer);
        registerMessageHandler(PaxosInfoReply.MSG_CODE, uponPaxosInfoReply, PaxosInfoReply.serializer);
        registerMessageHandler(PaxosInfoFollowupReply.MSG_CODE, uponPaxosInfoFollowupReply, PaxosInfoFollowupReply.serializer);
        registerMessageHandler(PaxosPublish.MSG_CODE, uponPaxosPublish, PaxosPublish.serializer);
        registerMessageHandler(PaxosMessageRangeRequest.MSG_CODE, uponPaxosMessageRangeRequest, PaxosMessageRangeRequest.serializer);

        // Declare Timers of the Protocol.

        // Declare Notifications issued by the protocol.
        registerNotification(PubSubDeliver.NOTIFICATION_ID, PubSubDeliver.NOTIFICATION_NAME);

        // Declare requests exposed by the protocol.
    }

    protected void checkForLateTopics(Long myTopicSeqNum, SortedSet<Proposal> pendingTopicBuffer, Set<Host> membership) {
        long rangeStart = myTopicSeqNum + 1;
        long rangeEnd = pendingTopicBuffer.last().getSeqNum() - 1;

        if (rangeStart > rangeEnd) {
            throw new RuntimeException(" WRONG RANGE [\" + rangeStart + \" \" + rangeEnd + \"]");
        }

        int num = 0;
        final String topic = pendingTopicBuffer.first().getTopic();
        for (Host member : membership) {
            if (member.compareTo(myself) != 0 && num < quorumSize()) {
                logger.debug("SENDING RANGE REQUEST [" + rangeStart + " " + rangeEnd + "] TO " + member);
                PaxosMessageRangeRequest request = new PaxosMessageRangeRequest(topic, rangeStart, rangeEnd);
                logger.debug("[pubsub protocol message] (network) " + myself.getPort());
                sendMessage(request, member);
                num++;
            }
        }
    }

    protected List<Proposal> rangeReplyWriteProcess(Map<String, SortedSet<Proposal>> pendingTopicsBuffer, Map<String, Long> myTopicSeqNums, ProtocolMessage msg, boolean write) {
        PaxosMessageRangeReply req = (PaxosMessageRangeReply) msg;

        String currTopic = req.getMessages().get(0).getTopic();
        final long initialCurrTopicSeqNum = myTopicSeqNums.getOrDefault(currTopic, 0L);

        List<Proposal> props = req.getMessages()
                .stream()
                .filter(message -> message.getSeqNum() > initialCurrTopicSeqNum)
                .map(message -> new Proposal(Proposal.ProposalType.PUBLISH, message.getTopic(), message.getMessage(), message.getSeqNum()))
                .collect(Collectors.toList());

        logger.debug("SIZE == " + pendingTopicsBuffer.size());
        if (props.isEmpty()) {
            return props;
        }

        SortedSet<Proposal> topicBuffer = pendingTopicsBuffer.getOrDefault(currTopic, new TreeSet<>());
        topicBuffer.addAll(props);

        logger.debug("TOPIC BUFFER ADD ALL");
        topicBuffer.forEach(t -> logger.debug(t.getSeqNum() + " "));

        pendingTopicsBuffer.put(currTopic, topicBuffer);

        List<Proposal> written = new LinkedList<>();
        Proposal proposal = topicBuffer.first();

        long currTopicSeqNum = initialCurrTopicSeqNum;
        logger.debug("CURR TOPIC SEQ NUM = " + currTopicSeqNum);
        while (proposal != null && proposal.getSeqNum() == (currTopicSeqNum + 1)) {
            topicBuffer.remove(proposal);

            myTopicSeqNums.put(currTopic, proposal.getSeqNum());
            currTopicSeqNum = proposal.getSeqNum();

            if (write) {
                Storage.writePublish(myself.getPort() + "_" + proposal.getTopic(), proposal);
            }
            written.add(proposal);
            logger.debug(String.format("WRITING %s-[%s] + %s", proposal.getTopic(), proposal.getSeqNum(), write));

            if (topicBuffer.isEmpty()) {
                break;
            }

            proposal = topicBuffer.first();
        }
        logger.warn("FINISHED WRITING; THERE ARE STILL " + topicBuffer.size() + " ELEMENTS IN BUFFER");

        return written;
    }

    protected void proposePaxos(Proposal proposal) {
        ProposeRequest proposeRequest = new ProposeRequest(proposal);
        proposeRequest.setDestination(Paxos.PROTOCOL_ID);
        try {
            sendRequest(proposeRequest);
        } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
            destinationProtocolDoesNotExist.printStackTrace();
            System.exit(-1);
        }
    }

    protected void startPaxos(Properties props) {

        myTopicSeqNums = new HashMap<>(100);

        if (!props.containsKey("PaxosContact")) {
            // No existing Contact; Am Paxos Leader

            membership = new HashSet<>();
            membership.add(myself);
            paxosLeader = myself;

            StartRequest startRequest = new StartRequest(membership, paxosLeader, 1);
            startRequest.setDestination(Paxos.PROTOCOL_ID);
            try {
                sendRequest(startRequest);
            } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
                destinationProtocolDoesNotExist.printStackTrace();
                System.exit(-1);
            }
        } else {
            String[] hostElems = props.getProperty("PaxosContact").split(":");
            Host paxosContact = null;
            try {
                paxosContact = new Host(InetAddress.getByName(hostElems[0]), Short.parseShort(hostElems[1]));
            } catch (UnknownHostException e) {
                e.printStackTrace();
                System.exit(-1);
            }
            logger.debug("SENDING ADD REPLICA REQUEST");
            AddReplicaRequest req = new AddReplicaRequest(myself);
            logger.debug("[pubsub protocol message] (network) " + myself.getPort());
            sendMessageSideChannel(req, paxosContact);
        }
    }

    protected long quorumSize() {
        return membership.size() / 2 + 1;
    }

    protected void addReplicaBehaviour(Host destination, long instanceNumber) {
        logger.debug("SENDING PAXOS INFO REPLY");
        Set<Entry<String, Long>> currTopics = myTopicSeqNums.entrySet();
        PaxosInfoReply reply = new PaxosInfoReply(membership, paxosLeader, currTopics, instanceNumber);
        logger.debug("[pubsub protocol message] (network) " + myself.getPort());
        sendMessageSideChannel(reply, destination);

        for (Entry<String, Long> en : currTopics) {
            logger.debug("SENDING FOLLOW UP REPLY");
            String filename = myself.getPort() + "_" + en.getKey();
            PaxosInfoFollowupReply followupReply = new PaxosInfoFollowupReply(en.getKey(), Storage.readFile(filename), instanceNumber);
            logger.debug("[pubsub protocol message] (network) " + myself.getPort());
            sendMessageSideChannel(followupReply, destination);
        }
    }
}
