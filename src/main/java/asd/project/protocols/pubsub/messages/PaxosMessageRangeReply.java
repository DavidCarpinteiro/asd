package asd.project.protocols.pubsub.messages;

import asd.project.utils.Message;
import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

public class PaxosMessageRangeReply extends ProtocolMessage {

    public final static short MSG_CODE = 305;
    public static final ISerializer<PaxosMessageRangeReply> serializer = new ISerializer<>() {
        @Override
        public void serialize(PaxosMessageRangeReply message, ByteBuf out) {
            out.writeInt(message.messages.size());
            for (Message m : message.messages)
                m.serialize(out);
        }

        @Override
        public PaxosMessageRangeReply deserialize(ByteBuf in) throws UnknownHostException {
            List<Message> deserialized = new LinkedList<>();

            int listLen = in.readInt();
            for (int i = 0; i < listLen; i++)
                deserialized.add(Message.deserialize(in));

            return new PaxosMessageRangeReply(deserialized);
        }

        @Override
        public int serializedSize(PaxosMessageRangeReply message) {
            int size = 0;
            size += Integer.BYTES;
            for (Message m : message.messages)
                size += m.serializedSize();

            return size;
        }
    };

    private final List<Message> messages;

    public PaxosMessageRangeReply(List<Message> messages) {
        super(MSG_CODE);
        this.messages = messages;
    }

    public List<Message> getMessages() {
        return messages;
    }

    @Override
    public String toString() {
        return String.format("PaxosMessageRangeReply{messages=%s}", messages.size());
    }
}
