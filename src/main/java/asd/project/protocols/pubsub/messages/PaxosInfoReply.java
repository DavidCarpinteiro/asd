package asd.project.protocols.pubsub.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

public class PaxosInfoReply extends ProtocolMessage {

    public final static short MSG_CODE = 302;
    public static final ISerializer<PaxosInfoReply> serializer = new ISerializer<>() {
        @Override
        public void serialize(PaxosInfoReply message, ByteBuf out) {
            int membershipSize = message.getMembership().size();
            out.writeInt(membershipSize);
            for (Host host : message.getMembership())
                host.serialize(out);

            message.leader.serialize(out);

            int topicsSize = message.entries.size();
            out.writeInt(topicsSize);

            for (Entry<String, Long> entry : message.entries) {
                byte[] topicBytes = entry.getKey().getBytes();
                out.writeInt(topicBytes.length);
                out.writeBytes(topicBytes);
                out.writeLong(entry.getValue());
            }

            out.writeLong(message.instanceNumber);
        }

        @Override
        public PaxosInfoReply deserialize(ByteBuf in) throws UnknownHostException {
            int membershipSize = in.readInt();
            Set<Host> membership = new HashSet<>(membershipSize);
            for (int i = 0; i < membershipSize; i++)
                membership.add(Host.deserialize(in));

            Host leader = Host.deserialize(in);


            int entrySetSize = in.readInt();
            Set<Entry<String, Long>> topics = new HashSet<>(entrySetSize);
            for (int i = 0; i < entrySetSize; i++) {
                int topicLen = in.readInt();
                byte[] topicBytes = new byte[topicLen];
                in.readBytes(topicBytes);
                long seqNum = in.readLong();
                topics.add(new AbstractMap.SimpleEntry<>(new String(topicBytes), seqNum));
            }

            long instanceNumber = in.readLong();

            return new PaxosInfoReply(membership, leader, topics, instanceNumber);
        }

        @Override
        public int serializedSize(PaxosInfoReply message) {
            int size = 0;
            size += Integer.BYTES;
            for (Host h : message.getMembership())
                size += h.serializedSize();

            size += message.getLeader().serializedSize();

            size += Integer.BYTES;
            for (Entry<String, Long> entry : message.entries) {
                size += Integer.BYTES;
                size += entry.getKey().getBytes().length;
                size += Long.BYTES;
            }

            size += Long.BYTES;

            return size;
        }
    };

    private final Set<Host> membership;
    private final Host leader;
    private Set<Entry<String, Long>> entries;
    private long instanceNumber;

    public PaxosInfoReply(Set<Host> membership, Host leader, Set<Entry<String, Long>> entries, long instanceNumber) {
        super(MSG_CODE);
        this.membership = membership;
        this.leader = leader;
        this.entries = entries;
        this.instanceNumber = instanceNumber;
    }

    public long getInstanceNumber() {
        return instanceNumber;
    }

    public Set<Host> getMembership() {
        return membership;
    }

    public Host getLeader() {
        return leader;
    }

    public Set<Entry<String, Long>> getEntries() {
        return entries;
    }

    @Override
    public String toString() {
        return String.format("PaxosInfoReply{membership=%s, leader=%s, topics=%s, instanceNumber=%s}", membership.size(), leader, entries.size(), instanceNumber);
    }
}
