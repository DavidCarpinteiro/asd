package asd.project.protocols.pubsub.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;

public class PaxosMessageRangeRequest extends ProtocolMessage {

    public final static short MSG_CODE = 306;
    public static final ISerializer<PaxosMessageRangeRequest> serializer = new ISerializer<>() {
        @Override
        public void serialize(PaxosMessageRangeRequest message, ByteBuf out) {
            byte[] topicBytes = message.topic.getBytes();
            out.writeInt(topicBytes.length);
            out.writeBytes(topicBytes);

            out.writeLong(message.startSeqNum);
            out.writeLong(message.endSeqNum);
        }

        @Override
        public PaxosMessageRangeRequest deserialize(ByteBuf in) throws UnknownHostException {
            int len = in.readInt();
            byte[] topicBytes = new byte[len];
            in.readBytes(topicBytes);

            return new PaxosMessageRangeRequest(new String(topicBytes), in.readLong(), in.readLong());
        }

        @Override
        public int serializedSize(PaxosMessageRangeRequest message) {
            return Long.BYTES * 2 + Integer.BYTES + message.topic.getBytes().length;
        }
    };

    private final String topic;
    private final long startSeqNum;
    private final long endSeqNum;

    public PaxosMessageRangeRequest(String topic, long startSeqNum, long endSeqNum) {
        super(MSG_CODE);
        this.topic = topic;
        this.startSeqNum = startSeqNum;
        this.endSeqNum = endSeqNum;
    }

    public String getTopic() {
        return topic;
    }

    public long getStartSeqNum() {
        return startSeqNum;
    }

    public long getEndSeqNum() {
        return endSeqNum;
    }

    @Override
    public String toString() {
        return String.format("PaxosMessageRangeRequest{topic=%s, start=%s, end=%s}", topic, startSeqNum, endSeqNum);
    }
}
