package asd.project.protocols.pubsub.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.math.BigInteger;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class PaxosMembershipReply extends ProtocolMessage {

    public final static short MSG_CODE = 303;
    public static final ISerializer<PaxosMembershipReply> serializer = new ISerializer<>() {
        @Override
        public void serialize(PaxosMembershipReply message, ByteBuf out) {
            byte[] idBytes = message.ID.toByteArray();
            out.writeInt(idBytes.length);
            out.writeBytes(idBytes);

            int membershipSize = message.getMembership().size();
            out.writeInt(membershipSize);
            for (Host host : message.getMembership())
                host.serialize(out);
        }

        @Override
        public PaxosMembershipReply deserialize(ByteBuf in) throws UnknownHostException {
            int idSize = in.readInt();
            byte[] idBytes = new byte[idSize];
            in.readBytes(idBytes);

            int membershipSize = in.readInt();
            Set<Host> membership = new HashSet<>(membershipSize);
            for (int i = 0; i < membershipSize; i++)
                membership.add(Host.deserialize(in));

            return new PaxosMembershipReply(new BigInteger(idBytes), membership);
        }

        @Override
        public int serializedSize(PaxosMembershipReply message) {
            int size = 0;
            size += Integer.BYTES;
            for (Host h : message.getMembership())
                size += h.serializedSize();

            size += Integer.BYTES;
            size += message.ID.toByteArray().length;
            return size;
        }
    };

    private final Set<Host> membership;
    private final BigInteger ID;

    public PaxosMembershipReply(BigInteger ID, Set<Host> membership) {
        super(MSG_CODE);
        this.membership = membership == null ? Collections.emptySet() : membership;
        this.ID = ID;
    }

    public BigInteger getID() {
        return ID;
    }

    public Set<Host> getMembership() {
        return membership;
    }

    @Override
    public String toString() {
        return String.format("PaxosMembershipReply{membership=%s, id=%s}", membership.size(), ID);
    }
}
