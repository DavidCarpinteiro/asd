package asd.project.protocols.pubsub.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;

public class PaxosPublish extends ProtocolMessage {

    public final static short MSG_CODE = 304;
    public static final ISerializer<PaxosPublish> serializer = new ISerializer<>() {
        @Override
        public void serialize(PaxosPublish message, ByteBuf out) {
            byte[] topicBytes = message.topic.getBytes();
            out.writeInt(topicBytes.length);
            out.writeBytes(topicBytes);

            byte[] messageBytes = message.message.getBytes();
            out.writeInt(messageBytes.length);
            out.writeBytes(messageBytes);
        }

        @Override
        public PaxosPublish deserialize(ByteBuf in) throws UnknownHostException {
            int topicSize = in.readInt();
            byte[] topicBytes = new byte[topicSize];
            in.readBytes(topicBytes);

            int messageSize = in.readInt();
            byte[] messageBytes = new byte[messageSize];
            in.readBytes(messageBytes);

            return new PaxosPublish(new String(topicBytes), new String(messageBytes));
        }

        @Override
        public int serializedSize(PaxosPublish message) {
            return Integer.BYTES + message.topic.getBytes().length + Integer.BYTES + message.message.getBytes().length;
        }
    };

    private final String topic;
    private final String message;

    public PaxosPublish(String topic, String message) {
        super(MSG_CODE);
        this.topic = topic;
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return String.format("PaxosPublish{topic=%s, message=%s}", topic, message);
    }
}
