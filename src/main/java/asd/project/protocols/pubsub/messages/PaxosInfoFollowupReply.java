package asd.project.protocols.pubsub.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;

public class PaxosInfoFollowupReply extends ProtocolMessage {

    public final static short MSG_CODE = 307;
    public static final ISerializer<PaxosInfoFollowupReply> serializer = new ISerializer<>() {
        @Override
        public void serialize(PaxosInfoFollowupReply message, ByteBuf out) {
            byte[] topicBytes = message.topic.getBytes();
            out.writeInt(topicBytes.length);
            out.writeBytes(topicBytes);

            out.writeInt(message.content.length);
            out.writeBytes(message.content);

            out.writeLong(message.instanceNumber);
        }

        @Override
        public PaxosInfoFollowupReply deserialize(ByteBuf in) throws UnknownHostException {
            int topicLen = in.readInt();
            byte[] topicBytes = new byte[topicLen];
            in.readBytes(topicBytes);

            int contentLen = in.readInt();
            byte[] content = new byte[contentLen];
            in.readBytes(content);

            long instanceNumber = in.readLong();

            return new PaxosInfoFollowupReply(new String(topicBytes), content, instanceNumber);
        }

        @Override
        public int serializedSize(PaxosInfoFollowupReply message) {
            int size = 0;
            size += Integer.BYTES;
            size += message.topic.getBytes().length;

            size += Integer.BYTES;
            size += message.content.length;

            size += Long.BYTES;

            return size;
        }
    };

    private final String topic;
    private final byte[] content;
    private long instanceNumber;

    public PaxosInfoFollowupReply(String topic, byte[] content, long instanceNumber) {
        super(MSG_CODE);
        this.topic = topic;
        this.content = content;
        this.instanceNumber = instanceNumber;
    }

    public long getInstanceNumber() {
        return instanceNumber;
    }

    public String getTopic() {
        return topic;
    }

    public byte[] getContent() {
        return content;
    }

    @Override
    public String toString() {
        return String.format("PaxosFollowupInfoReply{topic=%s, content=%s, instanceNumber=%s}", topic, content.length, instanceNumber);
    }
}
