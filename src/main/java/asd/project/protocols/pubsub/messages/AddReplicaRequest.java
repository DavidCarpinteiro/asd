package asd.project.protocols.pubsub.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;

public class AddReplicaRequest extends ProtocolMessage {

    public final static short MSG_CODE = 301;
    public static final ISerializer<AddReplicaRequest> serializer = new ISerializer<>() {
        @Override
        public void serialize(AddReplicaRequest message, ByteBuf out) {
            message.replica.serialize(out);
        }

        @Override
        public AddReplicaRequest deserialize(ByteBuf in) throws UnknownHostException {
            return new AddReplicaRequest(Host.deserialize(in));
        }

        @Override
        public int serializedSize(AddReplicaRequest message) {
            return message.replica.serializedSize();
        }
    };

    private final Host replica;

    public AddReplicaRequest(Host replica) {
        super(MSG_CODE);
        this.replica = replica;
    }

    public Host getReplica() {
        return replica;
    }

    @Override
    public String toString() {
        return String.format("AddReplicaRequest{replica=%s}", replica);
    }
}
