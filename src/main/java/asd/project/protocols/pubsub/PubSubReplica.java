package asd.project.protocols.pubsub;

import asd.project.protocols.paxos.Paxos;
import asd.project.protocols.paxos.notifications.DecideNotification;
import asd.project.protocols.paxos.notifications.PaxosLeaderChange;
import asd.project.protocols.pubsub.messages.PaxosMessageRangeReply;
import asd.project.utils.Proposal;
import asd.project.utils.Storage;
import babel.exceptions.HandlerRegistrationException;
import babel.exceptions.NotificationDoesNotExistException;
import babel.exceptions.ProtocolDoesNotExist;
import babel.handlers.ProtocolMessageHandler;
import babel.handlers.ProtocolNotificationHandler;
import babel.notification.ProtocolNotification;
import babel.protocol.event.ProtocolMessage;
import network.INetwork;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;

public class PubSubReplica extends AbstractPubSub {
    private static final Logger logger = LogManager.getLogger(PubSubReplica.class);
    private final ProtocolMessageHandler uponPaxosMessageRangeReply = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            rangeReplyWriteProcess(pendingTopicsBuffer, myTopicSeqNums, msg, true);
        }
    };
    private ProtocolNotificationHandler uponDecideNotification = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification n) {
            DecideNotification notification = (DecideNotification) n;
            Proposal proposal = notification.getProposal();

            logger.info("({}): received {} from ({})", myself, notification, notification.getEmitter());

            switch (proposal.getType()) {
                case PUBLISH:
                    Long myTopicSeqNum = myTopicSeqNums.getOrDefault(proposal.getTopic(), 0L);
                    if (myTopicSeqNum < (proposal.getSeqNum() - 1)) {
                        // Am late; missing messages; must actively request them
                        logger.warn("BEHIND: HAVE " + myTopicSeqNum + " ; SHOULD HAVE " + (proposal.getSeqNum() - 1));
                        SortedSet<Proposal> proposals = pendingTopicsBuffer.getOrDefault(proposal.getTopic(), new TreeSet<>());
                        proposals.add(proposal);
                        pendingTopicsBuffer.putIfAbsent(proposal.getTopic(), proposals);

                        checkForLateTopics(myTopicSeqNum, proposals, membership);
                    } else {
                        myTopicSeqNums.put(proposal.getTopic(), proposal.getSeqNum());
                        String filename = myself.getPort() + "_" + proposal.getTopic();
                        Storage.writePublish(filename, proposal);
                    }
                    break;
                case ADD_REPLICA:
                    membership.add(proposal.getReplica());
                    logger.debug("PUB/SUB ADDING REPLICA; NEW MEMBERSHIP SIZE = " + membership.size());
                    addReplicaBehaviour(proposal.getReplica(), notification.getInstanceNumber());
                    break;
                case REMOVE_REPLICA:
                    membership.remove(proposal.getReplica());
                    logger.debug("PUB/SUB REMOVING REPLICA; NEW MEMBERSHIP SIZE = " + membership.size());
                    break;
                default:
                    throw new RuntimeException("OOPS; WRONG TYPE = " + proposal.getType());
            }
        }
    };
    private ProtocolNotificationHandler uponPaxosMembershipChange = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification n) {
            PaxosLeaderChange change = (PaxosLeaderChange) n;
            paxosLeader = change.getNewLeader();
            logger.debug("PUBSUB WAS NOTIFIED OF CHANGES TO PAXOS LEADERSHIP; NEW LEADER = " + paxosLeader);
        }
    };

    public PubSubReplica(INetwork net) throws HandlerRegistrationException {
        super(net);
    }

    @Override
    public void init(Properties props) {
        pendingTopicsBuffer = new HashMap<>(50);
        Storage.clearStorage(String.valueOf(myself.getPort()));

        try {
            registerNotificationHandler(Paxos.PROTOCOL_ID, DecideNotification.NOTIFICATION_ID, uponDecideNotification);
            registerNotificationHandler(Paxos.PROTOCOL_ID, PaxosLeaderChange.NOTIFICATION_ID, uponPaxosMembershipChange);
            registerMessageHandler(PaxosMessageRangeReply.MSG_CODE, uponPaxosMessageRangeReply, PaxosMessageRangeReply.serializer);

        } catch (HandlerRegistrationException | NotificationDoesNotExistException | ProtocolDoesNotExist e) {
            e.printStackTrace();
            System.exit(1);
        }

        startPaxos(props);
    }
}
