package asd.project.protocols.pubsub.requests;

import babel.requestreply.ProtocolRequest;

public class PublishRequest extends ProtocolRequest {

    public static final short REQUEST_ID = 303;

    private final String topic;
    private final String message;

    public PublishRequest(String topic, String message) {
        super(PublishRequest.REQUEST_ID);
        this.topic = topic;
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return String.format("PublishRequest{topic='%s', message='%s'}", topic, message);
    }
}
