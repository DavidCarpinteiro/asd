package asd.project.protocols.pubsub.requests;

import babel.requestreply.ProtocolRequest;

public class SubscribeRequest extends ProtocolRequest {

    public static final short REQUEST_ID = 301;

    private final String topic;

    public SubscribeRequest(String topic) {
        super(SubscribeRequest.REQUEST_ID);
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    @Override
    public String toString() {
        return String.format("SubscribeRequest{topic='%s'}", topic);
    }
}
