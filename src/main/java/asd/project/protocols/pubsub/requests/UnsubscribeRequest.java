package asd.project.protocols.pubsub.requests;

import babel.requestreply.ProtocolRequest;

public class UnsubscribeRequest extends ProtocolRequest {

    public static final short REQUEST_ID = 302;

    private final String topic;

    public UnsubscribeRequest(String topic) {
        super(UnsubscribeRequest.REQUEST_ID);
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    @Override
    public String toString() {
        return String.format("UnsubscribeRequest{topic='%s'}", topic);
    }
}
