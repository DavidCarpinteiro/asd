package asd.project.protocols.paxos.notifications;

import asd.project.utils.Proposal;
import babel.notification.ProtocolNotification;

public class DecideNotification extends ProtocolNotification {

    public static final short NOTIFICATION_ID = 601;
    public static final String NOTIFICATION_NAME = "Decide";

    private final Proposal proposal;
    private final long instanceNumber;

    public DecideNotification(Proposal proposal, long instanceNumber) {
        super(DecideNotification.NOTIFICATION_ID, DecideNotification.NOTIFICATION_NAME);
        this.proposal = proposal;
        this.instanceNumber = instanceNumber;

    }

    public long getInstanceNumber() {
        return instanceNumber;
    }

    public Proposal getProposal() {
        return proposal;
    }

    @Override
    public String toString() {
        return String.format("DecideNotification{proposal=%s, instanceNumber=%s}", proposal.toString(), instanceNumber);
    }
}
