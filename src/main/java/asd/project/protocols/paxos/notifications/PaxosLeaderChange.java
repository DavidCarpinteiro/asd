package asd.project.protocols.paxos.notifications;

import babel.notification.ProtocolNotification;
import network.Host;

public class PaxosLeaderChange extends ProtocolNotification {

    public static final short NOTIFICATION_ID = 602;
    public static final String NOTIFICATION_NAME = "PaxosLeaderChange";

    private final Host newLeader;

    public PaxosLeaderChange(Host newLeader) {
        super(PaxosLeaderChange.NOTIFICATION_ID, PaxosLeaderChange.NOTIFICATION_NAME);
        this.newLeader = newLeader;
    }

    public Host getNewLeader() {
        return newLeader;
    }

    @Override
    public String toString() {
        return String.format("PaxosMembershipChange{newLeader=%s}", newLeader);
    }
}
