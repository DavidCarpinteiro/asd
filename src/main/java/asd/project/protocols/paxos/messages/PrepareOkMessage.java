package asd.project.protocols.paxos.messages;

import asd.project.utils.Proposal;
import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

public class PrepareOkMessage extends ProtocolMessage {

    public final static short MSG_CODE = 605;
    public static final ISerializer<PrepareOkMessage> serializer = new ISerializer<>() {
        @Override
        public void serialize(PrepareOkMessage message, ByteBuf out) {
            out.writeLong(message.paxosInstance);
            out.writeLong(message.seqNum);

            out.writeInt(message.proposalList.size());
            for (Proposal proposal : message.proposalList) {
                proposal.serialize(out);
            }
        }

        @Override
        public PrepareOkMessage deserialize(ByteBuf in) throws UnknownHostException {
            long paxosInstance = in.readLong();
            long seqNum = in.readLong();

            int proposalListSize = in.readInt();
            List<Proposal> proposalList = new LinkedList<>();
            for (int i = 0; i < proposalListSize; i++) {
                proposalList.add(Proposal.deserialize(in));
            }

            return new PrepareOkMessage(paxosInstance, seqNum, proposalList);
        }

        @Override
        public int serializedSize(PrepareOkMessage message) {
            int size = Long.BYTES * 2 + Integer.BYTES;
            for (Proposal proposal : message.proposalList) {
                size += proposal.serializedSize();
            }
            return size;
        }
    };

    private final long paxosInstance;
    private final long seqNum;
    private List<Proposal> proposalList;

    public PrepareOkMessage(PrepareMessage prepareMessage) {
        super(MSG_CODE);
        this.paxosInstance = prepareMessage.getPaxosInstance();
        this.seqNum = prepareMessage.getSeqNum();
        this.proposalList = new LinkedList<>();
    }

    public PrepareOkMessage(PrepareMessage prepareMessage, List<Proposal> proposalList) {
        super(MSG_CODE);
        this.paxosInstance = prepareMessage.getPaxosInstance();
        this.seqNum = prepareMessage.getSeqNum();
        this.proposalList = proposalList;
    }

    private PrepareOkMessage(long paxosInstance, long seqNum, List<Proposal> proposalList) {
        super(MSG_CODE);
        this.paxosInstance = paxosInstance;
        this.seqNum = seqNum;
        this.proposalList = proposalList;
    }

    public long getPaxosInstance() {
        return paxosInstance;
    }

    public long getSeqNum() {
        return seqNum;
    }

    public List<Proposal> getProposalList() {
        return proposalList;
    }

    @Override
    public String toString() {
        return "PrepareOkMessage{" +
                "payload=" + " " + paxosInstance + " " + seqNum +
                '}';
    }
}
