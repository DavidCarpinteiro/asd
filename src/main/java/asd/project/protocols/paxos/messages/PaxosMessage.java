package asd.project.protocols.paxos.messages;

import asd.project.utils.Proposal;
import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;

public class PaxosMessage extends ProtocolMessage {

    public final static short MSG_CODE = 601;
    public static final ISerializer<PaxosMessage> serializer = new ISerializer<>() {
        @Override
        public void serialize(PaxosMessage message, ByteBuf out) {
            message.proposal.serialize(out);
        }

        @Override
        public PaxosMessage deserialize(ByteBuf in) throws UnknownHostException {
            return new PaxosMessage(Proposal.deserialize(in));
        }

        @Override
        public int serializedSize(PaxosMessage message) {
            return message.proposal.serializedSize();
        }
    };
    private final Proposal proposal;

    public PaxosMessage(Proposal proposal) {
        super(MSG_CODE);
        this.proposal = proposal;
    }

    public Proposal getProposal() {
        return proposal;
    }

    @Override
    public String toString() {
        return "PaxosReplicaMessage{" +
                "payload=" + proposal.toString() +
                '}';
    }
}
