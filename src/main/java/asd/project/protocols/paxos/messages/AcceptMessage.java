package asd.project.protocols.paxos.messages;

import asd.project.utils.Proposal;
import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;

public class AcceptMessage extends ProtocolMessage {

    public final static short MSG_CODE = 602;
    public static final ISerializer<AcceptMessage> serializer = new ISerializer<>() {
        @Override
        public void serialize(AcceptMessage message, ByteBuf out) {
            out.writeLong(message.paxosInstance);
            out.writeLong(message.seqNum);
            message.proposal.serialize(out);
        }

        @Override
        public AcceptMessage deserialize(ByteBuf in) throws UnknownHostException {
            return new AcceptMessage(in.readLong(), in.readLong(), Proposal.deserialize(in));
        }

        @Override
        public int serializedSize(AcceptMessage message) {
            return Long.BYTES * 2 + message.proposal.serializedSize();
        }
    };
    private final long paxosInstance;
    private final long seqNum;
    private final Proposal proposal;

    public AcceptMessage(long paxosInstance, long seqNum, Proposal proposal) {
        super(MSG_CODE);
        this.paxosInstance = paxosInstance;
        this.seqNum = seqNum;
        this.proposal = proposal;
    }

    public Proposal getProposal() {
        return proposal;
    }

    public long getPaxosInstance() {
        return paxosInstance;
    }

    public long getSeqNum() {
        return seqNum;
    }

    @Override
    public String toString() {
        return "AcceptMessage{" +
                "payload=" + proposal.toString() + " " + paxosInstance + " " + seqNum +
                '}';
    }
}
