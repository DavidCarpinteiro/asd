package asd.project.protocols.paxos.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

import java.net.UnknownHostException;

public class PrepareMessage extends ProtocolMessage {

    public final static short MSG_CODE = 604;
    public static final ISerializer<PrepareMessage> serializer = new ISerializer<>() {
        @Override
        public void serialize(PrepareMessage message, ByteBuf out) {
            out.writeLong(message.paxosInstance);
            out.writeLong(message.seqNum);
        }

        @Override
        public PrepareMessage deserialize(ByteBuf in) throws UnknownHostException {
            return new PrepareMessage(in.readLong(), in.readLong());
        }

        @Override
        public int serializedSize(PrepareMessage message) {
            return Long.BYTES * 2;
        }
    };
    private final long paxosInstance;
    private final long seqNum;

    public PrepareMessage(long paxosInstance, long seqNum) {
        super(MSG_CODE);
        this.paxosInstance = paxosInstance;
        this.seqNum = seqNum;
    }

    public long getPaxosInstance() {
        return paxosInstance;
    }

    public long getSeqNum() {
        return seqNum;
    }

    @Override
    public String toString() {
        return "PrepareMessage{" +
                "payload=" + " " + paxosInstance + " " + seqNum +
                '}';
    }
}
