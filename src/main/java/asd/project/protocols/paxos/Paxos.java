package asd.project.protocols.paxos;

import asd.project.protocols.paxos.messages.*;
import asd.project.protocols.paxos.notifications.DecideNotification;
import asd.project.protocols.paxos.notifications.PaxosLeaderChange;
import asd.project.protocols.paxos.requests.ProposeRequest;
import asd.project.protocols.paxos.requests.StartRequest;
import asd.project.protocols.paxos.timers.LeaderCheckTimer;
import asd.project.protocols.paxos.timers.NoOpTimer;
import asd.project.utils.Proposal;
import asd.project.utils.Proposal.ProposalType;
import asd.project.utils.Storage;
import babel.exceptions.HandlerRegistrationException;
import babel.handlers.ProtocolMessageHandler;
import babel.handlers.ProtocolRequestHandler;
import babel.handlers.ProtocolTimerHandler;
import babel.protocol.GenericProtocol;
import babel.protocol.event.ProtocolMessage;
import babel.requestreply.ProtocolRequest;
import babel.timer.ProtocolTimer;
import network.Host;
import network.INetwork;
import network.INodeListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Paxos extends GenericProtocol implements INodeListener {

    //Numeric identifier of the protocol
    public final static short PROTOCOL_ID = 600;
    public final static String PROTOCOL_NAME = "Paxos";
    private static final Logger logger = LogManager.getLogger(Paxos.class);
    private int maxMembershipSize;
    private long seqNum;
    private long paxosInstance;
    private Set<Host> membership;
    private Host paxosLeader;
    private Queue<Proposal> pendingProposals;
    private long highestSeqNum;
    private Set<AcceptOkMessage> acceptOks;
    private Set<PrepareOkMessage> prepareOks;
    private long leaderCheckTimeout;
    private UUID leaderCheckTimer;
    private String storageFilename;
    private boolean paxosStarted;
    private long initialInstance;
    private final ProtocolMessageHandler uponPrepareMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            PrepareMessage prepareMessage = (PrepareMessage) msg;
            logger.debug("GOT PREPARE " + prepareMessage.toString() + " FROM " + prepareMessage.getFrom().getPort());
            logger.debug("MY PAXOS INSTANCE = " + paxosInstance + " ; My highestSeqNum = " + highestSeqNum);

            if (prepareMessage.getSeqNum() > highestSeqNum) {
                highestSeqNum = prepareMessage.getSeqNum();

                prepareOks.removeIf(messageToRemove -> messageToRemove.getSeqNum() <= highestSeqNum);

                if (prepareMessage.getPaxosInstance() < paxosInstance) {
                    long start = prepareMessage.getPaxosInstance() - (initialInstance - 1);
                    long end = (paxosInstance - 1) - (initialInstance - 1);

                    logger.debug(String.format("READING PROPOSALS start=%d, end=%d\n", start, end));

                    List<Proposal> proposals = Storage.readPaxosProposalRange(storageFilename, start, end);

                    PrepareOkMessage prepareOkMessage = new PrepareOkMessage(prepareMessage, proposals);
                    logger.debug(String.format("SENDING PREPARE_OK (%s) WITH PROPOSALS (%d) TO %s\n", prepareOkMessage, proposals.size(), prepareMessage.getFrom()));
                    logger.debug("[paxos protocol message] (network) " + myself.getPort());
                    sendMessage(prepareOkMessage, prepareMessage.getFrom());
                } else {
                    PrepareOkMessage prepareOkMessage = new PrepareOkMessage(prepareMessage);
                    logger.debug("SENDING PREPOK " + prepareOkMessage.toString() + " TO " + prepareMessage.getFrom());
                    logger.debug("[paxos protocol message] (network) " + myself.getPort());
                    sendMessage(prepareOkMessage, prepareMessage.getFrom());
                }
            }
        }
    };
    private boolean currentlyProposing;
    private final ProtocolMessageHandler uponAcceptMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {

            AcceptMessage acceptMessage = (AcceptMessage) msg;
            logger.info("({}) -> received AcceptMessage from ({})", myself, msg.getFrom());

            if (acceptMessage.getPaxosInstance() > paxosInstance || acceptMessage.getSeqNum() >= highestSeqNum) {
                Proposal proposal = acceptMessage.getProposal();

                logger.debug("GOT VALID ACCEPT - TYPE " + proposal.getType() + " INSTANCE: " + acceptMessage.getPaxosInstance() + "; SEQNUM: " + acceptMessage.getSeqNum() +
                        " ; MY INSTANCE:" + paxosInstance + "; MY SEQNUM: " + highestSeqNum + "; FROM: " + msg.getFrom());

                // In case of no-op there's no need to go to the AcceptOK phase.
                if (proposal.getType() == ProposalType.NOOP) {
                    logger.debug("GOT NO OP " + msg.getFrom());

                    cancelTimer(leaderCheckTimer);
                    logger.debug("LEADER CHECK = " + leaderCheckTimer);
                    leaderCheckTimer = setupTimer(new LeaderCheckTimer(), getLeaderCheckTimeout());

                    pendingProposals.remove(proposal);
                    currentlyProposing = false;
                    propose();
                    return;
                }

                highestSeqNum = acceptMessage.getSeqNum();

                // clear old oks only because might have received valid oks before the accept
                acceptOks.removeIf(ok -> ok.getPaxosInstance() != acceptMessage.getPaxosInstance() && ok.getSeqNum() != acceptMessage.getSeqNum());

                if (acceptMessage.getFrom().compareTo(paxosLeader) != 0) {
                    logger.debug("PAXOS LEADER CHANGE AT PAXOS LEVEL - FROM " + paxosLeader + " TO " + acceptMessage.getFrom());
                    prepareOks.clear();
                    paxosLeader = acceptMessage.getFrom();
                    triggerNotification(new PaxosLeaderChange(paxosLeader));
                }

                for (Host member : membership) {
                    logger.debug("[paxos protocol message] (network) " + myself.getPort());
                    sendMessage(new AcceptOkMessage(acceptMessage), member);
                    // The below was used to test late leader election and recovery
                    /*if (myself.getPort() == 10002 && member.getPort() == 10003) {
                        System.err.println("NOT SENDING!!!!!!!!!!!!!!!!!!!");
                    } else {
                        sendMessage(new AcceptOkMessage(acceptMessage), member);
                    }*/
                }
            }
        }
    };
    private final ProtocolMessageHandler uponPaxosMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            PaxosMessage paxosMessage = (PaxosMessage) msg;
            logger.info("({}) -> received PaxosMessage from ({})", myself, msg.getFrom());
            logger.debug("GOT PAXOS MESSAGE TYPE " + paxosMessage.getProposal().getType());
            pendingProposals.add(paxosMessage.getProposal());
            propose();
        }
    };
    private final ProtocolMessageHandler uponAcceptOkMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            AcceptOkMessage acceptOkMessage = (AcceptOkMessage) msg;
            logger.info("({}) -> received AcceptMessage from ({})", myself, msg.getFrom());

            if (acceptOkMessage.getPaxosInstance() > paxosInstance) {
                acceptOks.add(acceptOkMessage);
                logger.debug("GOT VALID ACCEPT OK - TYPE " + acceptOkMessage.getProposal().getType() + " INSTANCE: " + acceptOkMessage.getPaxosInstance() + "; SEQNUM: " + acceptOkMessage.getSeqNum() +
                        " ; MY INSTANCE:" + paxosInstance + "; MY SEQNUM: " + highestSeqNum + "; FROM: " + msg.getFrom());
            }

            int validOks = 0;
            for (AcceptOkMessage ok : acceptOks)
                if (ok.getPaxosInstance() == acceptOkMessage.getPaxosInstance())
                    validOks++;

            if (validOks >= quorumSize()) {
                logger.debug("NEEDED " + quorumSize() + " OKS GOT " + validOks);
                acceptOks.clear();

                paxosInstance = acceptOkMessage.getPaxosInstance();

                if (acceptOkMessage.getProposal().getType() == ProposalType.ADD_REPLICA) {
                    membership.add(acceptOkMessage.getProposal().getReplica());
                    addNetworkPeer(acceptOkMessage.getProposal().getReplica());
                } else if (acceptOkMessage.getProposal().getType() == ProposalType.REMOVE_REPLICA) {
                    membership.remove(acceptOkMessage.getProposal().getReplica());
                    removeNetworkPeer(acceptOkMessage.getProposal().getReplica());
                }

                Storage.writePaxosProposal(storageFilename, acceptOkMessage.getProposal());

                logger.debug("TRIGGERING DECIDE " + acceptOkMessage.getProposal().getType());
                triggerNotification(new DecideNotification(acceptOkMessage.getProposal(), paxosInstance));

                pendingProposals.remove(acceptOkMessage.getProposal());
                logger.debug("REMAINING PENDING == " + pendingProposals.size());
                currentlyProposing = false;
                propose();
            }
        }
    };
    private final ProtocolMessageHandler uponPrepareOkMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            PrepareOkMessage prepareOkMessage = (PrepareOkMessage) msg;
            if (prepareOkMessage.getSeqNum() == highestSeqNum) {
                logger.debug("GOT PREPARE OK " + prepareOkMessage.getSeqNum() + " FROM " + msg.getFrom().getPort());
                prepareOks.add(prepareOkMessage);

                if (prepareOks.size() >= quorumSize()) { // still need quorum size I think; for 3 replicas, still need 2 oks if one crashes;
                    List<Proposal> proposals = new LinkedList<>();

                    // Find the largest proposal list.
                    for (PrepareOkMessage prepareOk : prepareOks) {
                        if (prepareOk.getProposalList().size() > proposals.size()) {
                            proposals = prepareOk.getProposalList();
                        }
                    }

                    // Am now the new leader; first thing to do; remove the previous leader;
                    logger.debug("GOT " + prepareOks.size() + " NEEDED " + quorumSize() + " ; MEMBERSHIP SIZE IS " + membership.size());
                    logger.info("AM NOW PAXOS LEADER");
                    prepareOks.clear();

                    Queue<Proposal> newPending = new LinkedList<>(proposals);
                    newPending.addAll(pendingProposals);
                    pendingProposals = newPending;

                    // Remove old leader.
                    pendingProposals.add(new Proposal(ProposalType.REMOVE_REPLICA, paxosLeader));

                    // Continue working as usual;
                    propose();
                }
            }
        }
    };
    private ProtocolRequestHandler uponStartRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest request) {
            if (paxosStarted)
                return;

            paxosStarted = true;
            StartRequest req = (StartRequest) request;
            Set<Host> members = req.getMembership();
            if (req.getLeader().compareTo(myself) != 0) {
                // Not first replica; must inform others and get state
                membership = members;
                membership.forEach(node -> addNetworkPeer(node));
                seqNum = membership.size();
                highestSeqNum = -1;
                paxosInstance = req.getInstanceNumber();
                initialInstance = paxosInstance;
                paxosLeader = req.getLeader();
                logger.debug("GOT START REQUEST; LEADER IS " + paxosLeader + "; INSTANCE: " + paxosInstance + "; SEQNUM: " + highestSeqNum);

                logger.debug("[paxos protocol message] (network) " + myself.getPort());
                sendMessage(new PaxosMessage(new Proposal(ProposalType.ADD_REPLICA, myself)), paxosLeader);
            } else {
                // First replica; can become leader immediately
                paxosLeader = myself;
                paxosInstance = 1;
                initialInstance = paxosInstance;
                membership = members;
                seqNum = membership.size();
                highestSeqNum = seqNum;
                logger.debug("GOT START REQUEST; AM LEADER - INSTANCE: " + paxosInstance + "; SEQNUM: " + highestSeqNum);
            }

            setupPeriodicTimer(
                    new NoOpTimer(),
                    leaderCheckTimeout,
                    leaderCheckTimeout
            );
            leaderCheckTimer = setupTimer(
                    new LeaderCheckTimer(),
                    getLeaderCheckTimeout()
            );
        }
    };

    private ProtocolRequestHandler uponProposeRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest request) {
            logger.debug("PAXOS LEADER IS " + paxosLeader.getPort());
            ProposeRequest req = (ProposeRequest) request;
            pendingProposals.add(req.getProposal());

            logger.debug("GOT PROPOSE REQ");
            propose();
        }
    };
    private ProtocolTimerHandler uponNoOpTimer = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            logger.debug(String.format("(%d) -> got no op timer", myself.getPort()));

            if (paxosLeader != null && paxosLeader.compareTo(myself) == 0) {
                Proposal proposal = new Proposal(ProposalType.NOOP);
                pendingProposals.add(proposal);

                logger.debug("PROPOSING NOOP");
                propose();
            }
        }
    };
    private ProtocolTimerHandler uponLeaderCheckTimer = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            logger.debug(String.format("(%d) -> leader failed", myself.getPort()));

            logger.warn("LEADER " + paxosLeader + " HAS FAILED!");

            PrepareMessage prepareMessage = new PrepareMessage(paxosInstance, getLargerSeqNum());
            for (Host member : membership) {
                if (member.compareTo(paxosLeader) != 0) { // I am guessing there's no need to send to a known disconnected channel
                    logger.debug("SENDING PREPARE MSG " + prepareMessage.toString() + " TO " + member.getPort());
                    logger.debug("[paxos protocol message] (network) " + myself.getPort());
                    sendMessage(prepareMessage, member);
                    // The below was used to test late leader election and recovery
                    /*if (myself.getPort() == 10003) {
                        sendMessage(prepareMessage, member);
                    } else {
                        System.err.println("NOT SENDING PREPARE!!!!!!!!!!!!!!1");
                    }*/
                }
            }
        }
    };

    public Paxos(INetwork net) throws HandlerRegistrationException {
        super(PROTOCOL_NAME, PROTOCOL_ID, net);

        //Declare Messages sent/received of the protocol
        registerMessageHandler(PaxosMessage.MSG_CODE, uponPaxosMessage, PaxosMessage.serializer);
        registerMessageHandler(AcceptMessage.MSG_CODE, uponAcceptMessage, AcceptMessage.serializer);
        registerMessageHandler(AcceptOkMessage.MSG_CODE, uponAcceptOkMessage, AcceptOkMessage.serializer);
        registerMessageHandler(PrepareMessage.MSG_CODE, uponPrepareMessage, PrepareMessage.serializer);
        registerMessageHandler(PrepareOkMessage.MSG_CODE, uponPrepareOkMessage, PrepareOkMessage.serializer);

        // Declare Requests
        registerRequestHandler(ProposeRequest.REQUEST_ID, uponProposeRequest);
        registerRequestHandler(StartRequest.REQUEST_ID, uponStartRequest);

        registerNotification(DecideNotification.NOTIFICATION_ID, DecideNotification.NOTIFICATION_NAME);
        registerNotification(PaxosLeaderChange.NOTIFICATION_ID, PaxosLeaderChange.NOTIFICATION_NAME);

        registerTimerHandler(NoOpTimer.TimerCode, uponNoOpTimer);
        registerTimerHandler(LeaderCheckTimer.TimerCode, uponLeaderCheckTimer);
    }

    @Override
    public void nodeUp(Host host) {
    }

    @Override
    public void nodeConnectionReestablished(Host host) {
    }

    @Override
    public void nodeDown(Host host) {
        if (host.compareTo(paxosLeader) != 0) {
            logger.debug("[paxos protocol message] (network) " + myself.getPort());
            sendMessage(new PaxosMessage(new Proposal(ProposalType.REMOVE_REPLICA, host)), paxosLeader);
        }
    }

    @Override
    public void init(Properties props) {
        registerNodeListener(this);

        this.paxosStarted = false;
        this.membership = new HashSet<>(maxMembershipSize);
        this.paxosLeader = null;
        this.paxosInstance = 0;
        this.seqNum = 0;
        this.pendingProposals = new LinkedList<>();
        this.highestSeqNum = 0;
        this.acceptOks = new HashSet<>(maxMembershipSize);
        this.currentlyProposing = false;
        this.prepareOks = new HashSet<>(maxMembershipSize);
        this.storageFilename = myself.getPort() + "__paxos";

        this.leaderCheckTimeout = Long.parseLong(props.getProperty("NoOpTimerPeriod"));

        this.maxMembershipSize = Integer.parseInt(props.getProperty("MaxMembershipSize"));
    }

    private void propose() {
        Proposal proposal = pendingProposals.peek();

        while (proposal != null) {
            if (proposal.getType() != ProposalType.REMOVE_REPLICA && proposal.getType() != ProposalType.ADD_REPLICA) {
                break;
            } else if (proposal.getType() == ProposalType.REMOVE_REPLICA && membership.contains(proposal.getReplica())) {
                break;
            } else if (proposal.getType() == ProposalType.ADD_REPLICA && !membership.contains(proposal.getReplica())) {
                break;
            }
            logger.debug(String.format("REMOVING REDUNDANT PROPOSAL: %s\n", proposal));
            pendingProposals.remove(proposal);
            proposal = pendingProposals.peek();
        }

        if (proposal != null && !currentlyProposing) {
            currentlyProposing = true;

            logger.debug("PROPOSING TO MEMBERSHIP OF SIZE = " + membership.size());
            AcceptMessage acceptMessage = new AcceptMessage(paxosInstance + 1, highestSeqNum, proposal);
            for (Host member : membership) {
                logger.debug("SENDING ACCEPT MSG TO " + member.getPort() + " | INSTANCE: " + paxosInstance + " ; SEQNUM: " + highestSeqNum);
                logger.debug("[paxos protocol message] (network) " + myself.getPort());
                sendMessage(acceptMessage, member);

                // The below was used to test late leader election and recovery;
                /*if (proposal.getType() != ProposalType.NOOP && myself.getPort() == 10002 && member.getPort() == 10003) {
                    System.err.println("NOT SENDING!!!!!!!!!!!!!!!!!!!");
                } else {
                    sendMessage(acceptMessage, member);
                }*/
            }
        } else {
            logger.debug("NO PROPOSE - PROPOSAL IS " + proposal + " ; CURRENTLY PROPOSING IS " + currentlyProposing);
        }
    }

    public long quorumSize() {
        return membership.size() / 2 + 1;
    }

    private long getLargerSeqNum() {
        return seqNum + (highestSeqNum / maxMembershipSize) * maxMembershipSize + maxMembershipSize;
    }

    private long getLeaderCheckTimeout() {
        long timeout = (long) (leaderCheckTimeout * (1.5 + ThreadLocalRandom.current().nextFloat()));
        logger.debug("MY TIMEOUT IS = " + timeout);
        return timeout;
    }
}