package asd.project.protocols.paxos.timers;

import babel.timer.ProtocolTimer;

public class NoOpTimer extends ProtocolTimer {

    public static final short TimerCode = 601;

    public NoOpTimer() {
        super(NoOpTimer.TimerCode);
    }

    @Override
    public Object clone() {
        return this;
    }
}