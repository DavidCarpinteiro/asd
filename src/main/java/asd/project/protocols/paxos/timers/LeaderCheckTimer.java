package asd.project.protocols.paxos.timers;

import babel.timer.ProtocolTimer;

public class LeaderCheckTimer extends ProtocolTimer {

    public static final short TimerCode = 602;

    public LeaderCheckTimer() {
        super(LeaderCheckTimer.TimerCode);
    }

    @Override
    public Object clone() {
        return this;
    }
}