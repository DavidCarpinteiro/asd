package asd.project.protocols.paxos.requests;

import babel.requestreply.ProtocolRequest;
import network.Host;

import java.util.Set;

public class StartRequest extends ProtocolRequest {

    public final static short REQUEST_ID = 602;

    private final Set<Host> membership;
    private final Host leader;
    private final long instanceNumber;

    public StartRequest(Set<Host> membership, Host leader, long instanceNumber) {
        super(REQUEST_ID);
        this.membership = membership;
        this.leader = leader;
        this.instanceNumber = instanceNumber;
    }

    public Host getLeader() {
        return leader;
    }

    public Set<Host> getMembership() {
        return membership;
    }

    public long getInstanceNumber() {
        return instanceNumber;
    }

    @Override
    public String toString() {
        return String.format("StartRequest{membership=%s, leader=%s, instanceNumber=%s}", membership.size(), leader, instanceNumber);
    }
}
