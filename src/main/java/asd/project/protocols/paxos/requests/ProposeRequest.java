package asd.project.protocols.paxos.requests;

import asd.project.utils.Proposal;
import babel.requestreply.ProtocolRequest;

public class ProposeRequest extends ProtocolRequest {

    public final static short REQUEST_ID = 601;

    private final Proposal proposal;

    public ProposeRequest(Proposal proposal) {
        super(REQUEST_ID);
        this.proposal = proposal;
    }

    public Proposal getProposal() {
        return proposal;
    }

    @Override
    public String toString() {
        return String.format("ProposeRequest{Proposal=%s}", proposal.toString());
    }
}