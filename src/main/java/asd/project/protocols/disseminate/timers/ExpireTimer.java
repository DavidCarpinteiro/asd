package asd.project.protocols.disseminate.timers;

import babel.timer.ProtocolTimer;

public class ExpireTimer extends ProtocolTimer {
    public static final short TimerCode = 501;

    public ExpireTimer() {
        super(ExpireTimer.TimerCode);
    }

    @Override
    public Object clone() {
        return this;
    }
}