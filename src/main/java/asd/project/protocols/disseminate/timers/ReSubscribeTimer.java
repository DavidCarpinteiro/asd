package asd.project.protocols.disseminate.timers;

import babel.timer.ProtocolTimer;

public class ReSubscribeTimer extends ProtocolTimer {
    public static final short TimerCode = 502;

    public ReSubscribeTimer() {
        super(ReSubscribeTimer.TimerCode);
    }

    @Override
    public Object clone() {
        return this;
    }
}