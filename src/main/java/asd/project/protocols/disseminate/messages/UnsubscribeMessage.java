package asd.project.protocols.disseminate.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.ISerializer;

public class UnsubscribeMessage extends ProtocolMessage {
    public final static short MSG_CODE = 502;
    public static final ISerializer<UnsubscribeMessage> serializer = new ISerializer<>() {
        @Override
        public void serialize(UnsubscribeMessage req, ByteBuf out) {
            byte[] idBytes = req.topic.getBytes();
            out.writeInt(idBytes.length);
            out.writeBytes(idBytes);
            out.writeInt(req.popularity);
        }

        @Override
        public UnsubscribeMessage deserialize(ByteBuf in) {
            byte[] idBytes = new byte[in.readInt()];
            in.readBytes(idBytes);
            String topic = new String(idBytes);

            UnsubscribeMessage msg = new UnsubscribeMessage(topic);
            msg.popularity = in.readInt();
            return msg;
        }

        @Override
        public int serializedSize(UnsubscribeMessage req) {
            if (req.size == -1) {
                req.size = 2 * Integer.BYTES + req.topic.getBytes().length;
            }
            return req.size;
        }
    };
    private final String topic;
    private int popularity;
    private int size;

    public UnsubscribeMessage(String topic) {
        super(MSG_CODE);
        this.topic = topic;
        this.popularity = -1;
        this.size = -1;
    }

    public String getTopic() {
        return topic;
    }

    public int getPopularity() {
        return popularity;
    }

    public void updatePopularity() {
        popularity -= 1;
    }

    @Override
    public String toString() {
        return String.format("UnsubscribeMessage{topic=%s}", topic);
    }
}
