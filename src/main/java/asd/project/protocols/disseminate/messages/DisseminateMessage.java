package asd.project.protocols.disseminate.messages;

import babel.protocol.event.ProtocolMessage;
import io.netty.buffer.ByteBuf;
import network.Host;
import network.ISerializer;

import java.net.UnknownHostException;

public class DisseminateMessage extends ProtocolMessage {
    public final static short MSG_CODE = 501;
    public static final ISerializer<DisseminateMessage> serializer = new ISerializer<>() {
        @Override
        public void serialize(DisseminateMessage req, ByteBuf out) {
            byte[] idBytes = req.topic.getBytes();
            out.writeInt(idBytes.length);
            out.writeBytes(idBytes);

            idBytes = req.message.getBytes();
            out.writeInt(idBytes.length);
            out.writeBytes(idBytes);

            req.leader.serialize(out);

            out.writeLong(req.seqNum);
        }

        @Override
        public DisseminateMessage deserialize(ByteBuf in) throws UnknownHostException {
            byte[] idBytes = new byte[in.readInt()];
            in.readBytes(idBytes);
            String topic = new String(idBytes);

            idBytes = new byte[in.readInt()];
            in.readBytes(idBytes);
            String message = new String(idBytes);

            Host leader = Host.deserialize(in);

            long seqNum = in.readLong();

            return new DisseminateMessage(topic, message, leader, seqNum);
        }

        @Override
        public int serializedSize(DisseminateMessage req) {
            if (req.size == -1) {
                req.size = 2 * Integer.BYTES + req.topic.getBytes().length + req.message.getBytes().length + req.leader.serializedSize() + Long.BYTES;
            }
            return req.size;
        }
    };
    private final String topic;
    private final String message;
    private final Host leader;
    private final long seqNum;
    private int size;

    public DisseminateMessage(String topic, String message, Host leader, long seqNum) {
        super(MSG_CODE);
        this.topic = topic;
        this.message = message;
        this.leader = leader;
        this.seqNum = seqNum;
        this.size = -1;
    }

    public String getMessage() {
        return message;
    }

    public String getTopic() {
        return topic;
    }

    public Host getLeader() {
        return leader;
    }

    public long getSeqNum() {
        return seqNum;
    }

    @Override
    public String toString() {
        return String.format("DisseminateMessage{topic=%s, message=%s, leader=%s, seqNum=%s}", topic, message, leader, seqNum);
    }
}
