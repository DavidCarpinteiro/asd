package asd.project.protocols.disseminate;

import asd.project.protocols.dht.Chord;
import asd.project.protocols.dht.FingerTable;
import asd.project.protocols.dht.requests.*;
import asd.project.protocols.disseminate.messages.DisseminateMessage;
import asd.project.protocols.disseminate.messages.UnsubscribeMessage;
import asd.project.protocols.disseminate.requests.DisseminateRequest;
import asd.project.protocols.disseminate.requests.DisseminateSubscribeRequest;
import asd.project.protocols.disseminate.requests.DisseminateUnsubscribeRequest;
import asd.project.protocols.disseminate.requests.MessageDelivery;
import asd.project.protocols.disseminate.timers.ExpireTimer;
import asd.project.protocols.disseminate.timers.ReSubscribeTimer;
import asd.project.protocols.pubsub.PubSub;
import asd.project.protocols.pubsub.notifications.RemovePopularity;
import asd.project.protocols.pubsub.notifications.UpdatePopularity;
import asd.project.utils.Message;
import asd.project.utils.Message.MessageType;
import asd.project.utils.Pair;
import babel.exceptions.DestinationProtocolDoesNotExist;
import babel.exceptions.HandlerRegistrationException;
import babel.exceptions.NotificationDoesNotExistException;
import babel.exceptions.ProtocolDoesNotExist;
import babel.handlers.*;
import babel.notification.ProtocolNotification;
import babel.protocol.GenericProtocol;
import babel.protocol.event.ProtocolMessage;
import babel.requestreply.ProtocolReply;
import babel.requestreply.ProtocolRequest;
import babel.timer.ProtocolTimer;
import network.Host;
import network.INetwork;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Disseminate extends GenericProtocol {
    public final static short PROTOCOL_ID = 500;
    public final static String PROTOCOL_NAME = "Disseminate";
    private static final Logger logger = LogManager.getLogger(Disseminate.class);
    // Set of nodes that this node knows are interested in a given topic (children nodes). Contains subscribe time.
    private Map<String, Map<Host, Long>> interested;
    // Set of nodes that this node knows are interested in a given topic (parent nodes). Contains subscribe time.
    private Map<String, Pair<Host, Long>> parents;

    private Map<String, Map<Host, Integer>> interestedPopularity;

    private Map<String, Host> leaders;
    private final ProtocolMessageHandler uponUnsubscribeMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage protocolMessage) {
            UnsubscribeMessage unsubscribeMessage = (UnsubscribeMessage) protocolMessage;
            String topic = unsubscribeMessage.getTopic();

            Map<Host, Integer> interestedChildren = interestedPopularity.getOrDefault(topic, new HashMap<>(10));
            interestedChildren.remove(unsubscribeMessage.getFrom());
            interestedPopularity.putIfAbsent(topic, interestedChildren);

            Map<Host, Long> interestedNodes = interested.getOrDefault(topic, new HashMap<>());
            interestedNodes.remove(unsubscribeMessage.getFrom());
            interested.putIfAbsent(topic, interestedNodes);

            Pair<Host, Long> pair = parents.get(topic);
            Host parent = (pair != null) ? pair.first : null;

            if (interestedNodes.isEmpty()) {
                // Case when this node is no longer part of a path in the tree. We can forget everything we know about
                // the topic. In case we are not the topic leader, we have to inform our parent that we are no longer
                // interested in the topic.
                if (parent != null) {
                    sendMessageSideChannel(unsubscribeMessage, parent);
                    logger.debug("[disseminate protocol message] (network) " + myself.getPort());
                    parents.remove(topic);
                } else {
                    removeTopicPopularity(topic);
                }
                interested.remove(topic);
                leaders.remove(topic);
            } else {
                // Case when we are still part of a path in the tree. There are two possible scenarios, either we are
                // the topic leader, or we are not. If we are the topic leader we simple update the topic's popularity.
                // If we are not the topic leader, we need to inform him so he can update the topic's popularity.
                int popularity = getTopicPopularity(topic);
                if (parent == null) {
                    updatePopularity(topic, popularity);
                }
            }
        }
    };
    private final ProtocolMessageHandler uponDisseminateMessage = new ProtocolMessageHandler() {
        @Override
        public void receive(ProtocolMessage msg) {
            DisseminateMessage disseminateMessage = (DisseminateMessage) msg;

            String topic = disseminateMessage.getTopic();
            String message = disseminateMessage.getMessage();
            Host leader = disseminateMessage.getLeader();
            long seqNum = disseminateMessage.getSeqNum();

            if (parents.get(topic) != null)
                leaders.put(topic, leader);


            Pair<Host, Long> pair = parents.get(topic);
            Host parent = (pair != null) ? pair.first : null;

            if (parent != null && parent.compareTo(disseminateMessage.getFrom()) != 0) {
                throw new RuntimeException("Should not happen, check yourself");
            }

            for (Host host : interested.getOrDefault(topic, new HashMap<>()).keySet()) {
                if (host.compareTo(myself) == 0) {
                    MessageDelivery messageDelivery = new MessageDelivery(topic, message, seqNum);
                    sendPubSubReply(messageDelivery);
                } else {
                    sendMessageSideChannel(disseminateMessage, host);
                    logger.debug("[disseminate protocol message] (network) " + myself.getPort());
                }
            }
        }
    };
    private long expireTime;
    private ProtocolRequestHandler uponDisseminateRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest request) {
            DisseminateRequest req = (DisseminateRequest) request;
            String topic = req.getTopic();
            String message = req.getMessage();
            long seqNum = req.getSeqNum();

            BigInteger id = FingerTable.generate(topic);
            Message publishMessage = new Message(Message.MessageType.PUBLISH, topic, message, seqNum);

            logger.info("({}): sending route request for publish ({})", myself, publishMessage);

            Host leader = leaders.get(topic);

            if (leader != null) {
                logger.info("Sending directly to leader");
                // Assume leader is always correct
                DisseminateMessage msg = new DisseminateMessage(topic, message, leader, seqNum);
                sendMessageSideChannel(msg, leader);
                logger.debug("[disseminate protocol message] (network) " + myself.getPort());
            } else {
                RouteRequest routeRequest = new RouteRequest(id, publishMessage);
                sendChordRequest(routeRequest);
            }

        }
    };
    private ProtocolRequestHandler uponDisseminateSubscribeRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest request) {
            DisseminateSubscribeRequest req = (DisseminateSubscribeRequest) request;
            String topic = req.getTopic();

            Map<Host, Long> interestedSet = interested.getOrDefault(req.getTopic(), new HashMap<>());
            // Initialize the number of nodes that are going to be added to the tree. It might be the case that this
            // node is already in the tree of the given topic, in which case, this number starts at zero.
            int totalPopularity = getTopicPopularity(topic);

            interestedSet.put(myself, System.currentTimeMillis());
            interested.putIfAbsent(topic, interestedSet);

            BigInteger id = FingerTable.generate(topic);
            Message subscribeMessage = new Message(MessageType.SUBSCRIBE, topic, totalPopularity);

            logger.info("({}): sending first forward request for subscribe ({})", myself, subscribeMessage);
            ForwardRequest route = new ForwardRequest(id, subscribeMessage);
            sendChordRequest(route);
        }
    };
    private ProtocolRequestHandler uponDisseminateUnsubscribeRequest = new ProtocolRequestHandler() {
        @Override
        public void uponRequest(ProtocolRequest request) {
            DisseminateUnsubscribeRequest req = (DisseminateUnsubscribeRequest) request;
            String topic = req.getTopic();

            Map<Host, Long> interestedNodes = interested.getOrDefault(req.getTopic(), new HashMap<>());
            interestedNodes.remove(myself);
            interested.putIfAbsent(topic, interestedNodes);

            Pair<Host, Long> pair = parents.get(req.getTopic());
            Host parent = (pair != null) ? pair.first : null;

            // If we know zero interested nodes, it means that we are no longer part of a path in the tree, we can
            // forget everything we know about the topic.
            if (interestedNodes.isEmpty()) {
                // If we are not the topic leader we need to inform our parent that we are no longer interested.
                if (parent != null) {
                    UnsubscribeMessage unsubscribeMessage = new UnsubscribeMessage(topic);
                    sendMessageSideChannel(unsubscribeMessage, parent);
                    logger.debug("[disseminate protocol message] (network) " + myself.getPort());
                    parents.remove(topic);
                } else {
                    removeTopicPopularity(topic);
                }

                interested.remove(topic);
                leaders.remove(topic);

            } else if (parent == null) {
                // We are the topic leader but there are still nodes interested in the topic.
                updatePopularity(topic, getTopicPopularity(topic));
            }
        }
    };
    private ProtocolNotificationHandler uponRouteDelivery = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification notification) {
            RouteDelivery delivery = (RouteDelivery) notification;
            Message message = delivery.getMessage();
            String topic = message.getTopic();

            switch (message.getType()) {
                case SUBSCRIBE:
                    // Simply update the interested nodes.
                    Map<Host, Long> interestedNodes = interested.getOrDefault(topic, new HashMap<>());
                    interestedNodes.put(delivery.getHostSender(), System.currentTimeMillis());
                    interested.putIfAbsent(topic, interestedNodes);

                    // Update topic's popularity, because whenever the Disseminate layer receives a RouteDelivery
                    // we can safely assume that this node is the topic leader.
                    Map<Host, Integer> interestedChildren = interestedPopularity.getOrDefault(topic, new HashMap<>(10));
                    interestedChildren.put(delivery.getHostSender(), message.getPopularity());
                    interestedPopularity.putIfAbsent(topic, interestedChildren);

                    updatePopularity(topic, getTopicPopularity(topic));
                    break;

                case PUBLISH:
                    for (Host host : interested.getOrDefault(topic, new HashMap<>()).keySet()) {
                        if (host.compareTo(myself) != 0) {
                            DisseminateMessage disseminate = new DisseminateMessage(topic, message.getMessage(), myself, message.getSeqNum());
                            sendMessageSideChannel(disseminate, host);
                            logger.debug("[disseminate protocol message] (network) " + myself.getPort());
                        } else {
                            MessageDelivery messageDelivery = new MessageDelivery(topic, message.getMessage(), message.getSeqNum());
                            sendPubSubReply(messageDelivery);
                        }
                    }
                    break;

                default:
                    // Ignore.
                    break;
            }
        }
    };
    private ProtocolReplyHandler uponForwardInfo = new ProtocolReplyHandler() {
        @Override
        public void uponReply(ProtocolReply reply) {
            ForwardInfo info = (ForwardInfo) reply;
            Message message = info.getMessage();

            if (message.getType() == MessageType.SUBSCRIBE) {
                if (info.getNextHost().compareTo(myself) == 0) {
                    logger.warn("LOOP DETECTED IN THE TREE: adding parent to topic that is myself");
                }
                parents.put(message.getTopic(), new Pair<>(info.getNextHost(), System.currentTimeMillis()));
            }
        }
    };
    private ProtocolNotificationHandler uponForwardDelivery = new ProtocolNotificationHandler() {
        @Override
        public void uponNotification(ProtocolNotification notification) {
            ForwardDelivery delivery = (ForwardDelivery) notification;
            Message message = delivery.getMessage();
            String topic = message.getTopic();

            if (message.getType() == MessageType.SUBSCRIBE) {
                Map<Host, Integer> interestedChildren = interestedPopularity.getOrDefault(topic, new HashMap<>(10));
                interestedChildren.put(delivery.getHostSender(), delivery.getMessage().getPopularity());
                interestedPopularity.putIfAbsent(topic, interestedChildren);

                Map<Host, Long> interestedNodes = interested.getOrDefault(topic, new HashMap<>());
                Pair<Host, Long> pair = parents.get(topic);
                Host parent = (pair != null) ? pair.first : null;

                message = new Message(message.getType(), message.getTopic(), getTopicPopularity(topic));

                if (interestedNodes.isEmpty() && parent == null) {
                    // Case when this node is not in the tree.
                    BigInteger id = FingerTable.generate(topic);
                    ForwardRequest forward = new ForwardRequest(id, message);
                    sendChordRequest(forward);
                } else if (interestedNodes.isEmpty() || parent == null) {
                    // Error case, either we know interested nodes and don't have a parent (yet we are not the leader),
                    // or we have a parent but know no interested nodes, in which case we shouldn't be in the tree.
                    throw new RuntimeException("No Luke, I'm your father!");
                }
                interestedNodes.put(delivery.getHostSender(), System.currentTimeMillis());
                interested.putIfAbsent(topic, interestedNodes);
            } else {
                throw new RuntimeException(
                        "Disseminate received a ForwardDelivery with an unexpected message (" + message + ")."
                );
            }
        }
    };
    private ProtocolTimerHandler uponExpireTimer = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            long now = System.currentTimeMillis();

            // Expire interested nodes.
            Map<Host, Integer> emptyMap = new HashMap<>();
            for (Map.Entry<String, Map<Host, Long>> entry : interested.entrySet()) {
                String topic = entry.getKey();
                interested.get(topic).entrySet().removeIf(e -> {
                    Host host = e.getKey();
                    long subscribeTime = e.getValue();
                    boolean isExpired = (subscribeTime + expireTime < now && host.compareTo(myself) != 0);
                    if (isExpired) {
                        interestedPopularity.getOrDefault(topic, emptyMap).remove(host);
                    }
                    return isExpired;
                });
            }

            // Remove all topic entries that have no interested nodes.
            interested.entrySet().removeIf(e -> e.getValue().isEmpty());
            // Expire parent nodes.
            parents.entrySet().removeIf(e -> {
                long subscribeTime = e.getValue().second;
                return subscribeTime + expireTime < now;
            });
        }
    };
    private ProtocolTimerHandler uponReSubscribeTimer = new ProtocolTimerHandler() {
        @Override
        public void uponTimer(ProtocolTimer timer) {
            for (String topic : interested.keySet()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Topic: ").append(topic).append(" [ ");
                interested.get(topic).forEach((k, v) -> sb.append(k).append(" "));
                sb.append("]");
                logger.warn(sb.toString());
            }

            for (String topic : parents.keySet()) {
                logger.warn("Topic: " + topic + " -> Parent [ " + parents.get(topic).first + " ]");

                BigInteger id = FingerTable.generate(topic);
                Message resubscribeMessage = new Message(MessageType.SUBSCRIBE, topic, getTopicPopularity(topic));

                logger.info("({}): sending re-subscribe forward request ({})", myself, resubscribeMessage);
                ForwardRequest route = new ForwardRequest(id, resubscribeMessage);
                sendChordRequest(route);
            }
        }
    };

    public Disseminate(INetwork net) throws HandlerRegistrationException {
        super(PROTOCOL_NAME, PROTOCOL_ID, net);

        registerRequestHandler(DisseminateRequest.REQUEST_ID, uponDisseminateRequest);
        registerRequestHandler(DisseminateSubscribeRequest.REQUEST_ID, uponDisseminateSubscribeRequest);
        registerRequestHandler(DisseminateUnsubscribeRequest.REQUEST_ID, uponDisseminateUnsubscribeRequest);

        registerReplyHandler(ForwardInfo.REPLY_ID, uponForwardInfo);

        registerMessageHandler(DisseminateMessage.MSG_CODE, uponDisseminateMessage, DisseminateMessage.serializer);
        registerMessageHandler(UnsubscribeMessage.MSG_CODE, uponUnsubscribeMessage, UnsubscribeMessage.serializer);

        registerTimerHandler(ExpireTimer.TimerCode, uponExpireTimer);
        registerTimerHandler(ReSubscribeTimer.TimerCode, uponReSubscribeTimer);

        registerNotification(RemovePopularity.NOTIFICATION_ID, RemovePopularity.NOTIFICATION_NAME);
        registerNotification(UpdatePopularity.NOTIFICATION_ID, UpdatePopularity.NOTIFICATION_NAME);
    }

    @Override
    public void init(Properties props) {
        this.interested = new HashMap<>(100);
        this.parents = new HashMap<>(100);
        this.interestedPopularity = new HashMap<>(100);

        this.expireTime = Long.parseLong(props.getProperty("ExpireTimerPeriod"));

        leaders = new HashMap<>();

        try {
            registerNotificationHandler(Chord.PROTOCOL_ID, ForwardDelivery.NOTIFICATION_ID, uponForwardDelivery);
            registerNotificationHandler(Chord.PROTOCOL_ID, RouteDelivery.NOTIFICATION_ID, uponRouteDelivery);
        } catch (HandlerRegistrationException | NotificationDoesNotExistException | ProtocolDoesNotExist e) {
            e.printStackTrace();
            System.exit(1);
        }

        setupPeriodicTimer(
                new ExpireTimer(),
                this.expireTime,
                this.expireTime
        );
        setupPeriodicTimer(
                new ReSubscribeTimer(),
                Long.parseLong(props.getProperty("SubscribeTimerPeriod")),
                Long.parseLong(props.getProperty("SubscribeTimerPeriod"))
        );
    }

    private void sendChordRequest(ProtocolRequest request) {
        request.setDestination(Chord.PROTOCOL_ID);
        try {
            sendRequest(request);
        } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
            destinationProtocolDoesNotExist.printStackTrace();
            System.exit(1);
        }
    }

    private void sendPubSubReply(ProtocolReply reply) {
        reply.setDestination(PubSub.PROTOCOL_ID);
        try {
            sendReply(reply);
        } catch (DestinationProtocolDoesNotExist destinationProtocolDoesNotExist) {
            destinationProtocolDoesNotExist.printStackTrace();
            System.exit(1);
        }
    }

    private void updatePopularity(String topic, int delta) {
        UpdatePopularity notification = new UpdatePopularity(topic, delta);
        triggerNotification(notification);
    }

    private void removeTopicPopularity(String topic) {
        RemovePopularity notification = new RemovePopularity(topic);
        triggerNotification(notification);
    }

    private int getTopicPopularity(String topic) {
        return 1 + interestedPopularity
                .getOrDefault(topic, new HashMap<>())
                .values()
                .stream()
                .mapToInt(Integer::intValue)
                .sum();
    }
}
