package asd.project.protocols.disseminate.requests;

import babel.requestreply.ProtocolRequest;

public class DisseminateUnsubscribeRequest extends ProtocolRequest {

    public final static short REQUEST_ID = 503;

    private final String topic;

    public DisseminateUnsubscribeRequest(String topic) {
        super(REQUEST_ID);
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    @Override
    public String toString() {
        return String.format("DisseminateUnsubscribeRequest{topic=%s}", topic);
    }
}