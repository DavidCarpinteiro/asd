package asd.project.protocols.disseminate.requests;

import babel.requestreply.ProtocolRequest;

public class DisseminateRequest extends ProtocolRequest {

    public final static short REQUEST_ID = 501;

    private final String topic, message;

    private final long seqNum;

    public DisseminateRequest(String topic, String message, long seqNum) {
        super(REQUEST_ID);
        this.topic = topic;
        this.message = message;
        this.seqNum = seqNum;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    public long getSeqNum() {
        return seqNum;
    }

    @Override
    public String toString() {
        return String.format("DisseminateRequest{topic=%s, message=%s, seqNum=%s}", topic, message, seqNum);
    }
}