package asd.project.protocols.disseminate.requests;

import babel.requestreply.ProtocolReply;

public class MessageDelivery extends ProtocolReply {

    public static final short REPLY_ID = DisseminateRequest.REQUEST_ID;

    private final String topic, message;

    private final long seqNum;

    public MessageDelivery(String topic, String message, long seqNum) {
        super(MessageDelivery.REPLY_ID);
        this.message = message;
        this.topic = topic;
        this.seqNum = seqNum;
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    public long getSeqNum() {
        return seqNum;
    }

    @Override
    public String toString() {
        return String.format("MessageDelivery{topic=%s, message=%s, seqNum=%s}", topic, message, seqNum);
    }
}
