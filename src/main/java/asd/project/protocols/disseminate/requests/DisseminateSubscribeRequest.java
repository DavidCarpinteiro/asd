package asd.project.protocols.disseminate.requests;

import babel.requestreply.ProtocolRequest;

public class DisseminateSubscribeRequest extends ProtocolRequest {

    public final static short REQUEST_ID = 502;

    private final String topic;

    public DisseminateSubscribeRequest(String topic) {
        super(REQUEST_ID);
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    @Override
    public String toString() {
        return String.format("DisseminateSubscribeRequest{topic=%s}", topic);
    }
}