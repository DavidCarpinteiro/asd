package asd.project.utils;

public class TimedEntry<T> implements Comparable<TimedEntry<T>> {

    private long additionTime;
    private T object;

    public TimedEntry(T object) {
        this.additionTime = System.currentTimeMillis();
        this.object = object;
    }

    public T getObject() {
        return object;
    }

    public long getAdditionTime() {
        return additionTime;
    }

    @Override
    public int compareTo(TimedEntry<T> timedEntry) {
        if (this.getAdditionTime() > timedEntry.getAdditionTime())
            return 1;
        else if (this.getAdditionTime() < timedEntry.getAdditionTime())
            return -1;
        return 0;
    }
}
