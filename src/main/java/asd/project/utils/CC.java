package asd.project.utils;

import java.util.HashSet;
import java.util.Set;
import java.util.StringJoiner;

public class CC {

    private final char ESC = '\033';
    private final char FOREGROUND_COLOR = '3';
    private final char BACKGROUND_COLOR = '4';
    private final String START = "[";
    private final String END = "m";
    private final String SEPARATOR = ";";
    private COLOR foreground_color;
    private COLOR background_color;
    private Set<EFFECT> effects;
    private String s;
    private CC(String s, COLOR color) {
        this.s = s;
        this.foreground_color = color;
        this.background_color = COLOR.DEFAULT;
        this.effects = new HashSet<>();
    }

    public static CC BLACK(String s) {
        return new CC(s, COLOR.BLACK);
    }

    public static CC RED(String s) {
        return new CC(s, COLOR.RED);
    }

    // ---------- Foregroud Colors ----------

    public static CC GREEN(String s) {
        return new CC(s, COLOR.GREEN);
    }

    public static CC YELLOW(String s) {
        return new CC(s, COLOR.YELLOW);
    }

    public static CC BLUE(String s) {
        return new CC(s, COLOR.BLUE);
    }

    public static CC PURPLE(String s) {
        return new CC(s, COLOR.PURPLE);
    }

    public static CC CYAN(String s) {
        return new CC(s, COLOR.CYAN);
    }

    public static CC WHITE(String s) {
        return new CC(s, COLOR.WHITE);
    }

    public static CC DEFAULT(String s) {
        return new CC(s, COLOR.DEFAULT);
    }

    public final CC BLACK_B() {
        return setBackgroundColor(COLOR.BLACK);
    }

    public final CC RED_B() {
        return setBackgroundColor(COLOR.RED);
    }

    // ---------- Bakegroud Colors ----------

    public final CC GREEN_B() {
        return setBackgroundColor(COLOR.GREEN);
    }

    public final CC YELLOW_B() {
        return setBackgroundColor(COLOR.YELLOW);
    }

    public final CC BLUE_B() {
        return setBackgroundColor(COLOR.BLUE);
    }

    public final CC PURPLE_B() {
        return setBackgroundColor(COLOR.PURPLE);
    }

    public final CC CYAN_B() {
        return setBackgroundColor(COLOR.CYAN);
    }

    public final CC WHITE_B() {
        return setBackgroundColor(COLOR.WHITE);
    }

    public final CC DEFAULT_B() {
        return setBackgroundColor(COLOR.DEFAULT);
    }

    private CC setBackgroundColor(COLOR c) {
        this.background_color = c;
        return this;
    }

    public final CC RESET() {
        effects.add(EFFECT.RESET);
        return this;
    }

    public final CC BOLD() {
        effects.add(EFFECT.BOLD);
        return this;
    }

    // ---------- Effects ----------

    public final CC FAINT() {
        effects.add(EFFECT.FAINT);
        return this;
    }

    public final CC ITALIC() {
        effects.add(EFFECT.ITALIC);
        return this;
    }

    public final CC UNDERLINE() {
        effects.add(EFFECT.UNDERLINE);
        return this;
    }

    public final CC SLOW_BLINK() {
        effects.add(EFFECT.SLOW_BLINK);
        return this;
    }

    public final CC FAST_BLINK() {
        effects.add(EFFECT.FAST_BLINK);
        return this;
    }

    public final CC INVERSE() {
        effects.add(EFFECT.INVERSE);
        return this;
    }

    public final CC CONCEAL() {
        effects.add(EFFECT.CONCEAL);
        return this;
    }

    public final CC STRIKETHROUGH() {
        effects.add(EFFECT.STRIKETHROUGH);
        return this;
    }

    private String buildFormat() {
        StringBuilder sb = new StringBuilder();

        sb.append(ESC); // \033
        sb.append(START); // [

        sb.append(FOREGROUND_COLOR); // 3
        sb.append(this.foreground_color.getCode()); //0
        sb.append(SEPARATOR); // ;

        sb.append(BACKGROUND_COLOR); // 4
        sb.append(this.background_color.getCode()); //0


        if (this.effects.size() > 0) {
            sb.append(SEPARATOR); // ;
            StringJoiner join_effects = new StringJoiner(SEPARATOR);
            for (EFFECT f : effects) {
                join_effects.add(f.getCode());
            }
            sb.append(join_effects); // ; ;
        }

        sb.append(END); // m

        sb.append(this.s);

        // Reset Sequence
        sb.append(ESC);
        sb.append(START);
        sb.append(EFFECT.RESET.getCode());
        sb.append(END);

        return sb.toString();
    }

    @Override
    public String toString() {
        return buildFormat();
    }

    private void printASCIcode(String s) {
        char[] cc = s.toCharArray();
        for (char c : cc) {
            System.out.println((int) c);
        }
    }

    enum COLOR {
        BLACK("0"),
        RED("1"),
        GREEN("2"),
        YELLOW("3"),
        BLUE("4"),
        PURPLE("5"),
        CYAN("6"),
        WHITE("7"),
        DEFAULT("9");

        private final String code;

        COLOR(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

    enum EFFECT {
        RESET("0"),
        BOLD("1"),
        FAINT("2"),
        ITALIC("3"),
        UNDERLINE("4"),
        SLOW_BLINK("5"),
        FAST_BLINK("6"),
        INVERSE("7"),
        CONCEAL("8"),
        STRIKETHROUGH("9");

        private final String code;

        EFFECT(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

}