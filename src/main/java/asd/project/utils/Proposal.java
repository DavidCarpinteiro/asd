package asd.project.utils;

import io.netty.buffer.ByteBuf;
import network.Host;

import java.net.UnknownHostException;
import java.util.UUID;

public class Proposal implements Comparable<Proposal> {

    private final String topic;
    private final String message;
    private final Host replica;
    private final UUID proposalId;
    private final Proposal.ProposalType type;
    private final long seqNum;
    private int size = -1;

    public Proposal(Proposal.ProposalType type) {
        this(type, null, null, null, UUID.randomUUID(), -1L);
        if (type != ProposalType.NOOP) {
            throw new RuntimeException("Invalid arguments for Proposal with type " + type);
        }
    }

    public Proposal(Proposal.ProposalType type, String topic) {
        this(type, topic, null, null, UUID.randomUUID(), -1L);
        if (type != Proposal.ProposalType.SUBSCRIBE && type != Proposal.ProposalType.UNSUBSCRIBE) {
            throw new RuntimeException("Invalid arguments for Proposal with type " + type);
        }
    }

    public Proposal(Proposal.ProposalType type, String topic, String message, long seqNum) {
        this(type, topic, message, null, UUID.randomUUID(), seqNum);
        if (type != Proposal.ProposalType.PUBLISH) {
            throw new RuntimeException("Invalid arguments for Proposal with type " + type);
        }
    }

    public Proposal(Proposal.ProposalType type, Host replica) {
        this(type, null, null, replica, UUID.randomUUID(), -1L);
        if (type != ProposalType.ADD_REPLICA && type != ProposalType.REMOVE_REPLICA) {
            throw new RuntimeException("Invalid arguments for Proposal with type " + type);
        }
    }

    private Proposal(ProposalType type, String topic, String message, Host replica, UUID id, long seqNum) {
        this.type = type;
        this.topic = topic;
        this.message = message;
        this.replica = replica;
        this.proposalId = id;
        this.seqNum = seqNum;
    }

    public static Proposal deserialize(ByteBuf buf) throws UnknownHostException {
        UUID id = new UUID(buf.readLong(), buf.readLong());

        Proposal.ProposalType type = Proposal.ProposalType.decode(buf.readByte());

        if (type == ProposalType.NOOP) {
            return new Proposal(type, null, null, null, id, -1L);
        }

        if (type == ProposalType.ADD_REPLICA || type == ProposalType.REMOVE_REPLICA) {
            return new Proposal(type, null, null, Host.deserialize(buf), id, -1L);
        }

        byte[] topicBytes = new byte[buf.readInt()];
        buf.readBytes(topicBytes);
        String topic = new String(topicBytes);

        if (type == Proposal.ProposalType.SUBSCRIBE || type == Proposal.ProposalType.UNSUBSCRIBE) {
            return new Proposal(type, topic, null, null, id, -1L);
        }

        byte[] messageBytes = new byte[buf.readInt()];
        buf.readBytes(messageBytes);
        String message = new String(messageBytes);

        long seqNum = buf.readLong();

        return new Proposal(type, topic, message, null, id, seqNum);
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    public ProposalType getType() {
        return type;
    }

    public Host getReplica() {
        return replica;
    }

    public long getSeqNum() {
        return seqNum;
    }

    public void serialize(ByteBuf buf) {
        buf.writeLong(proposalId.getMostSignificantBits());
        buf.writeLong(proposalId.getLeastSignificantBits());

        buf.writeByte(type.getCode());

        if (type == ProposalType.NOOP) {
            return;
        }

        if (type == ProposalType.ADD_REPLICA || type == ProposalType.REMOVE_REPLICA) {
            replica.serialize(buf);
            return;
        }

        buf.writeInt(topic.getBytes().length);
        buf.writeBytes(topic.getBytes());

        if (type == Proposal.ProposalType.SUBSCRIBE || type == Proposal.ProposalType.UNSUBSCRIBE) {
            return;
        }

        buf.writeInt(message.getBytes().length);
        buf.writeBytes(message.getBytes());

        buf.writeLong(seqNum);
    }

    public int serializedSize() {
        if (size == -1) {
            size = Byte.BYTES;
            size += Long.BYTES * 2;
            if (type == ProposalType.NOOP) {
                return size;
            } else if (type == ProposalType.ADD_REPLICA || type == ProposalType.REMOVE_REPLICA) {
                size += replica.serializedSize();
            } else {
                size += Integer.BYTES + topic.getBytes().length;

                if (type == Proposal.ProposalType.PUBLISH) {
                    size += Integer.BYTES + message.getBytes().length;
                    size += Long.BYTES;
                }
            }
        }
        return size;
    }

    @Override
    public String toString() {
        return String.format("Proposal{type=%s, topic=%s, message=%s, replica=%s}", type, topic, message, replica);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (!(obj instanceof Proposal))
            return false;

        Proposal objProposal = (Proposal) obj;

        return this.proposalId.equals(objProposal.proposalId);
    }

    @Override
    public int compareTo(Proposal proposal) {
        return Long.compare(this.seqNum, proposal.seqNum);
    }

    public enum ProposalType {
        SUBSCRIBE(0),
        UNSUBSCRIBE(1),
        PUBLISH(2),
        ADD_REPLICA(3),
        REMOVE_REPLICA(4),
        NOOP(5);

        static final Proposal.ProposalType[] proposals = new Proposal.ProposalType[values().length];

        static {
            for (Proposal.ProposalType prop : values())
                proposals[prop.getCode()] = prop;
        }

        private final byte code;

        ProposalType(int code) {
            this.code = (byte) code;
        }

        public static Proposal.ProposalType decode(byte code) {
            return proposals[code];
        }

        public byte getCode() {
            return code;
        }
    }
}
