package asd.project.utils;

import java.io.*;
import java.math.BigInteger;
import java.util.*;

public class Utils {
    private static Random random = new Random();

    public static <T> List<T> getRandomSetFromSet(Set<T> set, int size) {
        List<T> copy = new LinkedList<>(set);
        Collections.shuffle(copy);

        if (copy.size() <= size)
            return copy;
        else
            return copy.subList(0, size);
    }

    public static <T> T getRandomElementFromSetExcept(Set<T> set, T element) {
        Set<T> copy = new HashSet<>(set);
        copy.remove(element);
        return getRandomElementFromSet(copy);
    }

    public static <T> T getRandomElementFromSetExceptQueue(Set<T> set, Queue<T> elements) {
        Set<T> copy = new HashSet<>(set);
        copy.removeAll(elements);
        return getRandomElementFromSet(copy);
    }

    public static <T> T getRandomElementFromSet(Set<T> set) {
        if (set.size() < 1)
            return null;
        int randIndex = random.nextInt(set.size());

        int i = 0;
        for (T host : set) {
            if (i == randIndex)
                return host;
            else
                i++;
        }

        throw new RuntimeException("ERROR FINDING RANDOM SET ELEMENT");
    }

    public static byte[] fromObject(Serializable obj) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos)) {
            out.writeObject(obj);
            out.flush();
            return bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("error converting object");
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T toObject(byte[] bytes) {
        try (ByteArrayInputStream bos = new ByteArrayInputStream(bytes);
             ObjectInput out = new ObjectInputStream(bos)) {
            return (T) out.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("error converting bytes");
        }
    }

    // toCompare in ]a, b[
    public static boolean betweenOpen(BigInteger toCompare, BigInteger a, BigInteger b) {
        if (a.compareTo(b) < 0)
            return a.compareTo(toCompare) < 0 && toCompare.compareTo(b) < 0;
        else
            return a.compareTo(toCompare) < 0 || toCompare.compareTo(b) < 0;
    }

    // toCompare in ]a, b]
    public static boolean betweenRightClosed(BigInteger toCompare, BigInteger a, BigInteger b) {
        if (a.compareTo(b) < 0)
            return a.compareTo(toCompare) < 0 && toCompare.compareTo(b) <= 0;
        else
            return a.compareTo(toCompare) < 0 || toCompare.compareTo(b) <= 0;
    }

    // toCompare in [a, b[
    public static boolean betweenLeftClosed(BigInteger toCompare, BigInteger a, BigInteger b) {
        if (a.compareTo(b) < 0)
            return a.compareTo(toCompare) <= 0 && toCompare.compareTo(b) < 0;
        else
            return a.compareTo(toCompare) <= 0 || toCompare.compareTo(b) < 0;
    }
}
