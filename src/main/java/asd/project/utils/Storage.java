package asd.project.utils;

import network.Host;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Storage {
    private final static String STORAGE = "storage";

    private static String makeFilePath(String name) {
        return STORAGE + "/" + name;
    }

    public static void writePublish(String name, Proposal proposal) {
        if (!Files.isDirectory(Paths.get(STORAGE))) {
            try {
                Files.createDirectory(Paths.get(STORAGE));
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }

        String filename = makeFilePath(name);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true))) {
            writer.append(String.format("%s %s\n", proposal.getSeqNum(), proposal.getMessage()));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void writePaxosProposal(String name, Proposal proposal) {
        if (!Files.isDirectory(Paths.get(STORAGE))) {
            try {
                Files.createDirectory(Paths.get(STORAGE));
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }

        String filename = makeFilePath(name);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true))) {
            switch (proposal.getType()) {
                case PUBLISH:
                    writer.append(String.format("%s %s %s %s\n", proposal.getType(), proposal.getSeqNum(), proposal.getTopic(), proposal.getMessage()));
                    break;
                case ADD_REPLICA:
                case REMOVE_REPLICA:
                    writer.append(String.format("%s %s\n", proposal.getType(), proposal.getReplica()));
                    break;
                default:
                    throw new RuntimeException("Unexpected proposal type (" + proposal.getType() + ")");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static byte[] readFile(String name) {
        String filename = makeFilePath(name);
        try (FileInputStream in = new FileInputStream(filename)) {
            return in.readAllBytes();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("MISSING FILE: " + filename);
        }
    }

    public static void writeFullFile(String name, byte[] fileContent) {
        String filename = makeFilePath(name);

        try (FileOutputStream out = new FileOutputStream(filename)) {
            out.write(fileContent);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static List<Message> readRange(String name, String topic, long start, long end) {
        List<Message> messages = new LinkedList<>();

        String filename = makeFilePath(name);
        try (BufferedReader in = new BufferedReader(new FileReader(filename))) {

            // Skip file start
            for (int i = 1; i < start; i += 1) {
                in.readLine();
            }

            for (long i = start; i <= end; i += 1) {
                final String[] parts = in.readLine().split(" ");
                messages.add(new Message(Message.MessageType.PUBLISH, topic, parts[1], Long.parseLong(parts[0])));
                //System.err.println("READING MSG " + parts[0] + " " + parts[1]);
            }
            return messages;

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("ERROR DURING READ RANGE");
        }
    }

    public static List<Proposal> readPaxosProposalRange(String name, long start, long end) {
        List<Proposal> proposals = new LinkedList<>();

        String filename = makeFilePath(name);
        try (BufferedReader in = new BufferedReader(new FileReader(filename))) {
            // Skip file start
            for (int i = 1; i < start; i += 1) {
                in.readLine();
            }

            for (long i = start; i <= end; i += 1) {
                final String[] parts = in.readLine().split(" ");
                Proposal.ProposalType type = Proposal.ProposalType.valueOf(parts[0]);

                switch (type) {
                    case PUBLISH:
                        proposals.add(new Proposal(type, parts[2], parts[3], Long.parseLong(parts[1])));
                        break;
                    case ADD_REPLICA:
                    case REMOVE_REPLICA:
                        String[] hostElems = parts[1].split(":");
                        try {
                            Host replica = new Host(InetAddress.getByName(hostElems[0]), Short.parseShort(hostElems[1]));
                            proposals.add(new Proposal(type, replica));
                            break;
                        } catch (UnknownHostException e) {
                            e.printStackTrace();
                            System.exit(-1);
                        }
                    default:
                        throw new RuntimeException("Unexpected proposal type (" + type + ")");
                }
            }
            return proposals;

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
            throw new RuntimeException("ERROR DURING READ RANGE");
        }
    }

    public static void clearStorage(String prefix) {
        File dir = new File(STORAGE);
        for (File f : Objects.requireNonNull(dir.listFiles())) {
            if (f.getName().startsWith(prefix)) {
                f.delete();
            }
        }
    }
}
