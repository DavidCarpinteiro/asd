package asd.project.utils;

import io.netty.buffer.ByteBuf;
import network.Host;

import java.net.UnknownHostException;

public class Message {
    private final String topic;
    private final String message;
    private final Host from;
    private MessageType type;
    private int popularity;
    private long seqNum;
    private int size;
    public Message(MessageType type, String topic, int popularity) {
        if (type != MessageType.SUBSCRIBE && type != MessageType.UNSUBSCRIBE) {
            throw new RuntimeException("Invalid arguments for Message with type " + type);
        }
        this.type = type;
        this.topic = topic;
        this.message = "";
        this.popularity = popularity;
        this.from = null;
        this.size = -1;
    }

    public Message(MessageType type, String topic, String message, long seqNum) {
        if (type != MessageType.PUBLISH) {
            throw new RuntimeException("Invalid arguments for Message with type " + type);
        }
        this.type = type;
        this.topic = topic;
        this.message = message;
        this.popularity = 0;
        this.from = null;
        this.seqNum = seqNum;
        this.size = -1;
    }

    public Message(MessageType type, Host from) {
        if (type != MessageType.GET_MEMBERSHIP) {
            throw new RuntimeException("Invalid arguments for Message with type " + type);
        }
        this.type = type;
        this.topic = "";
        this.message = "";
        this.popularity = 0;
        this.from = from;
        this.size = -1;
    }

    public static Message deserialize(ByteBuf buf) throws UnknownHostException {
        MessageType type = MessageType.decode(buf.readByte());

        if (type == MessageType.GET_MEMBERSHIP) {
            return new Message(type, Host.deserialize(buf));
        }

        byte[] topicBytes = new byte[buf.readInt()];
        buf.readBytes(topicBytes);
        String topic = new String(topicBytes);

        int popularity = buf.readInt();

        if (type == MessageType.SUBSCRIBE || type == MessageType.UNSUBSCRIBE) {
            return new Message(type, topic, popularity);
        }

        byte[] messageBytes = new byte[buf.readInt()];
        buf.readBytes(messageBytes);
        String message = new String(messageBytes);

        long seqNum = buf.readLong();

        return new Message(type, topic, message, seqNum);
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    public MessageType getType() {
        return type;
    }

    public int getPopularity() {
        return popularity;
    }

    public Host getFrom() {
        return from;
    }

    public long getSeqNum() {
        return seqNum;
    }

    public void serialize(ByteBuf buf) {
        buf.writeByte(type.getCode());

        if (type == MessageType.GET_MEMBERSHIP) {
            from.serialize(buf);
            return;
        }

        buf.writeInt(topic.getBytes().length);
        buf.writeBytes(topic.getBytes());

        buf.writeInt(popularity);

        if (type == MessageType.SUBSCRIBE || type == MessageType.UNSUBSCRIBE) {
            return;
        }

        buf.writeInt(message.getBytes().length);
        buf.writeBytes(message.getBytes());

        buf.writeLong(seqNum);
    }

    public int serializedSize() {
        if (size == -1) {
            size = Byte.BYTES;
            if (type == MessageType.GET_MEMBERSHIP) {
                size += from.serializedSize();
            } else {
                size += Integer.BYTES + topic.getBytes().length + Integer.BYTES;
                if (type == MessageType.PUBLISH) {
                    size += Integer.BYTES + message.getBytes().length;
                    size += Long.BYTES;
                }
            }
        }
        return size;
    }

    @Override
    public String toString() {
        if (type == MessageType.GET_MEMBERSHIP) {
            return String.format("Message{type=%s, from=%s}", type, from);
        } else {
            return String.format("Message{type=%s, topic=%s, message=%s, seqNum=%s}", type, topic, message, seqNum);
        }
    }

    public enum MessageType {
        SUBSCRIBE(0),
        UNSUBSCRIBE(1),
        PUBLISH(2),
        GET_MEMBERSHIP(3);

        static final MessageType[] messages = new MessageType[values().length];

        static {
            for (MessageType msg : values())
                messages[msg.getCode()] = msg;
        }

        private final byte code;

        MessageType(int code) {
            this.code = (byte) code;
        }

        public static MessageType decode(byte code) {
            return messages[code];
        }

        public byte getCode() {
            return code;
        }
    }
}
