package asd.project.client;

import asd.project.utils.CC;

import java.io.File;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class ClientAutomated {
    private static final String LOCK_FOLDER = "locks";
    private static String MY_PORT;
    private static String UNIQUE_ID;
    private static boolean IN_RING;

    public static void main(String[] args) throws Exception {
        System.setProperty("log4j.configurationFile", "log.xml");

        // separate client and test arguments
        Arguments arguments = extractArgs(args);

        // collection of parameters for test
        Parameters par = extractParameters(arguments.other_args);

        UNIQUE_ID = getPublicIP(MY_PORT);
        ClientTest c = new ClientTest(arguments.client_args);

        // delay start of test to give time to system to stabilize
        if (IN_RING)
            new Thread(() -> run(c, par)).start();
    }

    private static void run(ClientTest c, Parameters p) {
        // use provided seed for all random operations
        Random r = new Random(p.seed);

        for (int round = 0; round < p.numb_rounds; round++) {

            giveSignal();
            //sleep(r, round_sleep);
            waitForSignal();

            System.out.println(CC.YELLOW("SUB STEP"));
            executeSubStep(c, r, p.numb_ops, p.op_sleep, p.percentages[0], p.contention);

            giveSignal();
            //sleep(r, round_sleep);
            waitForSignal();

            System.out.println(CC.YELLOW("PUB STEP"));

            executePubStep(c, r, p.numb_ops, p.op_sleep, p.percentages[2], p.contention);

            giveSignal();
            //sleep(r, round_sleep);
            waitForSignal();

            System.out.println(CC.YELLOW("UNSUB STEP"));
            executeUnsubStep(c, r, p.numb_ops, p.op_sleep, p.percentages[1]);
        }

        System.out.println(CC.YELLOW("DONE!"));
    }

    // On middle round only execute publishes
    private static void executePubStep(ClientTest c, Random r, int numb_ops, int sleep, float pub_prob, int contention) {
        for (int op = 0; op < numb_ops; op++) {
            float chance = r.nextFloat();

            sleep(r, sleep);

            if (chance >= 0 && chance < pub_prob) {
                final int m = getRdBetween(r, 1000000, 9000000);
                //final int idx = getRdBetween(r, 0, c.numberOfTopics());
                final String topic = /*idx == -1 ? "topic1" : c.getTopic(idx);*/ "topic" + getRdBetween(r, 1, 10 * contention);
                c.publish(topic, "message" + m);
            }
        }
    }

    // Execute subscribes
    private static void executeSubStep(ClientTest c, Random r, int numb_ops, int sleep, float sub_prob, int contention) {
        for (int op = 0; op < numb_ops; op++) {
            float chance = r.nextFloat();

            sleep(r, sleep);

            if (chance >= 0 && chance < sub_prob) {
                final String topic = "topic" + getRdBetween(r, 1, 10 * contention);
                c.subscribe(topic);
            }
        }
    }

    private static void giveSignal() {
        File f = new File(LOCK_FOLDER + "/done" + UNIQUE_ID);
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Execute unsubscribes
    private static void executeUnsubStep(ClientTest c, Random r, int numb_ops, int sleep, float unsub_prob) {
        for (int op = 0; op < numb_ops; op++) {
            float chance = r.nextFloat();

            sleep(r, sleep);

            if (chance >= 0 && chance < unsub_prob) {
                final int idx = getRdBetween(r, 0, c.numberOfTopics());
                final String topic = idx == -1 ? "topic1" : c.getTopic(idx);
                c.unsubscribe(topic);
            }
        }
    }

    // Make thread sleep for sleep time +/- a deviation
    private static void sleep(Random r, int sleep) {
        final int dev = (int) (sleep * 0.5);
        if (sleep != 0)
            try {
                TimeUnit.MILLISECONDS.sleep(getRdBetween(r, sleep - dev, sleep + dev));
            } catch (InterruptedException e) {
                System.err.println("ERROR SLEEPING");
            }
    }

    // Wait to continue
    private static void waitForSignal() {
        try {
            File f = new File(LOCK_FOLDER + "/start");
            while (!f.isFile()) {
                TimeUnit.SECONDS.sleep(1);
                //System.out.println(CC.YELLOW("WAITING"));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static String getPublicIP(String port) {
        try (final DatagramSocket socket = new DatagramSocket()) {
            socket.connect(InetAddress.getByName("8.8.8.8"), Integer.parseInt(port));
            String ip = socket.getLocalAddress().getHostAddress();
            return ip + ":" + port;
        } catch (UnknownHostException | SocketException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not obtain a Public IP");
        }
    }

    // Separate Babbel and Tester arguments
    static Arguments extractArgs(String[] args) {
        Set<String> parameters = new HashSet<>(Arrays.asList(
                "listen_base_port",
                "listen_interface",
                "listen_address",
                "Contact",
                "PaxosContact",
                "RingReplica"
        ));

        List<String> tmp_client = new LinkedList<>();
        List<String> tmp_other = new LinkedList<>();

        for (String tmp_arg : args)
            for (String arg : tmp_arg.split(" ")) {
                if (arg.isBlank()) {
                    continue;
                }
                String val = arg.split("=")[0];
                if (parameters.contains(val)) {
                    System.out.println(CC.GREEN(arg));

                    tmp_client.add(arg);
                    if (val.equals("listen_base_port")) {
                        MY_PORT = arg.split("=")[1];
                    }
                    if (val.equals("RingReplica")) {
                        IN_RING = Boolean.parseBoolean(arg.split("=")[1]);
                    }
                } else {
                    System.out.println(CC.RED(arg));

                    tmp_other.add(arg);
                }
            }
        return new Arguments(tmp_client, tmp_other);
    }

    // Parse extra Tester arguments
    static Parameters extractParameters(String[] args) {
        // parameters need a set order [numb_rounds, numb_ops, round_sleep, op_sleep, seed, %sub, %unsub, %pub]

        try {
            int numb_rounds = Integer.parseInt(args[0]);
            int numb_ops = Integer.parseInt(args[1]);

            int op_sleep = Integer.parseInt(args[2]);
            int round_sleep = Integer.parseInt(args[3]);

            long seed = Long.parseLong(args[4]);
            seed = seed == -1 ? new Random().nextLong() : seed;

            float[] percentages = new float[3];

            for (int i = 0; i < (percentages.length); i++) {
                percentages[i] = Float.parseFloat(args[i + 5]);
            }

            int contention = Integer.parseInt(args[8]);

            return new Parameters(numb_rounds, numb_ops, round_sleep, op_sleep, seed, percentages, contention);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(CC.WHITE("ERROR PARSING ARGUMENTS").RED_B().SLOW_BLINK());
            System.exit(-1);
        }

        return null;
    }

    // Get a random number between low inclusive and high exclusive
    static int getRdBetween(Random r, int low, int high) {
        if (low - high == 0)
            return -1;
        return r.nextInt(high - low) + low;
    }

    // Helper classes to parse given arguments
    static class Arguments {
        String[] client_args;
        String[] other_args;

        Arguments(List<String> client_args, List<String> other_args) {
            this.client_args = new String[client_args.size()];
            this.client_args = client_args.toArray(this.client_args);
            this.other_args = new String[other_args.size()];
            this.other_args = other_args.toArray(this.other_args);
        }
    }

    static class Parameters {
        final int numb_rounds;
        final int numb_ops;
        final int op_sleep;
        final int round_sleep;
        final long seed;
        final float[] percentages;
        final int contention;

        Parameters(int numb_rounds, int numb_ops, int op_sleep, int round_sleep, long seed, float[] percentages, int contention) {
            this.numb_rounds = numb_rounds;
            this.numb_ops = numb_ops;
            this.op_sleep = op_sleep;
            this.round_sleep = round_sleep;
            this.seed = seed;
            this.percentages = percentages;
            this.contention = contention;
        }
    }

}
