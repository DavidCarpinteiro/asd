package asd.project.client;

import asd.project.protocols.pubsub.notifications.PubSubDeliver;
import babel.notification.ProtocolNotification;

import java.util.LinkedList;
import java.util.List;

public class ClientTest extends Client {
    private final List<String> topics;

    ClientTest(String[] args) throws Exception {
        super(args);
        topics = new LinkedList<>();
    }

    public void subscribe(String topic) {
        String print = String.format("s %d %s", System.currentTimeMillis(), topic);
        System.out.println(print);
        topics.add(topic);
        super.subscribe(topic);
    }

    public void unsubscribe(String topic) {
        String print = String.format("u %d %s", System.currentTimeMillis(), topic);
        System.out.println(print);
        topics.remove(topic);
        super.unsubscribe(topic);
    }

    public void deliverNotification(ProtocolNotification protocolNotification) {
        PubSubDeliver psDeliver = (PubSubDeliver) protocolNotification;
        String print = String.format("r %d %s %s %s", System.currentTimeMillis(), psDeliver.getTopic(), psDeliver.getMessage(), psDeliver.getSeqNum());
        System.out.println(print);
        super.deliverNotification(protocolNotification);
    }

    public void publish(String topic, String message) {
        String print = String.format("p %d %s %s", System.currentTimeMillis(), topic, message);
        System.out.println(print);
        super.publish(topic, message);
    }

    public String getTopic(int idx) {
        if (idx < 0 || idx >= topics.size())
            throw new RuntimeException("Index out of bounds idx: " + idx);
        return topics.get(idx);
    }

    public int numberOfTopics() {
        return topics.size();
    }
}
