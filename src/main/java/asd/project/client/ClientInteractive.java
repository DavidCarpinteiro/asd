package asd.project.client;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class ClientInteractive {

    private static final String SUBSCRIBE = "SUB", PUBLISH = "PUB", UNSUBSCRIBE = "UNSUB";
    private static final String HELP = "H", EXIT = "E";
    private static final String ERROR_SUBSCRIBE = "Usage: sub \"topic\"",
            ERROR_PUBLISH = "Usage: pub \"topic\" \"msg\"",
            ERROR_UNSUBSCRIBE = "Usage: unsub \"topic\"";

    private static final String SUCCESS_SUBSCRIBE = "Client subscribed to topic %s",
            SUCCESS_PUBLISH = "Client published to the topic %s the message %s",
            SUCCESS_UNSUBSCRIBE = "Client unsubscribed from topic %s";

    private static final String COMMANDS = "Commands:\nSubscribe    -> sub\nUnsubscribe  -> unsub\nPublish      -> pub";

    public static void main(String[] args) throws Exception {
        setLogging(true);

        Client c = new ClientTest(args);

        TimeUnit.SECONDS.sleep(2);

        print("Client online");

        start(c);

        print("Client offline");

        System.exit(-1);
    }

    private static void print(String msg) {
        System.out.println(msg);
    }

    private static void start(Client c) {
        try (Scanner scanner = new Scanner(System.in)) {
            String cmd;

            do {
                cmd = scanner.nextLine();
                String[] parts = cmd.split(" ");

                if (parts.length == 0)
                    continue;

                switch (parts[0].toUpperCase()) {
                    case SUBSCRIBE:
                        if (isCorrect(parts, 2, ERROR_SUBSCRIBE))
                            sub(c, parts[1]);
                        break;
                    case UNSUBSCRIBE:
                        if (isCorrect(parts, 2, ERROR_UNSUBSCRIBE))
                            unsub(c, parts[1]);
                        break;
                    case PUBLISH:
                        if (isCorrect(parts, 3, ERROR_PUBLISH))
                            pub(c, parts[1], parts[2]);
                        break;
                    case EXIT:
                        print("Shutting down");
                        return;
                    case HELP:
                    default:
                        print(COMMANDS);
                }
            } while (!cmd.toUpperCase().equals(EXIT));
        }
    }

    private static void setLogging(boolean on) {
        if (on) {
            System.setProperty("log4j.configurationFile", "log.xml");
            print("Logs are enabled");
        } else {
            System.setProperty("log4j.configurationFile", "log_disable.xml");
            print("Logs are disabled");
        }
    }

    private static boolean isCorrect(String[] cmd, int size, String error) {
        if (cmd.length != size) {
            print(error);
            return false;
        }
        return true;
    }

    private static void sub(Client c, String topic) {
        c.subscribe(topic);
        print(String.format(SUCCESS_SUBSCRIBE, topic));
    }

    private static void unsub(Client c, String topic) {
        c.unsubscribe(topic);
        print(String.format(SUCCESS_UNSUBSCRIBE, topic));
    }

    private static void pub(Client c, String topic, String msg) {
        c.publish(topic, msg);
        print(String.format(SUCCESS_PUBLISH, topic, msg));
    }

}
