package asd.project.client.distributed;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class IPChecker {

    static String getIp(String port) {
        try (final DatagramSocket socket = new DatagramSocket()) {
            socket.connect(InetAddress.getByName("8.8.8.8"), Integer.parseInt(port));
            String ip = socket.getLocalAddress().getHostAddress();
            return ip + ":" + port;
        } catch (UnknownHostException | SocketException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not obtain a Public IP");
        }
    }

}
