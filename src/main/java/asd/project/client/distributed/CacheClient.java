package asd.project.client.distributed;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.time.Duration;

public class CacheClient {

    private static CacheClient client = null;
    private final JedisPool pool;
    private CacheClient() {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(256);
        poolConfig.setMaxIdle(256);
        poolConfig.setMinIdle(16);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
        poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
        poolConfig.setNumTestsPerEvictionRun(3);
        poolConfig.setBlockWhenExhausted(true);
        String hostname = "asd1920.redis.cache.windows.net";
        String key = "ATs4j3QlaMTgK0cQwiQxAKiNE6JcofwpNW1rHn5m5Xs=";
        pool = new JedisPool(poolConfig, hostname, 6380, 1000, key, true);
    }

    public static Jedis getJedis() {
        try (Jedis jedis = CacheClient.getCache().getJedisPool().getResource()) {
            return jedis;
        } catch (Exception e) {
            throw new RuntimeException("Unable to retrieve jedis");
        }
    }

    private static synchronized CacheClient getCache() {
        if (client == null) {
            client = new CacheClient();
        }
        return client;
    }

    private JedisPool getJedisPool() {
        return pool;
    }

    public enum KEY {
        INFO_IP,
        READY,
        START,
        WAITING,
        INIT,
        COMPLETED
    }
}