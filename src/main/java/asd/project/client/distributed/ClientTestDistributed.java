package asd.project.client.distributed;

import asd.project.client.Client;
import asd.project.protocols.pubsub.notifications.PubSubDeliver;
import babel.notification.ProtocolNotification;

import java.util.LinkedList;
import java.util.List;

public class ClientTestDistributed extends Client {
    private final List<String> topics;
    private final String id;

    ClientTestDistributed(String[] args, String id) throws Exception {
        super(args);
        this.id = id;
        topics = new LinkedList<>();
    }

    public void subscribe(String topic) {
        String msg = String.format("s %d %s\n", System.currentTimeMillis(), topic);
        System.out.print(msg);
        CacheClient.getJedis().append(id, msg);
        topics.add(topic);
        super.subscribe(topic);
    }

    public void unsubscribe(String topic) {
        String msg = String.format("u %d %s\n", System.currentTimeMillis(), topic);
        System.out.print(msg);
        CacheClient.getJedis().append(id, msg);
        topics.remove(topic);
        super.unsubscribe(topic);
    }

    public void deliverNotification(ProtocolNotification protocolNotification) {
        PubSubDeliver psDeliver = (PubSubDeliver) protocolNotification;
        String msg = String.format("r %d %s %s %s\n", System.currentTimeMillis(), psDeliver.getTopic(), psDeliver.getMessage(), psDeliver.getSeqNum());
        System.out.print(msg);
        CacheClient.getJedis().append(id, msg);
        super.deliverNotification(protocolNotification);
    }

    public void publish(String topic, String message) {
        String msg = String.format("p %d %s %s\n", System.currentTimeMillis(), topic, message);
        System.out.print(msg);
        CacheClient.getJedis().append(id, msg);
        super.publish(topic, message);
    }

    public String getTopic(int idx) {
        if (idx < 0 || idx >= topics.size())
            throw new RuntimeException("Index out of bounds idx: " + idx);
        return topics.get(idx);
    }

    public int numberOfTopics() {
        return topics.size();
    }
}
