package asd.project.client.distributed;

import asd.project.utils.CC;

import java.util.*;

public class ClientAutomatedDistributed {
    public final ClientTestDistributed c;
    private final FlowControl.Node node;
    private final Parameters par;
    private final Arguments arguments;
    private String myPort;
    private String uniqueId;
    private boolean inRing;

    public ClientAutomatedDistributed() throws Exception {
        node = FlowControl.init().getNode();

        System.out.println(CC.GREEN("STARTED"));

        node.setInitComplete();
        String args_pre = node.waitForArgs();

        System.out.println(CC.GREEN("INIT : ") + args_pre);

        // separate client and test arguments
        arguments = extractArgs(args_pre);

        // collection of parameters for test
        par = extractParameters(arguments.other_args);

        uniqueId = IPChecker.getIp(myPort);

        System.out.println(CC.GREEN("\t" + uniqueId + "\t").WHITE_B());

        c = new ClientTestDistributed(arguments.client_args, uniqueId);
    }

    public static void main(String[] args) throws Exception {
        System.setProperty("log4j.configurationFile", "log.xml");

        ClientAutomatedDistributed client = new ClientAutomatedDistributed();

        client.launchTest();
    }

    private void launchTest() {
        node.addMyIP(uniqueId);

        if (inRing) {
            new Thread(() -> run(c, par)).start();
        }
    }

    private void run(ClientTestDistributed c, Parameters p) {
        // use provided seed for all random operations
        Random r = new Random(p.seed);

        for (int round = 0; round < p.numb_rounds; round++) {
            node.giveSignalAndWait();

            System.out.println(CC.YELLOW("SUB STEP"));
            executeSubStep(c, r, p.numb_ops, p.op_sleep, p.percentages[0], p.contention);

            node.giveSignalAndWait();

            System.out.println(CC.YELLOW("PUB STEP"));

            executePubStep(c, r, p.numb_ops, p.op_sleep, p.percentages[2], p.contention);

            node.giveSignalAndWait();

            System.out.println(CC.YELLOW("UNSUB STEP"));
            executeUnsubStep(c, r, p.numb_ops, p.op_sleep, p.percentages[1]);
        }

        FlowControl.sleep(15);

        node.runCompleted(uniqueId);
        node.exportResults(uniqueId);

        System.out.println(CC.YELLOW("DONE!"));
    }

    // On middle round only execute publishes
    private void executePubStep(ClientTestDistributed c, Random r, int numb_ops, int sleep, float pub_prob, int contention) {
        for (int op = 0; op < numb_ops; op++) {
            float chance = r.nextFloat();

            FlowControl.sleep(r, sleep);

            if (chance >= 0 && chance < pub_prob) {
                final int m = getRdBetween(r, 1000000, 9000000);
                //final int idx = getRdBetween(r, 0, c.numberOfTopics());
                final String topic = /*idx == -1 ? "topic1" : c.getTopic(idx);*/ "topic" + getRdBetween(r, 1, 10 * contention);
                c.publish(topic, "message" + m);
            }
        }
    }

    // Execute subscribes
    private void executeSubStep(ClientTestDistributed c, Random r, int numb_ops, int sleep, float sub_prob, int contention) {
        for (int op = 0; op < numb_ops; op++) {
            float chance = r.nextFloat();

            FlowControl.sleep(r, sleep);

            if (chance >= 0 && chance < sub_prob) {
                final String topic = "topic" + getRdBetween(r, 1, 10 * contention);
                c.subscribe(topic);
            }
        }
    }

    // Execute unsubscribes
    private void executeUnsubStep(ClientTestDistributed c, Random r, int numb_ops, int sleep, float unsub_prob) {
        for (int op = 0; op < numb_ops; op++) {
            float chance = r.nextFloat();

            FlowControl.sleep(r, sleep);

            if (chance >= 0 && chance < unsub_prob) {
                final int idx = getRdBetween(r, 0, c.numberOfTopics());
                final String topic = idx == -1 ? "topic1" : c.getTopic(idx);
                c.unsubscribe(topic);
            }
        }
    }

    // Separate Babbel and Tester arguments
    Arguments extractArgs(String args) {
        Set<String> parameters = new HashSet<>(Arrays.asList(
                "listen_base_port",
                "listen_interface",
                "listen_address",
                "Contact",
                "PaxosContact",
                "RingReplica"
        ));

        List<String> tmp_client = new LinkedList<>();
        List<String> tmp_other = new LinkedList<>();

        for (String arg : args.split(" ")) {
            if (arg.isBlank()) {
                continue;
            }
            String val = arg.split("=")[0];
            if (parameters.contains(val)) {
                //System.out.println(CC.GREEN(arg));

                tmp_client.add(arg);
                if (val.equals("listen_base_port")) {
                    myPort = arg.split("=")[1];
                }
                if (val.equals("RingReplica")) {
                    inRing = Boolean.parseBoolean(arg.split("=")[1]);
                }
            } else {
                //System.out.println(CC.RED(arg));

                tmp_other.add(arg);
            }
        }
        return new Arguments(tmp_client, tmp_other);
    }

    // Parse extra Tester arguments
    Parameters extractParameters(String[] args) {
        // parameters need a set order [numb_rounds, numb_ops, round_sleep, op_sleep, seed, %sub, %unsub, %pub]

        try {
            int numb_rounds = Integer.parseInt(args[0]);
            int numb_ops = Integer.parseInt(args[1]);

            int op_sleep = Integer.parseInt(args[2]);
            int round_sleep = Integer.parseInt(args[3]);

            long seed = Long.parseLong(args[4]);
            seed = seed == -1 ? new Random().nextLong() : seed;

            float[] percentages = new float[3];

            for (int i = 0; i < (percentages.length); i++) {
                percentages[i] = Float.parseFloat(args[i + 5]);
            }

            int contention = Integer.parseInt(args[8]);

            return new Parameters(numb_rounds, numb_ops, round_sleep, op_sleep, seed, percentages, contention);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(CC.WHITE("ERROR PARSING ARGUMENTS").RED_B().SLOW_BLINK());
            System.exit(-1);
        }

        return null;
    }

    // Get a random number between low inclusive and high exclusive
    int getRdBetween(Random r, int low, int high) {
        if (low - high == 0)
            return -1;
        return r.nextInt(high - low) + low;
    }

    // Helper classes to parse given arguments
    class Arguments {
        String[] client_args;
        String[] other_args;

        Arguments(List<String> client_args, List<String> other_args) {
            this.client_args = new String[client_args.size()];
            this.client_args = client_args.toArray(this.client_args);
            this.other_args = new String[other_args.size()];
            this.other_args = other_args.toArray(this.other_args);
        }
    }

    class Parameters {
        final int numb_rounds;
        final int numb_ops;
        final int op_sleep;
        final int round_sleep;
        final long seed;
        final float[] percentages;
        final int contention;

        Parameters(int numb_rounds, int numb_ops, int op_sleep, int round_sleep, long seed, float[] percentages, int contention) {
            this.numb_rounds = numb_rounds;
            this.numb_ops = numb_ops;
            this.op_sleep = op_sleep;
            this.round_sleep = round_sleep;
            this.seed = seed;
            this.percentages = percentages;
            this.contention = contention;
        }
    }
}
