package asd.project.client.distributed;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class FlowControl {
    private static FlowControl control = null;

    static void sleep(Random r, int sleep) {
        final int dev = (int) (sleep * 0.5);
        final int time = getRdBetween(r, sleep - dev, sleep + dev);
        if (sleep != 0)
            sleep(time);
    }

    static void sleep(int sleep) {
        try {
            TimeUnit.MILLISECONDS.sleep(sleep);
        } catch (InterruptedException e) {
            System.err.println("ERROR SLEEPING");
        }
    }

    // Get a random number between low inclusive and high exclusive
    static int getRdBetween(Random r, int low, int high) {
        if (low - high == 0)
            return -1;
        return r.nextInt(high - low) + low;
    }

    public static FlowControl init() {
        if (control == null) {
            control = new FlowControl();
        }
        return control;
    }

    public Node getNode() {
        return new Node();
    }

    public Controler getController() {
        return new Controler();
    }

    class Node {
        private Node() {
        }

        void setInitComplete() {
            CacheClient.getJedis().incr(CacheClient.KEY.WAITING.name());
        }

        String waitForArgs() {
            String args = null;
            Random r = new Random();
            while (args == null) {
                args = CacheClient.getJedis().lpop(CacheClient.KEY.INIT.name());
                sleep(r, 2);
            }
            return args;
        }

        void addMyIP(String ip) {
            CacheClient.getJedis().rpush(CacheClient.KEY.INFO_IP.name(), ip);
        }

        void giveSignalAndWait() {
            CacheClient.getJedis().incr(CacheClient.KEY.READY.name());
            boolean start;
            do {
                start = CacheClient.getJedis().exists(CacheClient.KEY.START.name());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (!start);
        }

        void runCompleted(String ip) {
            CacheClient.getJedis().lpush(CacheClient.KEY.COMPLETED.name(), ip);
        }

        void exportResults(String ip) {
            File f = new File("output.log");
            BufferedReader bf = null;
            try {
                bf = new BufferedReader(new FileReader(f));
                List<String> lines = new ArrayList<>(1000);

                while (bf.ready()) {
                    lines.add(bf.readLine());
                }
                CacheClient.getJedis().set(ip + "_results", String.join("\n", lines));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class Controler {
        private Controler() {
        }

        void cleanState() {
            CacheClient.getJedis().flushAll();
            CacheClient.getJedis().set(CacheClient.KEY.WAITING.toString(), "0");
            CacheClient.getJedis().set(CacheClient.KEY.READY.toString(), "0");
            CacheClient.getJedis().del(CacheClient.KEY.START.toString());
        }

        void waitForAllInit(int count) {
            boolean all_init = false;
            while (!all_init) {
                int ready = Integer.parseInt(CacheClient.getJedis().get(CacheClient.KEY.WAITING.toString()));
                all_init = ready == count;
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        void waitAndUnlock(int nodeCount) throws InterruptedException {
            boolean all_done = false;
            while (!all_done) {
                int ready = Integer.parseInt(CacheClient.getJedis().get(CacheClient.KEY.READY.name()));
                all_done = ready == nodeCount;
                TimeUnit.SECONDS.sleep(1);
            }
            CacheClient.getJedis().set(CacheClient.KEY.READY.name(), "0");
            CacheClient.getJedis().set(CacheClient.KEY.START.name(), "go");
            TimeUnit.SECONDS.sleep(5);
            CacheClient.getJedis().del(CacheClient.KEY.START.name());
        }

        String getRunningNodeIP() {
            return CacheClient.getJedis().blpop(999999999, CacheClient.KEY.INFO_IP.name()).get(1);
        }

        void launchNode(String args) {
            CacheClient.getJedis().lpush(CacheClient.KEY.INIT.name(), args);
        }

        List<String> getCompletedRunNodeIpAndResults() {
            String ip = CacheClient.getJedis().blpop(999999999, CacheClient.KEY.COMPLETED.name()).get(1);
            String results = CacheClient.getJedis().get(ip);
            return List.of(ip, results);
        }
    }

}