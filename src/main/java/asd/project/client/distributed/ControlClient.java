package asd.project.client.distributed;

import asd.project.utils.CC;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ControlClient {
    private static int nodeCount = 2;
    private static int replicaCount = 1;
    private static int initial_port = 7000;
    private static int rounds = 1;
    private static int roundSleepSec = 20;


    private static boolean first = true;
    private static List<String> ringNodesIPs = new LinkedList<>();
    private static List<String> outsideNodesIPs = new LinkedList<>();
    private static Random r = new Random();

    private static int totalNodes() {
        return nodeCount * replicaCount;
    }

    private static boolean checkInsideRing(int i, int ring_rnd, StringBuilder arguments) {
        boolean inside = false;
        if (i == ring_rnd) {
            inside = true;
            arguments.append(" ");
            if (first) {
                first = false;
            } else {
                // select random node from ring
                String url = ringNodesIPs.get(FlowControl.getRdBetween(r, 0, ringNodesIPs.size()));
                arguments.append(String.format("Contact=%s", url));
                arguments.append(" ");
            }
            arguments.append("RingReplica=true");
        } else {
            arguments.append(" ");
            arguments.append("RingReplica=false");
        }
        return inside;
    }

    private static void clearLogs() {
        File dir = new File("logs");
        for (File f : Objects.requireNonNull(dir.listFiles())) {
            if (!f.getName().contains(".gitkeep")) {
                f.delete();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        clearLogs();

        FlowControl.Controler cont = FlowControl.init().getController();

        cont.cleanState();

        System.out.println(CC.GREEN("READY"));

        cont.waitForAllInit(totalNodes());

        System.out.println(CC.YELLOW("ALL ONLINE"));

        StringBuilder arguments;
        int port = initial_port;

        System.out.println(CC.GREEN("««««««««««««»»»»»»»»»»»»"));
        for (int i = 0; i < nodeCount; i++) {
            int ring_rnd = new Random().nextInt(replicaCount);
            System.out.println(CC.PURPLE(String.format("RING CHANCE IS <%d>", ring_rnd)));
            arguments = new StringBuilder();

            // launch leader
            boolean inside = checkInsideRing(0, ring_rnd, arguments);

            cont.launchNode(String.format("listen_base_port=%d ", port++) + arguments.toString() + " 1 20 10 1000 -1 0.3 0.1 0.6 2");
            // wait for node to be online and get it's ip

            String leaderIP = cont.getRunningNodeIP();

            if (inside) {
                System.out.println(CC.PURPLE(String.format("LEADER <%d> INSIDE [%s]", 0, leaderIP)));
                ringNodesIPs.add(leaderIP);
            } else {
                System.out.println(CC.PURPLE(String.format("LEADER <%d> OUTSIDE [%s]", 0, leaderIP)));
                outsideNodesIPs.add(leaderIP);
            }

            TimeUnit.SECONDS.sleep(2);

            for (int j = 1; j < replicaCount; j++) {
                arguments = new StringBuilder();
                inside = checkInsideRing(j, ring_rnd, arguments);

                arguments.append(" ");
                arguments.append(String.format("PaxosContact=%s", leaderIP));

                cont.launchNode(String.format("listen_base_port=%d ", port++) + arguments.toString() + " 1 20 10 1000 -1 0.3 0.1 0.6 2");
                // wait for node to be online and get it's ip

                String replicaIP = cont.getRunningNodeIP();

                if (inside) {
                    System.out.println(CC.PURPLE(String.format("REPLICA <%d> INSIDE [%s] -> [%s]", j, replicaIP, leaderIP)));
                    ringNodesIPs.add(replicaIP);
                } else {
                    System.out.println(CC.PURPLE(String.format("REPLICA <%d> OUTSIDE [%s] -> [%s]", j, replicaIP, leaderIP)));
                    outsideNodesIPs.add(replicaIP);
                }
            }

            System.out.println(CC.GREEN("««««««««««««»»»»»»»»»»»»"));
            TimeUnit.SECONDS.sleep(15);
        }

        for (int round = 0; round < rounds; round++) {
            System.out.println(CC.CYAN(String.format("ROUND (%d)", round)));
            for (int i = 0; i < 3; i++) {
                TimeUnit.SECONDS.sleep(roundSleepSec);

                cont.waitAndUnlock(nodeCount);

                System.out.println(CC.CYAN(String.format("\tSTARTING PHASE (%d)", i)));
            }
        }
        System.out.println(CC.CYAN(String.format("DONE (%d) ROUNDS", rounds)));

        for (int done = 0; done < ringNodesIPs.size(); done++) {
            List<String> ip_results = cont.getCompletedRunNodeIpAndResults();
            System.out.println(CC.YELLOW(String.format("GETTING RESULTS FROM NODE <%s>: ", ip_results.get(0))));

            String results = ip_results.get(1);

            System.out.println("\tRESULTS: " + results);

            String cleanedIP = ip_results.get(0).replace(".", "_").replace(":", "_");
            BufferedWriter writer = new BufferedWriter(new FileWriter("logs/" + cleanedIP + ".log"));
            writer.write(results);
            writer.close();
        }

        //TODO get all log (protocol messages)
        /*
            CacheClient().get(ip + "_results")
                    do {
                results = CacheClient.getJedis().get(ip);
                if (results == null) {
                    System.out.println(CC.YELLOW("\tNO RESULTS -> WAITING"));
                    TimeUnit.SECONDS.sleep(1);
                }
            } while (results == null);
         */

        CacheClient.getJedis().flushAll();
    }
}
