package asd.project.client;

import asd.project.protocols.bcast.BCast;
import asd.project.protocols.dht.Chord;
import asd.project.protocols.disseminate.Disseminate;
import asd.project.protocols.hyparview.HyParView;
import asd.project.protocols.paxos.Paxos;
import asd.project.protocols.pubsub.AbstractPubSub;
import asd.project.protocols.pubsub.PubSub;
import asd.project.protocols.pubsub.PubSubReplica;
import asd.project.protocols.pubsub.notifications.PubSubDeliver;
import asd.project.protocols.pubsub.requests.PublishRequest;
import asd.project.protocols.pubsub.requests.SubscribeRequest;
import asd.project.protocols.pubsub.requests.UnsubscribeRequest;
import babel.Babel;
import babel.notification.INotificationConsumer;
import babel.notification.ProtocolNotification;
import network.INetwork;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

public class Client implements INotificationConsumer {
    private static final Logger logger = LogManager.getLogger(Client.class);
    private final AbstractPubSub pubSub;

    public Client(String[] args) throws Exception {

        // Setup babel
        Babel babel = Babel.getInstance();
        Properties configProps = babel.loadConfig("network_config.properties", args);
        INetwork net = babel.getNetworkInstance();

        // Define protocols
        HyParView hyparview = new HyParView(net);
        BCast bCast = new BCast(net);

        Chord chord = new Chord(net);
        Disseminate disseminate = new Disseminate(net);

        boolean isRingReplica = Boolean.parseBoolean(configProps.getProperty("RingReplica"));
        if (isRingReplica)
            pubSub = new PubSub(net);
        else {
            pubSub = new PubSubReplica(net);
        }

        Paxos paxos = new Paxos(net);

        // Init protocols
        if (isRingReplica) {
            hyparview.init(configProps);
            babel.registerProtocol(hyparview);

            bCast.init(configProps);
            babel.registerProtocol(bCast);

            chord.init(configProps);
            babel.registerProtocol(chord);

            disseminate.init(configProps);
            babel.registerProtocol(disseminate);
        }

        paxos.init(configProps);
        babel.registerProtocol(paxos);

        pubSub.init(configProps);
        babel.registerProtocol(pubSub);

        // Register protocols

        // Subscribe to notifications

        // bCast.subscribeNotification(BCastDeliver.NOTIFICATION_ID, pubSub);
        pubSub.subscribeNotification(PubSubDeliver.NOTIFICATION_NAME, this);

        // Start babel runtime
        babel.start();

    }

    public void subscribe(String topic) {
        SubscribeRequest subscribeRequest = new SubscribeRequest(topic);
        pubSub.deliverRequest(subscribeRequest);
    }

    public void unsubscribe(String topic) {
        UnsubscribeRequest unsubscribeRequest = new UnsubscribeRequest(topic);
        pubSub.deliverRequest(unsubscribeRequest);
    }

    public void publish(String topic, String msg) {
        PublishRequest publishRequest = new PublishRequest(topic, msg);
        pubSub.deliverRequest(publishRequest);
    }

    @Override
    public void deliverNotification(ProtocolNotification protocolNotification) {
        PubSubDeliver psDeliver = (PubSubDeliver) protocolNotification;
        //logger.debug("[protocol message]");
        logger.info(String.format("GOT -> TOPIC(%s) MSG(%s)", psDeliver.getTopic(), psDeliver.getMessage()));
    }
}