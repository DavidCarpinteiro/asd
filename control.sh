#!/bin/bash

rm -f output.log
find ./logs -type f -not -name '.gitkeep' -delete
find ./storage -type f -not -name '.gitkeep' -delete

mvn compile
mvn exec:java -Dexec.mainClass=asd.project.client.distributed.ControlClient
