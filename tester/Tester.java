import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Tester {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";

    public static void main(String[] args) throws IOException {
        boolean ok = true;

        Map<String, List<String>> logs = readLogs("../logs/");
        List<ClientStatus> clients = new LinkedList<>();
        logs.forEach((k, v) -> clients.add(new ClientStatus(k, v)));

        Map<String, SortedSet<MessagePubSub>> global_publications = new HashMap<>(1000);
        int global_total = 0;
        long global_avg_time = 0;
        int total_pubs = 0, total_subs = 0, total_unsubs = 0;

        List<Double> success_rate = new LinkedList<>();

        for (ClientStatus c : clients) {
            total_subs += c.getSubscriptions().size();
            total_unsubs += c.getUnsubscriptions().size();
            total_pubs += c.getPublications().size();
            for (MessagePubSub m : c.getPublications()) {
                SortedSet<MessagePubSub> msgs = global_publications.getOrDefault(m.getTopic(), new TreeSet<>());
                msgs.add(m);
                global_total++;
                global_publications.putIfAbsent(m.getTopic(), msgs);
            }
        }

        for (ClientStatus c : clients) {
            int total_messages = 0;
            int messages_received = 0;
            long max_time = 0, min_time = Long.MAX_VALUE;
            double avg_time = 0;
            int count_msg = 0;

            System.out.println("\n\nName: " + c.getName());
            for (Map.Entry<String, List<Long[]>> entry : c.getSubs_time_intervals().entrySet()) {
                System.out.print(entry.getKey() + ": ");
                for (Long[] l : entry.getValue()) {
                    System.out.printf("[%d %d], ", l[0], l[1]);
                }
                System.out.println();
            }
            for (Map.Entry<String, SortedSet<MessagePubSub>> e : global_publications.entrySet()) {
                String topic = e.getKey();
                for (MessagePubSub message : e.getValue()) {
                    long publish_time = message.getTime();
                    List<Long[]> topic_interval = c.getSubs_time_intervals().getOrDefault(topic, new LinkedList<>());
                    Long[] sub_interval = null;
                    for (Long[] interval : topic_interval) {

                        if (publish_time >= interval[0] && publish_time <= interval[1]) {
                            sub_interval = interval;
                            total_messages++;
                            break;
                        } else if (publish_time > (interval[0] * 1.2f) && publish_time <= (interval[1] * 1.2f)) {
                            System.out.printf(ANSI_RED + "HHHHHHHHHHHHH\n" + ANSI_RESET);
                        }
                    }
                    MessagePubSub message_found = null;
                    if (sub_interval != null) {
                        SortedSet<MessagePubSub> messages = c.getReceptions().getOrDefault(topic, new TreeSet<>());
                        for (MessagePubSub m : messages) {
                            if (message.getMessage().equals(m.getMessage())) {
                                if (m.getTime() >= sub_interval[0] && m.getTime() <= sub_interval[1]) {
                                    long time_diff = m.getTime() - message.getTime();
                                    //System.out.println("TTTTTTTTTTTTTTT::: " + time_diff);
                                    if (time_diff > 0) {
                                        max_time = time_diff > max_time ? time_diff : max_time;
                                        min_time = time_diff < min_time ? time_diff : min_time;
                                        avg_time += time_diff;
                                        count_msg++;
                                    }
                                    message_found = m;
                                    break;
                                }
                            }
                        }
                    }
                    if (message_found != null) {
                        messages_received++;
                        //System.out.printf("Found msg %s %s, count=%d\n", message_found.topic, message_found.message, messages_received);
                    } else if (sub_interval != null) {
                        System.out.printf(ANSI_RED + "Not found %s %s %d\n" + ANSI_RESET, message.getTopic(), message.getMessage(), message.getTime());
                    }
                }
            }
            if (messages_received != total_messages)
                ok = false;

            System.out.printf("Found %d of %d total\n", messages_received, total_messages);
            if (count_msg > 0) {
                avg_time = messages_received > 0 ? (avg_time / (double) count_msg) : 0;
                global_avg_time += avg_time;
            }
            System.out.printf("Times: avg: %.2f, max: %d, min: %d\n", avg_time, max_time, min_time);
            if (total_messages > 0)
                success_rate.add(messages_received / (double) total_messages);
        }
        double sum_of_rates = 0;
        for (int i = 0; i < success_rate.size(); i++) {
            sum_of_rates += success_rate.get(i);
        }

        System.out.print("\nALL RATES : [");
        for (double rate : success_rate) {
            if (rate != 1) {
                System.out.printf(ANSI_RED + "%.4f;" + ANSI_RESET, rate);
            } else {
                System.out.printf("%.1f;", rate);
            }

        }
        System.out.println("]\n");
        System.out.printf("AVERAGE RECEPTION RATE = %.3f %%\n", sum_of_rates * 100 / (double) success_rate.size());
        System.out.printf("AVERAGE RECEPTION TIME = %.2f ms\n", global_avg_time / (double) clients.size());
        System.out.printf("SUBS: %d, UNSUBS: %d, PUBS: %d\n", total_subs, total_unsubs, total_pubs);

        System.out.printf("\nRESULT: %s\n", ok ? ANSI_GREEN + "ALL GOOD" + ANSI_RESET : "SOME HAVE FAILED");
    }


    private static Map<String, List<String>> readLogs(String path) {
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        Map<String, List<String>> client_logs = new HashMap<>(50);

        try {
            for (File listOfFile : Objects.requireNonNull(listOfFiles)) {
                if (listOfFile.isFile()) {
                    BufferedReader bf = new BufferedReader(new FileReader(listOfFile));

                    List<String> lines = new ArrayList<>(1000);

                    while (bf.ready()) {
                        lines.add(bf.readLine());
                    }

                    if (listOfFile.getName().contains(".log"))
                        if (listOfFile.getName().contains("ring")) {
                            int start = new String("ring").length();
                            int end = listOfFile.getName().indexOf(".log");
                            client_logs.put(listOfFile.getName().substring(start, end), lines);
                        }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        return client_logs;
    }
}



