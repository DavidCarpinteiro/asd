import java.util.stream.Collectors;

class ClientStatus {
    private final SortedSet<MessagePubSub> subscriptions;
    private final SortedSet<MessagePubSub> unsubscriptions;
    private final SortedSet<MessagePubSub> publications;
    private final Map<String, SortedSet<MessagePubSub>> receptions;
    private final Map<String, List<Long[]>> subs_time_intervals;
    private final String name;

    ClientStatus(String name, List<String> logs) {
        this.name = name;
        subscriptions = new TreeSet<>();
        unsubscriptions = new TreeSet<>();
        publications = new TreeSet<>();
        receptions = new HashMap<>(1000);
        subs_time_intervals = new HashMap<>(1000);

        readLogs(logs);
    }

    private void readLogs(List<String> logs) {
        Map<String, Long> start_times = new HashMap<>();

        List<String[]> parts = logs.stream()
                .filter(s -> {
                    int l = s.split(" ").length;
                    return l >= 3 && l <= 5;
                })
                .map(s -> s.split(" "))
                .collect(Collectors.toList());

        parts.forEach(
                p -> {
                    String event = p[0];
                    long time = p[1].matches("\\d+") ? Long.parseLong(p[1]) : -1;
                    String topic = p[2];
                    String message = (p.length >= 4 && p.length <= 5) ? p[3] : null;

                    switch (event) {
                        case "s":
                            subscriptions.add(new MessagePubSub(time, topic));
                            start_times.putIfAbsent(topic, time);
                            break;
                        case "u":
                            unsubscriptions.add(new MessagePubSub(time, topic));
                            long time_start = start_times.getOrDefault(topic, -1L);

                            if (time_start != -1) {
                                List<Long[]> inter = subs_time_intervals.getOrDefault(topic, new LinkedList<>());
                                inter.add(new Long[]{time_start, time});
                                start_times.remove(topic);
                                subs_time_intervals.putIfAbsent(topic, inter);
                            }
                            break;
                        case "p":
                            publications.add(new MessagePubSub(time, topic, message));
                            break;
                        case "r":
                            SortedSet<MessagePubSub> msg = receptions.getOrDefault(topic, new TreeSet<>());
                            msg.add(new MessagePubSub(time, topic, message));
                            receptions.putIfAbsent(topic, msg);
                            break;
                    }
                }
        );

        for (Map.Entry<String, Long> e : start_times.entrySet()) {
            List<Long[]> inter = subs_time_intervals.getOrDefault(e.getKey(), new LinkedList<>());
            inter.add(new Long[]{e.getValue(), Long.MAX_VALUE});
            subs_time_intervals.putIfAbsent(e.getKey(), inter);
        }
    }


    public Map<String, List<Long[]>> getSubs_time_intervals() {
        return subs_time_intervals;
    }

    public Set<MessagePubSub> getSubscriptions() {
        return subscriptions;
    }

    public Set<MessagePubSub> getUnsubscriptions() {
        return unsubscriptions;
    }

    public Set<MessagePubSub> getPublications() {
        return publications;
    }

    public Map<String, SortedSet<MessagePubSub>> getReceptions() {
        return receptions;
    }

    public String getName() {
        return name;
    }
}
