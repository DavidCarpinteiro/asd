class MessagePubSub implements Comparable<MessagePubSub> {
    private final long time;
    private final String topic;
    private final String message;

    MessagePubSub(long time, String topic) {
        this(time, topic, null);
    }

    MessagePubSub(long time, String topic, String message) {
        this.time = time;
        this.topic = topic;
        this.message = message;
    }

    long getTime() {
        return time;
    }

    String getTopic() {
        return topic;
    }

    String getMessage() {
        return message;
    }

    @Override
    public int hashCode() {
        return Long.valueOf(time).hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof MessagePubSub)) {
            return false;
        }
        MessagePubSub other = (MessagePubSub) o;
        if (this.message != null && other.message != null)
            return this.message.equals(other.message) && this.time == other.time && this.topic.equals(other.topic);

        return this.time == other.time && this.topic.equals(other.topic) && this.message == null && other.message == null;
    }

    @Override
    public int compareTo(MessagePubSub msg) {
        int cmp = Long.compare(this.time, msg.time);
        if (cmp == 0) {
            cmp = this.topic.compareTo(msg.topic);
            if (cmp == 0 && this.message != null && msg.message != null) {
                cmp = this.message.compareTo(msg.message);
            }
        }
        return cmp;
    }

    @Override
    public String toString() {
        if (message == null)
            return String.format("Message %s topic at %d time", topic, time);
        else
            return String.format("Message %s topic %s at %d time", topic, message, time);
    }
}
