#!/bin/bash

if [[ "$#" -ne 10 ]]; then
  echo "Usage: ./run.sh number_of_processes starting_port | numb_ops sleep seed %sub %unsub %pub | %kill %revive"
  echo "Seed: -1 -> use random seed ; 0 -> use PORT as seed ; other -> use other as seed"
  exit 0
fi

exit_cmd() {
  echo "Exiting"
  pkill -f 'java -jar'
  #fuser -k "$KILL_PORT"/tcp >/dev/null 2>&1
  file="output.log"
  echo "Received by clients" "$(grep -o -F '[protocol message]' $file | wc -l)"
  echo "Sent by pubsub" "$(grep -o -F 'pubsub protocol message]' $file | wc -l)"
  echo "Sent by bcast" "$(grep -o -F 'bcast  protocol message]' $file | wc -l)"
  echo "Sent by hyparview" "$(grep -o -F 'hyparview protocol message]' $file | wc -l)"
  echo "Sent by disseminate" "$(grep -o -F 'disseminate protocol message]' $file | wc -l)"
  echo "Sent by chord" "$(grep -o -F 'chord protocol message]' $file | wc -l)"
  echo "Network only" "$(grep -o -F '(network)' $file | wc -l)"
  echo "Total " "$(grep -o -F 'protocol message]' $file | wc -l)"
  exit 2
}

trap "exit_cmd" INT SIGINT SIGTERM

NUMB=$1
PORT_START=$2

NUMB_ROUNDS=$3
NUMB_OPS=$4
SLEEP_ROUND=$5
SLEEP_OP=$6
SEED_ARG=$7

SUB=$8
UNSUB=$9
PUB=${10}

#KILL=$9
#REV=${10}

TARGET="java -jar target/asd-project-1.0-jar-with-dependencies.jar"
IP="127.0.0.1"

#declare -A nodes
nodes=()

pkill -f 'java -jar'

mvn package

rm -f ./logs/*
rm output.log
mkdir -p logs

clear

echo "Starting $NUMB processes at port $PORT_START++"

if [ "$SEED_ARG" -eq 0 ]; then
  SEED=$PORT_START
else
  SEED=$SEED_ARG
fi

$TARGET listen_base_port="$PORT_START" "$NUMB_ROUNDS" "$NUMB_OPS" "$SLEEP_ROUND" "$SLEEP_OP" "$SEED" "$SUB" "$UNSUB" "$PUB" | tee -a "logs/c$PORT_START.log" &
NODE_PID=$!
nodes["$PORT_START"]="$NODE_PID"

PORT=$PORT_START
PORT_OLD=$PORT_START

for ((c = 1; c < NUMB; c++)); do
  PORT_OLD="$(shuf -i "$PORT_START"-"$PORT" -n 1)"
  PORT=$((PORT + 1))
  if [ "$SEED_ARG" -eq 0 ]; then
    SEED=$PORT
  else
    SEED=$SEED_ARG
  fi
  $TARGET listen_base_port="$PORT" Contact="$IP":"$PORT_OLD" "$NUMB_ROUNDS" "$NUMB_OPS" "$SLEEP_ROUND" "$SLEEP_OP" "$SEED" "$SUB" "$UNSUB" "$PUB" | tee -a "logs/c$PORT.log" &
  NODE_PID=$!
  nodes["$PORT"]="$NODE_PID"
done
