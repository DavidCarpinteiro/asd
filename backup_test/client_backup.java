package asd.project.client;

import asd.project.utils.Pair;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.TimeUnit;

public class ClientAutomated {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static String port;
    private static String filename = "";

    public static void main(String[] args) throws Exception {
        System.setProperty("log4j.configurationFile", "log_disable.xml");

        // separate client and test arguments
        Arguments arguments = extractArgs(args);

        ClientTest c = new ClientTest(arguments.client_args);

        // load previously subribed
        Pair<List<String>, Integer> recovery = loadTopics(arguments.client_args);

        Parameters parameters = extractParameters(arguments.other_args, port);

        // Wait only if recovery is not necessary
        if (recovery.t.isEmpty())
            TimeUnit.SECONDS.sleep(4);

        run(c, parameters.num_ops, parameters.sleep, parameters.percentages, parameters.seed, recovery);
    }

    private static Pair<List<String>, Integer> loadTopics(String[] args) {
        for (String arg : args) {
            String val = arg.split("=")[0];
            if (val.equals("listen_base_port")) {
                port = arg.split("=")[1];
                filename = "topics/" + arg.split("=")[1] + ".topics";
                break;
            }
        }
        try {
            List<String> lines = Files.readAllLines(Paths.get(filename));
            List<String> topics = new LinkedList<>();
            int last_op = 0;
            for (String line : lines) {
                if (line.matches("[0-9]+"))
                    last_op = Integer.parseInt(line);
                else
                    topics.add(line);
            }
            //Files.deleteIfExists(Paths.get(file));
            new PrintWriter(filename).close();
            System.out.println(ANSI_RED + "loaded op " + last_op + " and " + topics.size() + " topics" + ANSI_RESET);
            return new Pair<>(topics, last_op);
        } catch (IOException e) {
            System.err.println(ANSI_RED + "nothing to load" + ANSI_RESET);
        }

        try {
            File file = new File(filename);
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Pair<>(new LinkedList<>(), 0);
    }

    private static void writeTopic(String topic) {
        topic = topic.concat("\n");
        try {
            Files.write(Paths.get(filename), topic.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.err.println("Error saving topic");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private static void run(ClientTest c, int numb_ops, int sleep, float[] percentages, long seed, Pair<List<String>, Integer> recovery) throws Exception {
        Random r = new Random(seed);
        String t;

        // Recovery from crash
        for (String topic : recovery.t) {
            c.subscribe(topic);
        }

        for (int op = recovery.u; op < numb_ops; op++) {

            //
            if ((op + 1) % 10 == 0) {
                writeTopic(Integer.toString(op + 1));
            }

            float chance = r.nextFloat();

            if (sleep != 0) {
                try {
                    int dev = (int) (sleep * 0.5);
                    TimeUnit.MILLISECONDS.sleep(getRdBetween(r, sleep - dev, sleep + dev));
                } catch (InterruptedException e) {
                    System.err.println("ERROR SLEEPING");
                }
            }
            if (chance >= 0 && chance < percentages[0]) {
                String top = "topic" + getRdBetween(r, 10, 30);
                c.subscribe(top);
                writeTopic(top);
            } else if (chance >= percentages[0] && chance < (percentages[0] + percentages[1])) {
                int idx = getRdBetween(r, 0, c.numberOfTopics());
                t = idx == -1 ? "topic1" : c.getTopic(idx);
                c.unsubscribe(t);
            } else if (chance >= (percentages[0] + percentages[1]) && chance < (percentages[0] + percentages[1] + percentages[2])) {
                int m = getRdBetween(r, 1000, 9000);
                int idx = getRdBetween(r, 0, c.numberOfTopics());
                t = idx == -1 ? "topic1" : c.getTopic(idx);
                c.publish(t, "message" + m);
            }
        }
    }

    private static Arguments extractArgs(String[] args) {
        Set<String> parameters = new HashSet<>(Arrays.asList(
                "listen_base_port",
                "listen_interface",
                "listen_address",
                "Contact"
        ));

        List<String> tmp_client = new LinkedList<>();
        List<String> tmp_other = new LinkedList<>();

        for (String arg : args) {
            String val = arg.split("=")[0];
            if (parameters.contains(val))
                tmp_client.add(arg);
            else
                tmp_other.add(arg);

        }

        return new Arguments(tmp_client, tmp_other);
    }

    private static Parameters extractParameters(String[] args, String port) {
        // parameters need a set order [numb_ops, %sub, %unsub, %pub]

        int num_ops = Integer.parseInt(args[0]);

        int sleep = Integer.parseInt(args[1]);

        float[] percentages = new float[3];

        for (int i = 0; i < (percentages.length); i++) {
            percentages[i] = Float.parseFloat(args[i + 2]);
        }

        long seed;
        if (args.length >= 6 && args[5].equals("y")) {
            seed = Long.parseLong(port);
            System.out.println(ANSI_RED + "using seed " + seed + ANSI_RESET);
        } else {
            seed = new Random().nextLong();
            System.out.println(ANSI_RED + "random seed" + ANSI_RESET);
        }
        return new Parameters(num_ops, sleep, percentages, seed);
    }

    // Get a random number between low inclusive and high exclusive
    private static int getRdBetween(Random r, int low, int high) {
        if (low - high == 0)
            return -1;
        return r.nextInt(high - low) + low;
    }

    static class Arguments {
        String[] client_args;
        String[] other_args;

        public Arguments(String[] client_args, String[] other_args) {
            this.client_args = client_args;
            this.other_args = other_args;
        }

        public Arguments(List<String> client_args, List<String> other_args) {
            this.client_args = new String[client_args.size()];
            this.client_args = client_args.toArray(this.client_args);
            this.other_args = new String[other_args.size()];
            this.other_args = other_args.toArray(this.other_args);
        }
    }

    static class Parameters {
        int num_ops;
        int sleep;
        float[] percentages;
        long seed;

        public Parameters(int num_ops, int sleep, float[] percentages, long seed) {
            this.num_ops = num_ops;
            this.sleep = sleep;
            this.percentages = percentages;
            this.seed = seed;
        }
    }

}
