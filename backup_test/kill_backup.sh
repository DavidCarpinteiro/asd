declare -A nodes
to_revive=()

if [ "$KILL" -gt 0 ]; then
  sleep 10
  while true; do
    sleep 1

    CHANCE="$(shuf -i 0-100 -n 1)"

    if [ "$CHANCE" -le "$KILL" ]; then
      SS=$((PORT_START + 1))
      KILL_PORT="$(shuf -i "$SS"-"$PORT" -n 1)"
      KILL_PID="${nodes[$KILL_PORT]}"

      if [ "$KILL_PID" -ne -1 ]; then
        echo "KILLING $KILL_PORT ID $KILL_PID"
        to_revive+=("$KILL_PORT")
        nodes["$KILL_PORT"]=-1
        fuser -k "$KILL_PORT"/tcp >/dev/null 2>&1
        sleep 1
      fi
    elif [ "$CHANCE" -le "$((REV + KILL))" ]; then
      if [ ${#to_revive[@]} -ne 0 ]; then
        #printf '%s\n' "${to_revive[@]}"
        size=${#to_revive[@]}
        size=$((size - 1))

        idx="$(shuf -i 0-"$size" -n 1)"
        delete="${to_revive[$idx]}"
        echo "REVIVING $delete"
        tmp=()
        for i in "${to_revive[@]}"; do
          if [[ "$i" -ne "$delete" ]]; then
            tmp+=("$i")
          fi
        done
        to_revive=("${tmp[@]}")
        $TARGET listen_base_port="$delete" Contact="$IP":"$PORT_START" "$NUMB_OPS" "$SLEEP" "$SUB" "$UNSUB" "$PUB" | tee -a "logs/c$delete.log" &
        NODE_PID=$!
        nodes["$delete"]="$NODE_PID"
      fi
    fi
  done
fi

# Used to randomly select a process and kill it (simulate crash)
: '
sleep 10
while true; do
  sleep 1
  if [ "$KILL" -gt 0 ]; then
    CHANCE="$(shuf -i 0-100 -n 1)"

    if [ "$CHANCE" -le "$KILL" ]; then
      SS=$((PORT_START + 1))
      KILL_PORT="$(shuf -i "$SS"-"$PORT" -n 1)"
      KILL_PID="${nodes[$KILL_PORT]}"

      if [[ "$KILL_PID" -ne "-1" ]]; then
        echo $'\e[1
33m'"KILLING $KILL_PORT"$'\e[0m'
        tmp=()
        for i in "${nodes[@]}"; do
          if [[ "$i" -ne "$KILL_PORT" ]]; then
            tmp+=("$i")
          fi
        done
        nodes=("${tmp[@]}")
        fuser -k "$KILL_PORT"/tcp >/dev/null 2>&1
        sleep 1
      fi
    elif [ "$CHANCE" -le "$((REV + KILL))" ]; then
      PORT=$((PORT + 1))
      echo $'\e[1
33m'"REVIVING $PORT"$'\e[0m'
      if [ "$SEED_ARG" -eq 0 ]; then
        SEED=$PORT
      else
        SEED=$SEED_ARG
      fi
      $TARGET listen_base_port="$PORT" Contact="$IP":"$PORT_START" "$NUMB_OPS" "$SLEEP" "$SEED" "$SUB" "$UNSUB" "$PUB" | tee -a "logs/c$PORT.log" &
      NODE_PID=$!
      nodes["$PORT"]="$NODE_PID"
    fi
  fi
done
'
