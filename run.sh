#!/bin/bash

if [[ "$#" -lt 11 ]]; then
  echo "Usage: ./run.sh starting_port | number_of_processes numb_replicas | numb_rounds numb_ops (sec)sleep_round (ms)sleep_op | (float) %sub %unsub %pub | (int) contention"
  echo "Seed: -1 -> use random seed ; 0 -> use PORT as seed ; other -> use other as seed"
  echo "Contention: if 2 then topic ids will be from 1 to 20"
  exit 0
fi

cleanup() {
  pkill -f 'java -jar'
  rm -f ./locks/*
  find ./logs -type f -not -name '.gitkeep' -delete
  find ./storage -type f -not -name '.gitkeep' -delete
  rm -f output.log
  mkdir -p logs
  mkdir -p locks

}

exit_cmd() {
  echo "Exiting"
  pkill -f 'java -jar'
  #fuser -k "$KILL_PORT"/tcp >/dev/null 2>&1
  file="output.log"
  echo "RESULTS FOR PHASE " #$j"
  echo "Received by clients" "$(grep -o -F '[protocol message]' $file | wc -l)"
  echo "Sent by pubsub" "$(grep -o -F 'pubsub protocol message]' $file | wc -l)"
  echo "Sent by bcast" "$(grep -o -F 'bcast  protocol message]' $file | wc -l)"
  echo "Sent by hyparview" "$(grep -o -F 'hyparview protocol message]' $file | wc -l)"
  echo "Sent by disseminate" "$(grep -o -F 'disseminate protocol message]' $file | wc -l)"
  echo "Sent by chord" "$(grep -o -F 'chord protocol message]' $file | wc -l)"
  echo "Sent by paxos" "$(grep -o -F 'paxos protocol message]' $file | wc -l)"
  echo "Network only" "$(grep -o -F '(network)' $file | wc -l)"
  echo "Total " "$(grep -o -F 'protocol message]' $file | wc -l)"
  exit 2
}

trap "exit_cmd" INT SIGINT SIGTERM

PORT_START=$1
NUMB_PROCESSES=$2
REPLICA_COUNT=$3
NUMB_ROUNDS=$4
NUMB_OPS=$5
SLEEP_ROUND=$6
SLEEP_OP=$7

SEED=-1 # unused

SUB=$8
UNSUB=$9
PUB=${10}

CONTENTION=${11}

CLIENT_ARGS="$NUMB_ROUNDS $NUMB_OPS $SLEEP_ROUND $SLEEP_OP $SEED $SUB $UNSUB $PUB $CONTENTION"

echo "$CLIENT_ARGS"

TIME_BETWEEN_LAUNCHES=7

TARGET="java -jar target/asd-project-1.0-jar-with-dependencies.jar"
IP="127.0.0.1"

cleanup
#mvn package
sleep 1
clear
echo "Starting $NUMB_PROCESSES x $REPLICA_COUNT processes at port $PORT_START++ executing $NUMB_ROUNDS rounds with $NUMB_OPS operations each"

launch_node() {
  IP="$1"
  MY_PORT="$2"
  CONTACT_PORT="$3"
  PAXOS_PORT="$4"
  IS_LEADER="$5"
  IN_RING="$6"
  IS_FIRST="$7"

  PRINT=""
  CONNECTION="listen_base_port=$MY_PORT"
  FILENAME=""

  if [[ 1 == "$IN_RING" ]]; then
    FILENAME="ring$IP:$MY_PORT.log"
    PRINT="$PRINT INSIDE  RING"
    CONNECTION="$CONNECTION RingReplica=true"
    # if node is not the first
    if [[ 0 == "$IS_FIRST" ]]; then
      CONNECTION="$CONNECTION Contact=$IP:$CONTACT_PORT"
    fi
  else
    FILENAME="nope$IP:$MY_PORT.log"
    PRINT="$PRINT OUTSIDE RING"
    CONNECTION="$CONNECTION RingReplica=false"
  fi

  if [[ 1 == "$IS_LEADER" ]]; then
    PRINT="$PRINT : LEADER"
  else
    PRINT="$PRINT : REPLICA"
    CONNECTION="$CONNECTION PaxosContact=$IP:$PAXOS_PORT"
  fi

  printf "\n\t\%s\n" "$PRINT"
  #echo "         $CONNECTION "
  echo "$CONNECTION $CLIENT_ARGS"

  #echo "asd" | tee -a "logs/$FILENAME"
  $TARGET "$CONNECTION $CLIENT_ARGS" | tee -a "logs/$FILENAME" &#&> /dev/null &#

}

# ports of ring nodes
declare -a ring_nodes

MY_PORT=$PORT_START
IS_FIRST=1

# launch nodes
for ((n = 0; n < NUMB_PROCESSES; n++)); do
  # choose a random node to be in inside ring
  TOTAL=$((REPLICA_COUNT - 1))
  echo "TOTAL: $TOTAL"
  CHANCE_RING="$(shuf -i 0-"$TOTAL" -n1)"

  IS_LEADER=1
  IN_RING=0

  for ((REPLICA = 0; REPLICA < REPLICA_COUNT; REPLICA++)); do
    # set the paxos contact port to the first node / leader
    if [[ "$REPLICA" == 0 ]]; then
      PAXOS_PORT="$MY_PORT"
    fi

    # if this node is in the ring
    #   -> set the contact port to a previously added ring_node
    #   -> add it to the ring_nodes array
    if [[ "$REPLICA" == "$CHANCE_RING" ]]; then
      IN_RING=1
      if [[ "$IS_FIRST" == 0 ]]; then
        RND_IDX="$(shuf -i 0-$((${#ring_nodes[@]} - 1)) -n 1)"
        CONTACT_PORT="${ring_nodes[$RND_IDX]}"
      fi
      ring_nodes+=("$MY_PORT")
    else
      IN_RING=0
    fi

    launch_node "$IP" "$MY_PORT" "$CONTACT_PORT" "$PAXOS_PORT" "$IS_LEADER" "$IN_RING" "$IS_FIRST"
    MY_PORT=$((MY_PORT + 1))

    if [[ "$IS_LEADER" == 1 ]]; then
      sleep 5
      IS_LEADER=0
    fi

  done
  IS_FIRST=0

  sleep "$TIME_BETWEEN_LAUNCHES"
done

giveSignal() {
  touch locks/start
  sleep 2
  rm locks/start
}

waitForSignal() {

  while [[ "$(find ./locks -name 'done*' | wc -l)" != $((NUMB_PROCESSES)) ]]; do
    sleep 1
  done
  rm -f ./locks/*
  sleep 1
}

for ((i = 0; i < NUMB_ROUNDS; i++)); do
  echo "Starting round $((i + 1))"

  waitForSignal
  sleep "$SLEEP_ROUND"
  #echo ">>>>>> END OF SUBSCRIBE ROUND <<<<<<" | tee -a output.log
  giveSignal

  waitForSignal
  sleep "$SLEEP_ROUND"
  #echo ">>>>>> END OF PUBLISH ROUND <<<<<<" | tee -a output.log
  giveSignal

  waitForSignal
  sleep 10
  #echo ">>>>>> END OF UNSUBSCRIBE ROUND <<<<<<" | tee -a output.log
  giveSignal
done

while true; do
  sleep 1
done
