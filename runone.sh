#!/bin/bash

if [[ "$#" -ne 8 ]]; then
  echo "Usage: ./run.sh port connnection numb_ops sleep seed %sub %unsub %pub"
  echo "Seed: -1 -> use random seed ; 0 -> use PORT as seed ; other -> use other as seed"
  echo "Connection: 0 -> none ; other -> other"
  exit 0
fi

exit_cmd() {
  echo "Exiting"
  pkill -f 'java -jar'
  file="output.log"
  echo "Sent protocol messages" "$(grep -o -F '[protocol message]' $file | wc -l)"
  exit 2
}

trap "exit_cmd" INT SIGINT SIGTERM

PORT=$1
CONNECTION=$2
NUMB_OPS=$3
SLEEP=$4
SEED_ARG=$5
SUB=$6
UNSUB=$7
PUB=$8

TARGET="java -jar target/asd-project-1.0-jar-with-dependencies.jar"

pkill -f 'java -jar'

mvn package

rm -f ./logs/*
rm output.log
mkdir -p logs

clear

if [ "$SEED_ARG" -eq 0 ]; then
  SEED=$PORT_START
else
  SEED=$SEED_ARG
fi

if [ "$CONNECTION" -eq 0 ]; then
  $TARGET listen_base_port="$PORT" "$NUMB_OPS" "$SLEEP" "$SEED" "$SUB" "$UNSUB" "$PUB" | tee -a logs/c"$PORT".log &
else
  $TARGET listen_base_port="$PORT" Contact="$CONNECTION" "$NUMB_OPS" "$SLEEP" "$SEED" "$SUB" "$UNSUB" "$PUB" | tee -a logs/c"$PORT".log &
fi
