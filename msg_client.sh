#!/bin/bash

NUMB=$1
PORT=$2

for ((c = 1; c <= NUMB; c++)); do
  echo "$PORT: " $(grep -o -F "protocol message] (network) $PORT" output.log | wc -l)
  PORT=$((PORT + 1))
done
